/*
 * Scheduler.cpp
 *
 *  Created on: 24 de Fev de 2013
 *      Author: Fernando Alexandre
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
#include "Scheduler.hpp"
#include "StaticAutoTuner.hpp"
#include "marrow/KernelWrapper.hpp"
#include "marrow/utils/Debug.hpp"

marrow::runtime::Scheduler::Scheduler() {
	init(-1, DEFAULT_OVERLAP_PARTITIONS);
}

marrow::runtime::Scheduler::Scheduler(const unsigned int numDevices) {
	init(numDevices, DEFAULT_OVERLAP_PARTITIONS);
}

marrow::runtime::Scheduler::Scheduler(const unsigned int numDevices, const unsigned int numOverlapPartitions) {
	init(numDevices, numOverlapPartitions);
}

marrow::runtime::Scheduler::~Scheduler() {
	std::list<std::shared_ptr<Task>>::iterator it;

	for (it = allTasks.begin(); it != allTasks.end(); it++) {
		(*it).reset();
	}

	autotuner.reset();
	taskLauncher.reset();
	gpuQueues.reset();
	for(unsigned int i = 0; i < numDevices; i++) {
		queuesMutex[i].reset();
	}
}

void marrow::runtime::Scheduler::init(const int numDevices, const unsigned int numOverlapPartitions) {
	std::vector<cl_device_type> types(1);
	types[0] = DEVICETYPE;

	this->numOverlapPartitions = numOverlapPartitions;

	if(numDevices == -1) {
		marrow::Skeleton::executionPlatform->initialize(types, numOverlapPartitions);
		this->numDevices = marrow::Skeleton::executionPlatform->getNumDevicesOfType(DEVICETYPE);
	} else {
		std::vector<unsigned int> numDevicesVector(1);
		numDevicesVector[0] = numDevices;

		marrow::Skeleton::executionPlatform->initialize(numDevicesVector, types, numOverlapPartitions);
		this->numDevices = numDevices;
	}

	gpuQueues = std::shared_ptr<std::vector<std::list<TaskEntry>>> (new std::vector<std::list<TaskEntry>>(this->numDevices));
	queuesMutex = new std::shared_ptr<boost::mutex>[this->numDevices];
	for(unsigned int i = 0; i < this->numDevices; i++) {
		queuesMutex[i] = std::shared_ptr<boost::mutex>(new boost::mutex());
	}

	marrow::runtime::Scheduler::autotuner = std::unique_ptr<IAutoTuner> (new StaticAutoTuner(this->numDevices, numOverlapPartitions));

	emptyQueue = new boost::condition_variable_any[this->numDevices];

	marrow::runtime::Scheduler::taskLauncher = std::unique_ptr<TaskLauncher> (new TaskLauncher(gpuQueues, queuesMutex, emptyQueue, this->numDevices, numOverlapPartitions));
}

void marrow::runtime::Scheduler::submit(std::shared_ptr<Task> task) {
	unsigned int i, j;

	cleanFinishedTasks();

	task->setNumPartitions(numDevices);
	task->setNumOverlapPartitions(numOverlapPartitions);

	DEBUG_MSG("Obtained queueMutex to submit task = %p", &task);

	for (i = 0; i < numDevices; i++) {
		for(j = 0; j < numOverlapPartitions; j++) {
			TaskEntry curr;
			curr.task = task;
			curr.partitionIndex = i;
			curr.overlapPartitionIndex = j;

			boost::lock_guard<boost::mutex> queueLock(*(queuesMutex[i]));
			(*gpuQueues)[i].push_back(curr);
			emptyQueue[i].notify_one();
			DEBUG_MSG("Pushing Task to queueIndex = %u", i);
		}
	}

	allTasks.push_back(task);
}

void marrow::runtime::Scheduler::cleanFinishedTasks() {
	std::list<std::shared_ptr<Task>>::iterator it;

	DEBUG_MSG("Triggered cleanup. allTasks.size() = %lu", allTasks.size());

	for (it = allTasks.begin(); it != allTasks.end();) {
		if((*it)->hasFinished()) {
			(*it).reset();
			it = allTasks.erase(it);
			DEBUG_MSG("Cleared item in allTasks; remaining = %lu", allTasks.size());
		} else {
			it++;
		}
	}
}

void marrow::runtime::Scheduler::setPartitions(marrow::Skeleton* s, bool isReconfigure) {
	unsigned int i;
	std::vector<void*> * computations = s->getExecutables();

	// Profile the duration of the data-partitioning not including the memory allocation
	#ifdef PROFILING
	double avg = 0.0;
	for(unsigned w = 0; w < 300; w++) {
		Stopwatch s1(true);

		for(i = 0; i < (*computations).size(); i++) {
		marrow::KernelWrapper *curr = static_cast<marrow::KernelWrapper*>((*computations)[i]);

		if(!curr->hasSetPartitions() || isReconfigure) {
			std::shared_ptr<std::vector<std::vector<std::vector<size_t>>>> workSizes = (*autotuner).getWorkSizes(
																			*(*curr).getGlobalWorkSize(),
																			*(*curr).getLocalWorkSize(),
																			(*curr).isDoublePrecision(),
																			(*curr).requiredStaticMem(),
																			(*curr).requiredDynamicMem());
			(*autotuner).getInputPartitions(*(*curr).getInputDataInfo(), *workSizes);
			(*autotuner).getOutputPartitions(*(*curr).getOutputDataInfo(), *workSizes);
			}
		}

		avg += s1.StopResult(2);

		avg = avg / 300.0;
		std::cout << "Partitioning Avg = " << avg << std::endl;
	}
	#endif

	for(i = 0; i < (*computations).size(); i++) {
		marrow::KernelWrapper *curr = static_cast<marrow::KernelWrapper*>((*computations)[i]);

		if(!curr->hasSetPartitions() || isReconfigure) {
			std::shared_ptr<std::vector<std::vector<std::vector<size_t>>>> workSizes = (*autotuner).getWorkSizes(
																			*(*curr).getGlobalWorkSize(),
																			*(*curr).getLocalWorkSize(),
																			(*curr).isDoublePrecision(),
																			(*curr).requiredStaticMem(),
																			(*curr).requiredDynamicMem());

			DEBUG_MSG("Starting partitioning for computation #%u", i);
			(*curr).setWorkSize(workSizes);
			(*curr).setPartitionedInput(*(*autotuner).getInputPartitions(*(*curr).getInputDataInfo(), *workSizes));
			(*curr).setPartitionedOutput(*(*autotuner).getOutputPartitions(*(*curr).getOutputDataInfo(), *workSizes));
		} else {
			DEBUG_MSG("Skipped setting partitions for computation #%u", i);
		}
	}
}

unsigned int marrow::runtime::Scheduler::getNumOverlapPartitions() {
	return numOverlapPartitions;
}

unsigned int marrow::runtime::Scheduler::getNumDevices() {
	return numDevices;
}
