/*
 * TaskLauncher.hpp
 *
 *  March 2013:
 *       Fernando Alexandre
 *       Creation of the task launcher concept
 *
 *  Created on: 1 de Mar de 2013
 *      Author: Fernando Alexandre
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef __MARROW_RUNTIME_TASKLAUNCHER_HPP__
#define __MARROW_RUNTIME_TASKLAUNCHER_HPP__

#ifdef __APPLE__
	#include <OpenCL/cl.h>
#else
	#include <CL/cl.h>
#endif

#include <map>
#include <vector>
#include <list>
#include <string>
#include <memory>
#include <boost/thread.hpp>


#include "marrow/runtime/Task.hpp"

/*#include "IPlatformContext.hpp"
#include "ExecutionPlatform.hpp"

#include "OpenCLErrorParser.hpp"
#include "Debug.hpp"*/

// Type of the OpenCL device used in the computations
#define DEVICETYPE CL_DEVICE_TYPE_GPU

// Represents an entry of a GPU queue.
 typedef struct {
	unsigned int partitionIndex;
	unsigned int overlapPartitionIndex;
	std::shared_ptr<Task> task;
} TaskEntry;

namespace marrow {

    namespace runtime {
        /**
         * This class represents the task launcher, which monitors the shared task queues for execution.
         */
        class TaskLauncher {
        public:
            /**
             * Default constructor
             */
            TaskLauncher(const std::shared_ptr<std::vector<std::list<TaskEntry>>> queues,
                std::shared_ptr<boost::mutex> * qMutexes,
                boost::condition_variable_any *emptyCond,
                const unsigned int numberDevices,
                const unsigned int nOverlapPartitions);

            /**
             * Class destructor
             */
            ~TaskLauncher();

            /**
             * Returns the number of devices configured.
             *
             * @return the number of devices.
             */
            virtual unsigned int getNumDevices();

        protected:
            // True if the overlapper is being deallocated.
            bool closed;

            // Array of workers
            boost::thread *workers;

            // The actual GPU queues
            std::shared_ptr<std::vector<std::list<TaskEntry>>> gpuQueues;
            // The (overlapping) command queues
            std::vector<std::vector<cl_command_queue>> queues;

            // Mutexes used to synchronize access in each task queue.
            std::shared_ptr<boost::mutex> * queuesMutex;

            // Condition strucuture for when @gpuQueues is empty
            boost::condition_variable_any * emptyQueue;

            // Number of devices used for computation.
            unsigned int numDevices;

            // Number of overlap partitions within each device.
            unsigned int numOverlapPartitions;

            /**
             * Attempts to pop a TaskEntry from the task queue of deviceIndex.
             *
             * @param deviceIndex - The index of the device.
             * @return An object which may or may not contain a TaskEntry.
             */
            virtual boost::optional<TaskEntry> popTask(const unsigned int deviceIndex);

            /**
             * Starts the execution of a thread, monitoring a device's queue for new TaskEntries.
             *
             * @param deviceIndex - The index of the device.
             */
            virtual void run(const unsigned int deviceIndex);

            /**
             * Executes a TaskEntry in its assigned device.
             *
             * @param taskEntry - The TaskEntry to be executed.
             * @param deviceIndex - The index of the device.
             */
            virtual void execute(const TaskEntry &taskEntry, const unsigned int deviceIndex);
        };
    }
}
#endif /* __MARROW_RUNTIME_TASKLAUNCHER_HPP__ */
