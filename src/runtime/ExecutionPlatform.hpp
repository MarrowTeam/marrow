/*
 * ExecutionPlatform.hpp
 *
 *	April 2012:
 *		Ricardo Marques
 *		Creation of the execution platform concept
 *
 *	September 2013:
 *		Fernando Alexandre
 *		Adaptation for multi-GPU environment
 *
 *  Created on: 3 de Abr de 2012
 *      Author: Ricardo Marques
 *			Modified by Fernando Alexandre
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef EXECUTIONPLATFORM_HPP_
#define EXECUTIONPLATFORM_HPP_

#include <vector>
#include <list>
#include <iostream>
#include <map>
#include <cstdio>

#include "marrow/runtime/IPlatformContext.hpp"

#include "OpenCLErrorParser.hpp"

/**
 * This class implements the IPlatformContext. It is used, ans shared, by the nodes of a composition tree.
 */
class ExecutionPlatform: public IPlatformContext {
public:
	/**
	 * This constructor simply creates the object, for it to be useable, it is necessary to call initizalize.
	 */
	ExecutionPlatform();

	/**
	 * This function enables the developer to select the number, and type, of devices that he, or she, wishes to
	 * associate to a ExecutionPlatform instance. The arguments are two vectors, that define the number of devices of
	 * each type to be associated to the context, in a one-to-one matching. In other words, @numDevices[i] states that
	 * the user intends for the context to contain N devices of type @types[i]. Logically, both vector must be of the same size.
	 * Nonetheless, the actual number of devices of type T = @types[i] associated to the context depend upon the number of
	 * devices of type T available on the runtime platform. Therefore, the context can contain up to @numDevices[i] devices
	 * of type @types[i]. To assert the number, and types of the devices effectively associated to the context, the user should invoque
	 * the appropriate IPlatformContext functions.
	 *
	 * @param numDevices - The number of devices to be associated to the context.
	 * @param types - The types of the devices to be associated to the context.
	 * @param numOverlap - Number of overlapping partitions (changes the number of contexts).
	 */
	virtual void initialize(const std::vector<unsigned int> &numDevices, const std::vector<cl_device_type> &types, unsigned int numOverlap);

	/**
	 * This function enables the developer to select the type of devices that he, or she, wishes to
	 * associate to a ExecutionPlatform instance, using the maximum number of devices available. The arguments are two vectors, that define the number of devices of
	 * each type to be associated to the context, in a one-to-one matching. In other words, @numDevices[i] states that
	 * the user intends for the context to contain N devices of type @types[i]. Logically, both vector must be of the same size.
	 * Nonetheless, the actual number of devices of type T = @types[i] associated to the context depend upon the number of
	 * devices of type T available on the runtime platform. Therefore, the context can contain up to @numDevices[i] devices
	 * of type @types[i]. To assert the number, and types of the devices effectively associated to the context, the user should invoque
	 * the appropriate IPlatformContext functions.
	 *
	 * @param types - The types of the devices to be associated to the context.
	 * @param numOverlap - Number of overlapping partitions (changes the number of contexts).
	 */
	virtual void initialize(const std::vector<cl_device_type> &types, unsigned int numOverlap);

	/**
	 * Class destructor
	 */
	virtual ~ExecutionPlatform();

	virtual cl_context getPlatformContext(unsigned int deviceIndex, unsigned int overlapIndex);

	virtual std::vector<std::vector<cl_context>> * getAllPlatformContexts();

	virtual unsigned int getNumDevices();

	virtual void getDevices(std::vector<cl_device_id> &devices);

	virtual void getDevicesOfType(cl_device_type type, std::vector<cl_device_id> &devices);

	virtual unsigned int getNumDevicesOfType(cl_device_type type);

	virtual cl_device_type getType(unsigned int deviceIndex);

	virtual cl_ulong getGlobalMemorySize(unsigned int deviceIndex);

	virtual cl_ulong getTotalFreeMemorySize();

	virtual cl_ulong getFreeMemorySize(unsigned int deviceIndex);

	virtual void maxWorkgroupLenghtPerDim(unsigned int deviceIndex, std::vector<size_t> &localSize);

	virtual size_t maxWorkgroupLenght(unsigned int deviceIndex);

	virtual cl_uint maxDimensions(unsigned int deviceIndex);

	virtual cl_ulong getLocalMemorySize(unsigned int deviceIndex);

	virtual cl_mem allocateDeviceMem(IWorkData &dataInfo, unsigned int deviceIndex, unsigned int overlapIndex);

	virtual void freeDeviceMem(IWorkData &dataInfo, unsigned int deviceIndex, cl_mem deviceMem);

	virtual cl_command_queue createCommandQueue(unsigned int deviceIndex, unsigned int overlapIndex);

	virtual bool supportImages(unsigned int deviceIndex);

protected:
	// OpenCL context
	std::vector<std::vector<cl_context>> context;
	// Devices associated to the context
	std::vector<cl_device_id> devices;
	// marrow::Map between the types of the devices associated to the context, and their total
	std::map<cl_device_type, int> numDevicePerType;
	// Error parser
	OpenCLErrorParser parser;
	// OpenCL platform id
	cl_platform_id platformId;
	// Total free memory of all the devices.
	cl_ulong deviceTotalFreeMem;

	// Free memory of each device
	std::vector<cl_ulong> deviceFreeMem;

	// Total memory capacity of each device.
	std::vector<cl_ulong> deviceMemoryCapacity;

	// Whether execution platform is initialized or not.
	bool isInit;
};

#endif /* EXECUTIONPLATFORM_HPP_ */
