/*
 * KernelBuilder.cpp
 *
 *  Created on: 22 de Fev de 2012
 *      Author: Ricardo Marques
 *			Modified by Fernando Alexandre
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "marrow/runtime/KernelBuilder.hpp"
#include "marrow/exceptions/KernelCompilationFailedException.hpp"
#include "marrow/exceptions/FileNotFoundException.hpp"
#include "marrow/utils/Debug.hpp"

#include "OpenCLErrorParser.hpp"


KernelBuilder::KernelBuilder(){}

KernelBuilder::~KernelBuilder(){}

cl_kernel KernelBuilder::createKernelFromFile(const std::string &fileName, const std::string &kernelFunc, cl_context context, std::vector<cl_device_id> &devices, std::vector<unsigned int> &deviceIndices){
	cl_program program;
	cl_kernel kernel;
	program = getProgram(fileName, context, devices, deviceIndices);
	kernel = getNewKernelInstance(program, kernelFunc);

	clReleaseProgram(program);
	return kernel;
}

cl_kernel KernelBuilder::getNewKernelInstance(const cl_program program, const std::string &kernelFunc) {
	int errcode;
	cl_kernel kernel;

	kernel = clCreateKernel(program, kernelFunc.data(), &errcode);

	if(errcode != CL_SUCCESS){
		DEBUG_MSG("Failed to get a new kernel instance: %s", OpenCLErrorParser::errorString(errcode));
		throw marrow::KernelCompilationFailedException("", std::string(""));
	}

	return kernel;
}


cl_program KernelBuilder::getProgram(const std::string &fileName, const cl_context context, std::vector<cl_device_id> &devices, std::vector<unsigned int> &deviceIndices){
	cl_program program;
	int errcode;
	bool fromSource = false;

	// Generate the device-aware filename.
	std::stringstream deviceFileName;
	deviceFileName << fileName;
	for(unsigned int i = 0; i < deviceIndices.size(); i++) {
		deviceFileName << "_";
		deviceFileName << deviceIndices[i];
	}
	// try to read from pre-created binaries
	program = programFromBinaries(deviceFileName.str(), context, devices);
	// if binaries not available read from source
	if(program == NULL){
		program = programFromSource(fileName, context);
		fromSource = true;
	}
	errcode = clBuildProgram(program, devices.size(), devices.data(), NULL, NULL, NULL);
	// this if block is a workaround for a bug in the clCreateProgramWithBinary NVIDIA OpenCL implementation
	// the funcion should return NULL if the pre-compiled binaries are incompatible with the devices
	// however, it is not doing so, because of clCreateProgramWithBinary
	// this if block should be removed when the bug if fixed
	// #########################################################################################
	if(errcode == CL_INVALID_BINARY){
		errcode = clReleaseProgram(program);
		program = programFromSource(fileName, context);
		fromSource = true;
		errcode = clBuildProgram(program, devices.size(), devices.data(), NULL, NULL, NULL);
	}
	else
	// #########################################################################################
	if(errcode != CL_SUCCESS){
		std::cerr << OpenCLErrorParser::errorString(errcode) << std::endl;
		char buildlog[16000];
		errcode = clGetProgramBuildInfo(program, devices[0], CL_PROGRAM_BUILD_LOG, sizeof(buildlog), buildlog, NULL);
		errcode = clReleaseProgram(program);
		throw marrow::KernelCompilationFailedException(fileName, buildlog);
	}
	// if the the kernel was created from source code, obtain and store its binaries
	if(fromSource){
		saveKernelBinaries(deviceFileName.str(), program, devices.size());
	}
	return program;
}

cl_program KernelBuilder::programFromBinaries(const std::string &fileName, const cl_context context, std::vector<cl_device_id> &devices){
	std::ifstream binaryFile(fileName.data() + std::string(EXTENSION), std::ios::in | std::ios::binary);
	if(!binaryFile.is_open()){
		binaryFile.close();
		return NULL;
	}
	int errcode = 0;
	cl_program program;
	size_t size = fileSize(binaryFile);
	unsigned char *binaries = new unsigned char[size];
	// read full binaries
	binaryFile.read((char*) binaries, size);
	program = clCreateProgramWithBinary(context, devices.size(), devices.data(), &size, (const unsigned char**) &binaries, NULL, &errcode);
	delete [] binaries;
	binaryFile.close();
	if(errcode != CL_SUCCESS){
		//std::cerr << "Error while creating program from binaries: " << OpenCLErrorParser::errorString(errcode) << std::endl;
		return NULL;
	}
	return program;
}

cl_program KernelBuilder::programFromSource(const std::string &fileName, const cl_context context){
	std::ifstream kernelFile(fileName.data(), std::ios::in);
	if (!kernelFile.is_open()){
		kernelFile.close();
		throw marrow::FileNotFoundException(fileName);
	}
	int errcode;
	cl_program program;
	std::ostringstream oss;
	oss << kernelFile.rdbuf();
	std::string srcStdStr = oss.str();
	const char *srcStr = srcStdStr.c_str();
	program = clCreateProgramWithSource(context, 1, (const char**)&srcStr, NULL, &errcode);
	if(errcode != CL_SUCCESS){
		std::cerr << OpenCLErrorParser::errorString(errcode) << std::endl;
		return NULL;
	}
	return program;
}

bool KernelBuilder::saveKernelBinaries(const std::string &fileName, const cl_program program, unsigned int numDevices){
	bool result = false;
	int errcode;
	size_t binariesSizes[numDevices];
	// get binarty size in bytes
	errcode = clGetProgramInfo(program, CL_PROGRAM_BINARY_SIZES, sizeof(size_t)*numDevices, binariesSizes, NULL);
	if(errcode != CL_SUCCESS){
		std::cerr << OpenCLErrorParser::errorString(errcode) << std::endl;
		return result;
	}
	unsigned char *programBinaries[numDevices];
	for(unsigned int i = 0; i < numDevices; i++){
		if(binariesSizes[i] == 0){
			return false;
		}
		programBinaries[i] = new unsigned char[binariesSizes[i]];
	}
	// get the actual binaries
	errcode = clGetProgramInfo(program, CL_PROGRAM_BINARIES, sizeof(unsigned char*) * numDevices, programBinaries, NULL);
	if(errcode != CL_SUCCESS){
		std::cerr << OpenCLErrorParser::errorString(errcode) << std::endl;
	}
	else {
		std::ofstream stream(fileName.data() + std::string(EXTENSION), std::ios::out | std::ios::binary);
		// no multi-device context support, assuming single device
		stream.write((const char*) programBinaries[0], binariesSizes[0]);
		stream.close();
		result = true;
		for(unsigned int i = 0; i < numDevices; i++){
			delete [] programBinaries[i];
		}
	}
	return result;
}

unsigned int KernelBuilder::fileSize(std::ifstream &stream){
	int size = 0;
	stream.seekg(0, std::ios::end);
	size = stream.tellg();
	stream.seekg(0, std::ios::beg);
	return size;
}
