/*
 * StaticAutoTuner.hpp
 *
 *	February 2013:
 *		Fernando Alexandre
 *		Creation of the static autotuner
 *
 *  Created on: 24 de Fev de 2013
 *      Author: Fernando Alexandre
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef StaticAutoTuner_HPP_
#define StaticAutoTuner_HPP_

#include <vector>


#include "IAutoTuner.hpp"
#include "marrow/runtime/IWorkData.hpp"
#include "PerfInfo.hpp"

/**
 * Class that extends IAutoTuner for a static decomposition for GPUs.
 */
class StaticAutoTuner: public IAutoTuner {
public:
	/**
	 * Constructor
	 * PRE-REQUISITE: IPlatform context in marrow::Skeleton must be initialized.
	 *
	 * @param execPlatform the execution platform
	 */
	StaticAutoTuner(unsigned int numDevices, unsigned int nOverlapPartitions);

	/**
	 * Class destructor
	 */
	~StaticAutoTuner();

	virtual std::vector<std::shared_ptr<PartitionedInfo>> * getInputPartitions(const std::vector<std::shared_ptr<IWorkData>> &inputData,
																			const std::vector<std::vector<std::vector<size_t>>> &workSizes);

	virtual std::vector<std::shared_ptr<PartitionedInfo>> * getOutputPartitions(const std::vector<std::shared_ptr<IWorkData>> &outputData,
																			const std::vector<std::vector<std::vector<size_t>>> &workSizes);

	virtual std::shared_ptr<std::vector<std::vector<std::vector<size_t>>>> getWorkSizes(const std::vector<unsigned int> &globalWorkSize,
																			const std::vector<size_t> &localWorkSize,
																			const bool isDoublePrecision,
																			const unsigned long staticMemSize,
																			const unsigned long dynamicMemSize);

protected:
	// Number of devices.
	unsigned int numDevices;

	// Number of overlapping partitions.
	unsigned int numOverlapPartitions;

	// Single precision ratios.
	std::vector<double> ratiosS;
	// Double precision ratios.
	std::vector<double> ratiosD;

	/**
	 * Sets the ratios from the PerfInfo class.
	 */
	virtual void setRatios();
};

#endif /* StaticAutoTuner_HPP_ */
