/*
 * PerfInfo.hpp
 *
 *  June 2013:
 *		Fernando Alexandre
 *		Definition of the required empiric information about the hardware
 *
 *  Created on: 13 de Jun de 2013
 *      Author: Fernando Alexandre
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef PERFINFO_HPP_
#define PERFINFO_HPP_

/**
 * Class which holds the performance values of the GPU devices.
 */
class PerfInfo {
public:
	// Array with the double-precision FLOPs performance.
	static double maxDFLOPs[];
	// Array with the single-precision FLOPs performance.
    static double maxSFLOPs[];
    // Array with the PCIe bus upload transfer speed (currently unused).
	static double uploadSpeed[];
    // Array with the PCIe bus download transfer speed (currently unused).
	static double downloadSpeed[];
};

#endif /*PERFINFO_HPP_ */
