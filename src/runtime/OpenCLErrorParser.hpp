/*
 * OpenCLErrorParser.hpp
 *
 *  February 2012:
 *		Ricardo Marques
 *		Creation of the OpenCL error parser
 *
 *  Created on: 23 de Fev de 2012
 *      Author: Ricardo Marques
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef OPENCLERRORPARSER_HPP_
#define OPENCLERRORPARSER_HPP_

#include <string>
#include "IErrorParser.hpp"

/**
 * Class that defines an implementation of the IErrorParser interface
 */
class OpenCLErrorParser: public IErrorParser {
public:
	/**
	 * Default constructor
	 */
	OpenCLErrorParser();

	/**
	 * Class destructor
	 */
	~OpenCLErrorParser();

	static const char* errorString(int error);
};

#endif /* OPENCLERRORPARSER_H_ */
