/*
 * StaticAutoTuner.cpp
 *
 *  Created on: 24 de Fev de 2013
 *      Author: Fernando Alexandre
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
#include "StaticAutoTuner.hpp"

#include "marrow/Skeleton.hpp"
#include "marrow/BufferData.hpp"
#include "marrow/runtime/PartitionedInfo.hpp"
#include "marrow/utils/Debug.hpp"

#include <memory>
#include <climits>

#ifdef __APPLE__
	#include <OpenCL/cl.h>
#else
	#include <CL/cl.h>
#endif


StaticAutoTuner::StaticAutoTuner(unsigned int nDevices, unsigned int nOverlapPartitions) :
numDevices(nDevices),
numOverlapPartitions(nOverlapPartitions) {
	setRatios();
}

StaticAutoTuner::~StaticAutoTuner() {}

std::vector<std::shared_ptr<PartitionedInfo>> * StaticAutoTuner::getInputPartitions(const std::vector<std::shared_ptr<IWorkData>> &inputData, const std::vector<std::vector<std::vector<size_t>>> &workSizes) {
	unsigned int i, j, w, overlapPartitionBegin = 0;
	unsigned long elemSize, nElems;
	std::shared_ptr<IWorkData> elem;
	std::vector<std::shared_ptr<PartitionedInfo>> * partitions = new std::vector<std::shared_ptr<PartitionedInfo>>(inputData.size());
	unsigned int numDimensions = workSizes[0][0].size();

	DEBUG_MSG("inputSize = %u; numDevices = %u; numOverlapPartitions = %u", inputData.size(), numDevices, numOverlapPartitions);

	for (i = 0; i < inputData.size(); i++) {
		elem = inputData[i];
		IWorkData::dataType type = (*elem).getDataType();

		if(type == IWorkData::BUFFER || type == IWorkData::IMAGE2D) {
			nElems = (*elem).getWidth() * (*elem).getHeight() * (*elem).getDepth();
			elemSize = (*elem).getMemSize() / nElems;
			std::shared_ptr<PartitionedInfo> partition (new PartitionedInfo(numDevices, numOverlapPartitions));

			for (j = 0; j < numDevices; j++) {
				// If there was no space on the device.
				if(workSizes[j][0][numDimensions-1] == 0) {
					DEBUG_MSG("Skipping partition on device %u", j);
					continue;
				}

				// Create partitions for overlapping.
				for(w = 0; w < numOverlapPartitions; w++) {
					PartitionedInfo::DataSegment currSeg;

					// If it is to be partitioned
					if((*elem).getPartitionMode() == IWorkData::PARTITIONABLE || (*elem).getPartitionMode() == IWorkData::COPY) {
						if((*elem).getPartitionMode() == IWorkData::COPY) {
							currSeg.begin_cpy = 0;
						} else {
							currSeg.begin_cpy = overlapPartitionBegin;
						}

						currSeg.begin_comp = overlapPartitionBegin;
						currSeg.size = workSizes[j][w][numDimensions-1] * (*elem).getIndivisibleSize();

						//DEBUG_MSG("%lu * %u", workSizes[j][w][numDimensions-1], (*elem).getIndivisibleSize());

						overlapPartitionBegin += currSeg.size;

						DEBUG_MSG("[%u][%u][%u] begin = %u/%u; size = %u; end = %u", i, j, w, currSeg.begin_comp, currSeg.begin_cpy, currSeg.size, currSeg.begin_comp + currSeg.size);

						// CAUTION: Copy mode allocated a whole set for each overlap.
						if((*elem).getPartitionMode() == IWorkData::COPY) {
							currSeg.segmentInfo = static_cast<IWorkData *>(new marrow::BufferData<char>(elem->getMemSize(), (*elem).getAccessMode(), (*elem).getPartitionMode()));
						} else {
							currSeg.segmentInfo = static_cast<IWorkData *>(new marrow::BufferData<char>(elemSize * currSeg.size, (*elem).getAccessMode(), (*elem).getPartitionMode()));
						}

						DEBUG_MSG("segmentInfoSize = %lu; inputData[%u].Size = %lu", currSeg.segmentInfo->getMemSize(), i, elem->getMemSize());
					} else { // Just add the same memory position to all partitions
						currSeg.begin_cpy = 0;
						currSeg.begin_comp = 0;
						currSeg.size = (*elem).getWidth() * (*elem).getHeight() * (*elem).getDepth();

						overlapPartitionBegin += currSeg.size;

						DEBUG_MSG("[%u][%u][%u] begin = %u/%u; size = %u; end = %u", i, j, w, currSeg.begin_comp, currSeg.begin_cpy, currSeg.size, currSeg.begin_comp + currSeg.size);

						currSeg.segmentInfo = &(*elem);
						DEBUG_MSG("Added a non-partitioned segment%s", ".");
					}
					(*partition).setSegment(currSeg, j, w);
				}
			}

			(*partitions)[i] = partition;
		}

		overlapPartitionBegin = 0;
	}

	return partitions;
}

std::vector<std::shared_ptr<PartitionedInfo>> * StaticAutoTuner::getOutputPartitions(const std::vector<std::shared_ptr<IWorkData>> &outputData, const std::vector<std::vector<std::vector<size_t>>> &workSizes) {
	DEBUG_MSG("Starting getOuputPartitions%s", ".");
	return getInputPartitions(outputData, workSizes);
}

void StaticAutoTuner::setRatios() {
	unsigned int i;
	double totalSFLOPs = 0.0;
	double totalDFLOPs = 0.0;

	ratiosD.resize(numDevices);
	ratiosS.resize(numDevices);


	for(i = 0; i < numDevices; i++) {
		totalDFLOPs += PerfInfo::maxDFLOPs[i];
		totalSFLOPs += PerfInfo::maxSFLOPs[i];
	}

	for(i = 0; i < numDevices; i++) {
		ratiosD[i] = PerfInfo::maxDFLOPs[i] / totalDFLOPs;
		ratiosS[i] = PerfInfo::maxSFLOPs[i] / totalSFLOPs;

	}
}

std::shared_ptr<std::vector<std::vector<std::vector<size_t>>>> StaticAutoTuner::getWorkSizes(const std::vector<unsigned int> &globalWorkSize, const std::vector<size_t> &localWorkSize, const bool isDoublePrecision, const unsigned long staticMemSize, const unsigned long dynamicMemSize) {
	unsigned int i, j, w;
	unsigned int partitionSize = 0;
	unsigned int acc = 0;
	std::shared_ptr<std::vector<std::vector<std::vector<size_t>>>> x (new std::vector<std::vector<std::vector<size_t>>>(numDevices));
	std::vector<unsigned long> maxNumElems(numDevices);
	unsigned int numAvailableDevices = 0;
	double remainingRatio = 0.0;

	std::vector<double> defaultRatios = isDoublePrecision ? ratiosD : ratiosS;
	std::vector<double> ratios(defaultRatios.size());

	// Calculate Maximum number of work items in each GPU
	// Check if any of the GPUs is full (can't hold a single work-item).

	for (i = 0; i < numDevices; i++) {
		if((*marrow::Skeleton::executionPlatform).getFreeMemorySize(i) > staticMemSize + dynamicMemSize) {
			if(dynamicMemSize > 0) {
				maxNumElems[i] = ((*marrow::Skeleton::executionPlatform).getFreeMemorySize(i) - staticMemSize) / dynamicMemSize;
				DEBUG_MSG("(%lu - %lu) / %lu = %lu", (*Skeleton::executionPlatform).getFreeMemorySize(i), staticMemSize, dynamicMemSize, maxNumElems[i]);
			} else {
				// If there is no dynamic limit and only static there is an unlimited number of possible workitems
				maxNumElems[i] = ULONG_MAX;
			}
			ratios[i] = defaultRatios[i];
			numAvailableDevices++;
		} else {
			DEBUG_MSG("GPU #%u full!", i);
			maxNumElems[i] = 0;
			remainingRatio += defaultRatios[i];
			ratios[i] = 0;
		}
	}

	// Adapt Ratios to include the extra from full GPUs.
	if(numAvailableDevices < ratios.size()) {
		for(i = 0; i < numDevices; i++) {
			if(ratios[i] != 0) {
				DEBUG_MSG("Adding %.3f to ratios[%u] = %.3f", ratios[i] * remainingRatio, i, ratios[i]);
				ratios[i] += ratios[i] * remainingRatio;
			}
		}
	}

	// Stage 1: Create the partitions while taking into account the maximum memory available.
	for (i = 0; i < numDevices; i++) {
		(*x)[i].resize(numOverlapPartitions);

		for(w = 0; w < numOverlapPartitions; w++) {
			(*x)[i][w].resize(globalWorkSize.size());

			for (j = 0; j < globalWorkSize.size(); j++) {
				// It's the dimension we want to partition
				if(j == globalWorkSize.size() - 1) {
					partitionSize = globalWorkSize[j] * ratios[i];

					// Take into account the maximum number of elements
					// Memory in the GPU is gonna become full. Won't happen too often, I hope.
					// If this DOES happen, it will only work if it is the last computation.
					if(partitionSize > maxNumElems[i]) {
						DEBUG_MSG("partitionSize %lu > maxNumElems[%u] %lu", partitionSize, i, maxNumElems[i]);
						if(localWorkSize.size() > 0) {
							partitionSize = maxNumElems[i] - (maxNumElems[i] % localWorkSize[j]);
						} else {
							partitionSize = maxNumElems[i];
						}
					}

					/*if(ratios[i] > biggestRatio) {
						biggestRatio = ratios[i];
						biggestRatioIndex = i;
					}*/

					(*x)[i][w][j] = partitionSize / numOverlapPartitions;

					// Take into account the remainder of the integer division of the partitionSize/numOverlapPartitions.
					if(w == numOverlapPartitions - 1) {
						(*x)[i][w][j] += partitionSize - (numOverlapPartitions * (partitionSize / numOverlapPartitions));
						DEBUG_MSG("Adding %u to the partitionSize", partitionSize - (numOverlapPartitions * (partitionSize / numOverlapPartitions)));
						// Take into account the remainder of the integer division of the partitionSize
					}

					if(localWorkSize.size() > 0) {
						if((*x)[i][w][j] % localWorkSize[j] != 0) {
							(*x)[i][w][j] -= (*x)[i][w][j] % localWorkSize[j];
						}
					}

					acc += (*x)[i][w][j];
					DEBUG_MSG("maxNumElems[%u] %lu -= (*x)[%u][%u][%u] %lu", i, maxNumElems[i], i, w, j, (*x)[i][w][j]);
					maxNumElems[i] -= (*x)[i][w][j];
				} else {
					(*x)[i][w][j] = globalWorkSize[j];
				}
				DEBUG_MSG("Stage1: x[%u][%u][%u] = %lu", i, w, j, (unsigned long int) (*x)[i][w][j]);
			}
		}
	}

	// Step 2: Redistribute the excess according to the ratios and memory limits (maxNumElems).
	j = globalWorkSize.size() - 1;
	if(acc < globalWorkSize[j]) {
		for (i = 0; i < numDevices; i++) {
			for(w = 0; w < numOverlapPartitions; w++) {
				partitionSize = ((globalWorkSize[j] - acc) * ratios[i]) / numOverlapPartitions;

				// Add a multiple of the Local Work Size that fits in the maxNumElems

				if(partitionSize > maxNumElems[i]) {
					if(localWorkSize.size() > 0) {
						partitionSize = maxNumElems[i] - (maxNumElems[i] % localWorkSize[j]);
					} else {
						partitionSize = maxNumElems[i];
					}
				} else {
					if(localWorkSize.size() > 0) {
						partitionSize -= (partitionSize % localWorkSize[j]);
					}
				}

				(*x)[i][w][j] += partitionSize;
				acc += partitionSize;
				maxNumElems[i] -= partitionSize;

				DEBUG_MSG("Stage2: x[%u][%u][%u] = %lu", i, w, j, (unsigned long int) (*x)[i][w][j]);
			}
		}
	}

	// If for _some_ reason there is still left overs; set them to the first device with space
	if(acc < globalWorkSize[j]) {
		for (i = 0; i < numDevices; i++) {
			if(maxNumElems[i] > (globalWorkSize[j] - acc)) {
				(*x)[i][0][j] += (globalWorkSize[j] - acc);
				acc += (globalWorkSize[j] - acc);
				DEBUG_MSG("Stage3: x[%u][0][%u] = %lu", i, j, (unsigned long int) (*x)[i][0][j]);
			}
		}
	}

	//This shouldn't happen, ever.
	if(acc != globalWorkSize[j]) {
		ERROR_MSG("Partitioning Error acc = %lu globalWorkSize[%u] = %lu", acc, j, globalWorkSize[j]);
	}

	return x;
}
