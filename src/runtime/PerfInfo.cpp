#include "PerfInfo.hpp"

/* This is the template for the creation of the PerfInfo.cpp
 * These arrays hold the performance values of the computation devices used
 * for the calculation of performance ratio among the devices.
 * 
 * maxDFLOPs - Double-precision performance of each device
 * maxSFLOPs - Single-precision performance of each device
 * uploadSpeed - Upload speed of for each device
 * downloadSpeed - Download speed of for each device 
 *
 */

double PerfInfo::maxDFLOPs[1] = {1000};
double PerfInfo::maxSFLOPs[1] = {1000};
double PerfInfo::uploadSpeed[1] = {1000};
double PerfInfo::downloadSpeed[1] = {1000};

