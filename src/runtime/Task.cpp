/*
 * Task.cpp
 *
 *  Created on: 10 de Mar de 2013
 *      Author: Fernando Alexandre
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
#include "marrow/runtime/Task.hpp"

Task::Task(marrow::Skeleton* skel,
	const unsigned int unique_id,
	std::vector<std::shared_ptr<marrow::Vector>> *iData,
	std::vector<std::shared_ptr<marrow::Vector>> *oData,
	marrow::IFuture* f) :
	computation(skel),
	uniqueId(unique_id),
	inputData(iData),
	outputData(oData),
	future(f),
	isCompleted(false),
	numComputed(0) {}


Task::~Task() {}

bool Task::hasFinished() {
	return numComputed == numPartitions * numOverlapPartitions;
}

void Task::incFinished() {
	numComputed++;
}


marrow::Skeleton* Task::getComputation() {
	return computation;
}

std::vector<std::shared_ptr<marrow::Vector>> * Task::getInputData() {
	return inputData;
}

std::vector<std::shared_ptr<marrow::Vector>> * Task::getOutputData() {
	return outputData;
}

marrow::IFuture* Task::getFuture() {
	return future;
}

unsigned int Task::getUniqueId() {
	return uniqueId;
}

void Task::setNumPartitions(unsigned int numPartitions) {
	this->numPartitions = numPartitions;
}

void Task::setNumOverlapPartitions(unsigned int numOverlapPartitions) {
	this->numOverlapPartitions = numOverlapPartitions;
}

boost::mutex * Task::getTaskMutex() {
	return &taskMutex;
}

