/*
 * PartitionedInfo.cpp
 *
 *  Created on: 3 de Apr de 2013
 *      Author: Fernando Alexandre
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
#include "marrow/runtime/PartitionedInfo.hpp"

PartitionedInfo::PartitionedInfo(const unsigned int nPartitions, const unsigned int numOverlappedPartitions) :
numOverlapPartitions(numOverlappedPartitions),
numPartitions(nPartitions)
{
	segments.reserve(nPartitions);

	for(unsigned int i = 0; i < numPartitions; i++) {
		segments.push_back(std::vector<DataSegment>(numOverlappedPartitions));
	}
}

PartitionedInfo::~PartitionedInfo() {
	unsigned int i, j;

	for (i = 0; i < segments.size(); i++) {
		for(j = 0; j < segments[i].size(); j++) {
			delete segments[i][j].segmentInfo;
		}
	}
}

unsigned int PartitionedInfo::getNumPartitions() {
	return numPartitions;
}

unsigned int PartitionedInfo::getNumOverlapPartitions() {
	return numOverlapPartitions;
}

PartitionedInfo::DataSegment PartitionedInfo::getSegment(const unsigned int deviceIndex, const unsigned int segIndex) {
	return segments[deviceIndex][segIndex];
}

void PartitionedInfo::setSegment(const DataSegment segment, const unsigned int deviceIndex, const unsigned int segIndex) {
	segments[deviceIndex][segIndex] = segment;
}
