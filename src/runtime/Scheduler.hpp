/*
 * Scheduler.hpp
 *
 *  February 2013:
 *       Fernando Alexandre
 *       Creation of the Scheduler concept
 *
 *  Created on: 24 de Fev de 2013
 *      Author: Fernando Alexandre
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef __MARROW_RUNTIME_SCHEDULER_HPP__
#define __MARROW_RUNTIME_SCHEDULER_HPP__

#include <vector>
#include <string>
#include <memory>
#include <list>

#include "marrow/runtime/IScheduler.hpp"

#include "marrow/Skeleton.hpp"

#include "TaskLauncher.hpp"
#include "IAutoTuner.hpp"

#define DEFAULT_OVERLAP_PARTITIONS 2

namespace marrow {

    namespace runtime {

        /**
         * Class that defines an implementation of the marrow::marrow::IScheduler interface, representing
         * the Task Scheduler, responsible for the configuration and submission of task to the
         * Task queue.
         */
        class Scheduler: public marrow::runtime::IScheduler {
        public:
            /**
             * Default constructor. Probes the system to find out the available GPUs
             * within the system. Uses the default number of overlapping partitions (defined
             * in marrow::marrow::IScheduler).
             */
            Scheduler();

            /**
             * Creates a Scheduler with a predetermined number of devices, and
             * a default number of overlapping partitions (defined in marrow::marrow::IScheduler).
             *
             * @param numDevices Number of devices to be used.
             */
            Scheduler(const unsigned int numDevices);

            /**
             * Creates a Scheduler with a predetermined number of devices and number of overlapping
             * partitions.
             *
             * @param numDevices Number of devices to be used.
             * @param numOverlapPartitions number of overlapping partitions.
             */
            Scheduler(const unsigned int numDevices, const unsigned int numOverlapPartitions);

            /**
             * Class destructor
             */
            ~Scheduler();

            virtual void submit(std::shared_ptr<Task> task);

            virtual void setPartitions(marrow::Skeleton* s, bool isReconfigure);

            virtual unsigned int getNumOverlapPartitions();

            virtual unsigned int getNumDevices();

        protected:
            // The task launcher which actually launches executions.
            std::unique_ptr<TaskLauncher> taskLauncher;

            // The auto-tuner.
            std::unique_ptr<IAutoTuner> autotuner;

            // A list of all tasks, used to free finished tasks.
            std::list<std::shared_ptr<Task>> allTasks;

            // The shared task queues.
            std::shared_ptr<std::vector<std::list<TaskEntry>>> gpuQueues;

            // The mutexes used for synchronized access to the task queues.
            std::shared_ptr<boost::mutex> * queuesMutex;

            // The condition used for when the queue is empty (to wait).
            boost::condition_variable_any * emptyQueue;

            // The number of devices configured.
            unsigned int numDevices;

            // The number of overlapping partitions.
            unsigned int numOverlapPartitions;

            /**
             * Initializes the scheduler.
             *
             * @param numDevices - The number of devices to be used.
             * @param numOverlapPartitions - The number of overlapping partitions within each device.
             */
            virtual void init(const int numDevices, const unsigned int numOverlapPartitions);

            /**
             * Clears all the finished tasks from allTasks.
             */
            virtual void cleanFinishedTasks();
        };
    }
}
#endif /* __MARROW_RUNTIME_SCHEDULER_HPP__ */
