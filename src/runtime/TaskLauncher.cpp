/*
 * TaskLauncher.cpp
 *
 *  Created on: 1 de Mar de 2013
 *      Author: Fernando Alexandre
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
 
#include "TaskLauncher.hpp"

#include "marrow/Skeleton.hpp"
#include "marrow/exceptions/NotEnoughMemoryException.hpp"
#include "marrow/utils/Debug.hpp"

marrow::runtime::TaskLauncher::TaskLauncher(const std::shared_ptr<std::vector<std::list<TaskEntry>>> queues, std::shared_ptr<boost::mutex> * qMutexes, boost::condition_variable_any *emptyCond, const unsigned int numberDevices, const unsigned int nOverlapPartitions) :
gpuQueues(queues),
queuesMutex(qMutexes),
emptyQueue(emptyCond),
numDevices(numberDevices),
numOverlapPartitions(nOverlapPartitions) {
	unsigned int i, j;

	closed = false;
	workers = new boost::thread[numDevices * numOverlapPartitions];

	this->queues.resize(numDevices);

	for (i = 0; i < numDevices; i++) {
		this->queues[i].resize(numOverlapPartitions);
		for(j = 0; j < numOverlapPartitions; j++) {
			DEBUG_MSG("Creating Command Queue for Device #%u and OverlapPartition #%u", i, j);
			this->queues[i][j] = (*marrow::Skeleton::executionPlatform).createCommandQueue(i, j);
		}
	}

	unsigned int currDevice = 0;
	for(i = 0; i < numDevices*numOverlapPartitions; i++) {
		if(i % numOverlapPartitions == 0 && i != 0) {
			currDevice++;
		}
		workers[i] = boost::thread(&marrow::runtime::TaskLauncher::run, this, currDevice);
	}
}

marrow::runtime::TaskLauncher::~TaskLauncher() {
	unsigned int i, j;
	closed = true;

	for(i = 0 ; i < numDevices; i++) {
		emptyQueue[i].notify_all();
	}

	for(i = 0; i < numDevices * numOverlapPartitions; i++) {
		workers[i].join();
	}

	for(i = 0; i < queues.size(); i++) {
		for(j = 0; j < queues[i].size(); j++) {
			clReleaseCommandQueue(queues[i][j]);
		}
	}

	for(i = 0; i < numDevices; i++) {
		queuesMutex[i].reset();
	}
	gpuQueues.reset();
	delete[] workers;
}

unsigned int marrow::runtime::TaskLauncher::getNumDevices() {
	return numDevices;
}

boost::optional<TaskEntry> marrow::runtime::TaskLauncher::popTask(const unsigned int deviceIndex) {
	boost::unique_lock<boost::mutex> queuesLock(*(queuesMutex[deviceIndex]));

	while((*gpuQueues)[deviceIndex].empty()) {
		if(closed) {
			DEBUG_MSG("Freed queueMutex (closed) @ deviceIndex = %u", deviceIndex);
			return boost::optional<TaskEntry>();
		}
		DEBUG_MSG("Waiting on emptyQueue @ deviceIndex = %u", deviceIndex);
		emptyQueue[deviceIndex].wait(queuesLock);
	}

	TaskEntry taskEntry = (*gpuQueues)[deviceIndex].front();

	(*gpuQueues)[deviceIndex].pop_front();

	DEBUG_MSG("Task popped @ deviceIndex = %u", deviceIndex);
	return boost::optional<TaskEntry>(taskEntry);
}

void marrow::runtime::TaskLauncher::run(const unsigned int deviceIndex) {
	while(!closed) {
		// prompting device execution
		boost::optional<TaskEntry> taskEntry = popTask(deviceIndex);
		if(taskEntry) {
			execute(taskEntry.get(), deviceIndex);
		}
	}
}

void marrow::runtime::TaskLauncher::execute(const TaskEntry &taskEntry, const unsigned int deviceIndex) {
	unsigned int partitionIndex = taskEntry.partitionIndex;
	unsigned int overlapPartition = taskEntry.overlapPartitionIndex;
	std::shared_ptr<Task> task = taskEntry.task;
	unsigned int uniqueId = (*task).getUniqueId();
	marrow::Skeleton* computation = (*task).getComputation();
	std::vector<cl_mem> * inputMem = (*computation).getInputMem(partitionIndex, overlapPartition);
	std::vector<cl_mem> * outputMem = (*computation).getOutputMem(partitionIndex, overlapPartition);
	cl_command_queue q = queues[deviceIndex][overlapPartition];
	std::vector<std::shared_ptr<marrow::Vector>> * inputData = (*task).getInputData();

	// There was no space on this device.
	if(!(inputMem->size() == 0 && outputMem->size() == 0)) {
		DEBUG_MSG("Launching execution @ deviceIndex = %u; partitionIndex = %u; overlapPartition = %u", deviceIndex, partitionIndex, overlapPartition);
		if((*inputData).size() > 0) {
			(*computation).uploadInputData(q,
									deviceIndex,
									uniqueId,
									partitionIndex,
									overlapPartition,
									*inputData,
									*inputMem);
			//clFlush(q);
			DEBUG_MSG("Added upload @ deviceIndex = %u; partitionIndex = %u; overlapPartition = %u", deviceIndex, partitionIndex, overlapPartition);

			for(unsigned int i = 0; i < (*inputData).size(); i++) {
				(*inputData)[i]->setUpdated();
			}
		}

		(*computation).executeSkel(q,
									deviceIndex,
									uniqueId,
									partitionIndex,
									overlapPartition,
									*(*task).getInputData(),
									*(*task).getOutputData(),
									*inputMem,
									*outputMem);
		//clFlush(q);
		DEBUG_MSG("Added execution @ deviceIndex = %u; partitionIndex = %u; overlapPartition = %u", deviceIndex, partitionIndex, overlapPartition);

		(*computation).downloadOutputData(q,
									deviceIndex,
									uniqueId,
									partitionIndex,
									overlapPartition,
									*(*task).getOutputData(),
									*outputMem);
		DEBUG_MSG("Added download @ deviceIndex = %u; partitionIndex = %u; overlapPartition = %u", deviceIndex, partitionIndex, overlapPartition);

		clFinish(q);
	}

	{
		boost::lock_guard<boost::mutex> lock(*(*task).getTaskMutex());

		(*task).incFinished();

		if((*task).hasFinished()) {
			DEBUG_MSG("Task %p has finished.", &task);
			std::vector<std::shared_ptr<marrow::Vector>> * outputData = (*task).getOutputData();
			(*computation).finishExecution(uniqueId, *outputData);
			(*task).getFuture()->setData(outputData);
		}
	}
}
