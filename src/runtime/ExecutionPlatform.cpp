/*
 * ExecutionPlatform.cpp
 *
 *  Created on: April 3rd, 2012
 *      Author: Ricardo Marques
 *			Modified by Fernando Alexandre
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "ExecutionPlatform.hpp"

#include "marrow/exceptions/NoOpenCLDeviceException.hpp"
#include "marrow/exceptions/ContextCreationException.hpp"
#include "marrow/exceptions/MemoryAllocationException.hpp"
#include "marrow/utils/Debug.hpp"

#ifdef __APPLE__
	#include <OpenCL/cl.h>
#else
	#include <CL/cl.h>
#endif


void reportError(const char *errinfo, const void *private_info, size_t cb, void *user_data) {
	UNUSED(errinfo);
	UNUSED(private_info);
	UNUSED(cb);
	UNUSED(user_data);
	ERROR_MSG("OpenCL Error (via pfn_notify): %s", errinfo);
}

ExecutionPlatform::ExecutionPlatform() : devices(0), isInit(false) {}

void ExecutionPlatform::initialize(const std::vector<cl_device_type> &types, unsigned int numOverlap) {
	unsigned int i, j, count;
	int errcode;
	cl_platform_id platformId;
	cl_uint bla;
	errcode = clGetPlatformIDs(1, &platformId, &bla);

	// Matrix of devices, where dimension 1 is the device type, and dimension two are the device identifiers
	// The type order is the same as defined in the @types vector argument
	std::vector<std::vector<cl_device_id>> availableDevices(types.size());
	cl_uint totalDevicesAvailable = 0;
	cl_uint numDevs = 0;

	for(i = 0; i < types.size(); i++) {
		cl_uint numDevicesAvailable = 0;
		availableDevices[i].resize(10);
		// Obtain up to @numDevices[i] devices of type @types[i] from the platform
		errcode = clGetDeviceIDs(platformId, types[i], 10, availableDevices[i].data(), &numDevicesAvailable);
		// If the user wants more devices than the available ones...
		numDevs = numDevicesAvailable;
		availableDevices[i].resize(numDevs);
		numDevicePerType[types[i]] = numDevs;
		// Total devices successfully obtained
		totalDevicesAvailable += numDevs;
	}

	if(totalDevicesAvailable == 0) {
		throw marrow::NoOpenCLDeviceException();
	}

	// Store devices sequentially, independently of their type
	devices.resize(totalDevicesAvailable);
	for(i = 0, count = 0; i < availableDevices.size(); i++) {
		for(j = 0; j < availableDevices[i].size(); j++) {
			devices[count] = availableDevices[i][j];
			count++;
		}
	}

	context.resize(devices.size());
	for(i = 0; i < devices.size(); i++) {
		context[i].reserve(numOverlap);
		const cl_device_id currDevice = devices.data()[i];

		for(j = 0; j < numOverlap; j++) {
			context[i].push_back(clCreateContext(0, 1, &currDevice, &reportError, NULL, &errcode));

			if(errcode != CL_SUCCESS) {
				throw marrow::ContextCreationException(parser.errorString(errcode));
			}
		}
	}

	deviceMemoryCapacity.resize(devices.size());
	deviceFreeMem.resize(devices.size());
	deviceTotalFreeMem = 0;

	for(i = 0; i < devices.size(); i++) {
		cl_ulong memSize = 0;
		errcode = clGetDeviceInfo(devices[i], CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(cl_ulong), &memSize, NULL);
		if(errcode != CL_SUCCESS) {
			ERROR_MSG("%s", parser.errorString(errcode));
		} else {
			deviceMemoryCapacity[i] = memSize;
			deviceFreeMem[i] = memSize;
			deviceTotalFreeMem += memSize;
		}
	}

	isInit = true;
}

void ExecutionPlatform::initialize(const std::vector<unsigned int> &numDevices, const std::vector<cl_device_type> &types, unsigned int numOverlap) {
	unsigned int i, j, count;
	int errcode;
	cl_platform_id platformId;
	errcode = clGetPlatformIDs(1, &platformId, NULL);

	// Matrix of devices, where dimension 1 is the device type, and dimension two are the device identifiers
	// The type order is the same as defined in the @types vector argument
	std::vector<std::vector<cl_device_id>> availableDevices(numDevices.size());
	cl_uint totalDevicesAvailable = 0;
	cl_uint numDevs = 0;

	for(i = 0; i < numDevices.size(); i++) {
		cl_uint numDevicesAvailable = 0;
		availableDevices[i].resize(numDevices[i]);
		// Obtain up to @numDevices[i] devices of type @types[i] from the platform
		errcode = clGetDeviceIDs(platformId, types[i], numDevices[i], availableDevices[i].data(), &numDevicesAvailable);
		// If the user wants more devices than the available ones...
		if(numDevices[i] > numDevicesAvailable) {
			numDevs = numDevicesAvailable;
		}
		else {
			numDevs = numDevices[i];
		}
		availableDevices[i].resize(numDevs);
		numDevicePerType[types[i]] = numDevs;
		// Total devices successfully obtained
		totalDevicesAvailable += numDevs;
	}

	if(totalDevicesAvailable == 0) {
		throw marrow::NoOpenCLDeviceException();
	}

	// Store devices sequentially, independently of their type
	devices.resize(totalDevicesAvailable);
	for(i = 0, count = 0; i < availableDevices.size(); i++) {
		for(j = 0; j < availableDevices[i].size(); j++) {
			devices[count] = availableDevices[i][j];
			count ++;
		}
	}

	context.resize(devices.size());
	for(i = 0; i < devices.size(); i++) {
		context[i].reserve(numOverlap);
		const cl_device_id currDevice = devices.data()[i];

		for(j = 0; j < numOverlap; j++) {
			context[i].push_back(clCreateContext(0, 1, &currDevice, &reportError, NULL, &errcode));

			if(errcode != CL_SUCCESS) {
				throw marrow::ContextCreationException(parser.errorString(errcode));
			}
		}
	}

	deviceMemoryCapacity.resize(devices.size());
	deviceFreeMem.resize(devices.size());
	deviceTotalFreeMem = 0;

	for(i = 0; i < devices.size(); i++) {
		cl_ulong memSize = 0;
		errcode = clGetDeviceInfo(devices[i], CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(cl_ulong), &memSize, NULL);
		if(errcode != CL_SUCCESS) {
			ERROR_MSG("%s", parser.errorString(errcode));
		} else {
			deviceMemoryCapacity[i] = memSize;
			deviceFreeMem[i] = memSize;
			deviceTotalFreeMem += memSize;
		}
	}

	isInit = true;
}

ExecutionPlatform::~ExecutionPlatform() {
	for(unsigned int i = 0; i < context.size(); i++) {
		for(unsigned int j = 0; j < context[i].size(); j++) {
			clReleaseContext(context[i][j]);
		}
	}
}

cl_context ExecutionPlatform::getPlatformContext(unsigned int deviceIndex, unsigned int overlapIndex) {
	return context[deviceIndex][overlapIndex];
}

std::vector<std::vector<cl_context>> * ExecutionPlatform::getAllPlatformContexts() {
	return &context;
}


unsigned int ExecutionPlatform::getNumDevices() {
	return devices.size();
}

void ExecutionPlatform::getDevices(std::vector<cl_device_id> &devices) {
	unsigned int i;
	unsigned numDevices = 0;
	// Cannot return more devices than the ones associated to the context
	if(this->devices.size() < devices.size()) {
		numDevices = this->devices.size();
	}
	else {
		numDevices = devices.size();
	}
	for( i = 0; i < numDevices ; i++ ) {
		devices[i] = this->devices[i];
	}
}

void ExecutionPlatform::getDevicesOfType(cl_device_type type, std::vector<cl_device_id> &devices) {
	int errcode;
	unsigned int i;
	cl_device_id* _devices = (cl_device_id*) calloc(sizeof(cl_device_id), 32);
	cl_uint number_devices = 0;

	errcode = clGetDeviceIDs(platformId, type, 32, _devices, &number_devices);

	if(errcode != CL_SUCCESS) {
		ERROR_MSG("%s", parser.errorString(errcode));
		//std::cerr << parser.errorString(errcode) << std::endl;
		throw;
	}

	devices.resize(number_devices);

	for(i = 0; i < number_devices; i++) {
		devices.push_back(_devices[i]);
	}
}

unsigned int ExecutionPlatform::getNumDevicesOfType(cl_device_type type) {
	return numDevicePerType[type];
}

cl_device_type ExecutionPlatform::getType(unsigned int deviceIndex) {
	if(deviceIndex > devices.size()-1) {
		return 0;
	}
	int errcode;
	cl_device_type type;
	errcode = clGetDeviceInfo(devices[deviceIndex], CL_DEVICE_TYPE, sizeof(cl_device_type), &type, NULL);
	if(errcode != CL_SUCCESS) {
		ERROR_MSG("%s", parser.errorString(errcode));
		return 0;
	}
	return type;
}

cl_ulong ExecutionPlatform::getGlobalMemorySize(unsigned int deviceIndex) {
	if(deviceIndex > devices.size()-1) {
		ERROR_MSG("Asked for deviceIndex #%u when number devices = %u", deviceIndex, devices.size()-1);
		return 0;
	}

	return deviceMemoryCapacity[deviceIndex];
}

cl_ulong ExecutionPlatform::getTotalFreeMemorySize() {
	return deviceTotalFreeMem;
}

cl_ulong ExecutionPlatform::getFreeMemorySize(unsigned int deviceIndex) {
	if(deviceIndex > devices.size()-1) {
		ERROR_MSG("Asked for deviceIndex #%u when number devices = %u", deviceIndex, devices.size()-1);
		return 0;
	}

	return deviceFreeMem[deviceIndex];
}

void ExecutionPlatform::maxWorkgroupLenghtPerDim(unsigned int deviceIndex, std::vector<size_t> &localSize) {
	if(deviceIndex > devices.size()-1) {
		return;
	}
	cl_uint maxDim;
	int errcode;
	maxDim = maxDimensions(deviceIndex);
	// Resize placeholder to the maximum number of supported dimensions
	localSize.resize(maxDim);
	errcode = clGetDeviceInfo(devices[deviceIndex], CL_DEVICE_MAX_WORK_ITEM_SIZES, sizeof(size_t)*maxDim, localSize.data(), NULL);
	if(errcode != CL_SUCCESS) {
		ERROR_MSG("%s", parser.errorString(errcode));
	}
}

size_t ExecutionPlatform::maxWorkgroupLenght(unsigned int deviceIndex) {
	if(deviceIndex > devices.size()-1) {
		return 0;
	}
	size_t maxWGSize;
	int errcode;
	errcode = clGetDeviceInfo(devices[deviceIndex], CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(size_t), &maxWGSize, NULL);
	if(errcode != CL_SUCCESS) {
		ERROR_MSG("%s", parser.errorString(errcode));
		return -1;
	}
	return maxWGSize;
}

cl_uint ExecutionPlatform::maxDimensions(unsigned int deviceIndex) {
	if(deviceIndex > devices.size()-1) {
		return 0;
	}
	cl_uint maxDimensions;
	int errcode;
	errcode = clGetDeviceInfo(devices[deviceIndex], CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, sizeof(cl_uint), &maxDimensions, NULL);
	if(errcode != CL_SUCCESS) {
		ERROR_MSG("%s", parser.errorString(errcode));
		return -1;
	}
	return maxDimensions;
}

cl_ulong ExecutionPlatform::getLocalMemorySize(unsigned int deviceIndex) {
	if(deviceIndex > devices.size()-1) {
		return 0;
	}
	cl_ulong maxLocalSize = 0;
	int errcode;
	errcode = clGetDeviceInfo(devices[deviceIndex], CL_DEVICE_LOCAL_MEM_SIZE, sizeof(cl_ulong), &maxLocalSize, NULL);
	if(errcode != CL_SUCCESS) {
		ERROR_MSG("%s", parser.errorString(errcode));
		return -1;
	}
	return maxLocalSize;
}

cl_mem ExecutionPlatform::allocateDeviceMem(IWorkData &dataInfo, unsigned int deviceIndex, unsigned int overlapIndex) {
	cl_mem newMem;
	int errcode;
	// In the current implementation, only buffers and 2D images are eligible to become memory objects
	switch (dataInfo.getDataType()) {
	case IWorkData::BUFFER:
		newMem = clCreateBuffer(context[deviceIndex][overlapIndex], dataInfo.getAccessMode(), dataInfo.getMemSize(), NULL, &errcode);
		if(errcode != CL_SUCCESS) {
			throw marrow::MemoryAllocationException(parser.errorString(errcode));
		}
		deviceFreeMem[deviceIndex] -= dataInfo.getMemSize();
		deviceTotalFreeMem -= dataInfo.getMemSize();
		DEBUG_MSG("Reserved %lu; freeDeviceMem[%u] = %lu; deviceTotalFreeMem = %lu", dataInfo.getMemSize(), deviceIndex, deviceFreeMem[deviceIndex], deviceTotalFreeMem);
		break;
	case IWorkData::IMAGE2D:
		cl_image_format format;
		format.image_channel_data_type = dataInfo.getFormat().clImageFormat.image_channel_data_type;
		format.image_channel_order = dataInfo.getFormat().clImageFormat.image_channel_order;
		newMem = clCreateImage2D(context[deviceIndex][overlapIndex], dataInfo.getAccessMode() , &format, dataInfo.getWidth(), dataInfo.getHeight(), 0, NULL, &errcode);
		if(errcode != CL_SUCCESS) {
			throw marrow::MemoryAllocationException(parser.errorString(errcode));
		}
		break;
	default:
		return NULL;
	}

	return newMem;
}

void ExecutionPlatform::freeDeviceMem(IWorkData &dataInfo, unsigned int deviceIndex, cl_mem deviceMem) {
	if(deviceMem) {
		clReleaseMemObject(deviceMem);
		deviceFreeMem[deviceIndex] += dataInfo.getMemSize();
		deviceTotalFreeMem += dataInfo.getMemSize();

		DEBUG_MSG("Freed %lu; freeDeviceMem[%u] = %lu; deviceTotalFreeMem = %lu", dataInfo.getMemSize(), deviceIndex, deviceFreeMem[deviceIndex], deviceTotalFreeMem);
	}
}

cl_command_queue ExecutionPlatform::createCommandQueue(unsigned int deviceIndex, unsigned int overlapIndex) {
	if(deviceIndex > devices.size()-1) {
		DEBUG_MSG("deviceIndex(%u) > devices.size()-1(%lu)", deviceIndex, devices.size()-1);
		return NULL;
	}
	int errcode;
	cl_command_queue newQueue;
	#ifndef PROFILING
	newQueue = clCreateCommandQueue(context[deviceIndex][overlapIndex], devices[deviceIndex], 0, &errcode);
	#else
	newQueue = clCreateCommandQueue(context[deviceIndex][overlapIndex], devices[deviceIndex], CL_QUEUE_PROFILING_ENABLE, &errcode);
	#endif
	if(errcode != CL_SUCCESS) {
		ERROR_MSG("%s", parser.errorString(errcode));
		return NULL;
	}
	return newQueue;
}

bool ExecutionPlatform::supportImages(unsigned int deviceIndex) {
	if(deviceIndex > devices.size()-1) {
		return false;
	}
	cl_bool supportImages;
	int errcode;
	errcode = clGetDeviceInfo(devices[deviceIndex], CL_DEVICE_IMAGE_SUPPORT, sizeof(cl_bool), &supportImages, NULL);
	if(errcode != CL_SUCCESS) {
		ERROR_MSG("%s", parser.errorString(errcode));
		return false;
	}
	return supportImages ? true : false;
}
