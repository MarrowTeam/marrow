/*
 * Stopwatch.hpp
 *
 *	April 2012:
 *		Ricardo Marques
 *		Creation of a timing concept for evaluation
 *
 *  Created on: 4 de Abr de 2012
 *      Author: Ricardo Marques
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef STOPWATCH_HPP
#define STOPWATCH_HPP

#ifdef _WIN32
	#include <windows.h>
#else
	#include <sys/time.h>
	#include <unistd.h>
#endif

#include <iostream>

class Stopwatch{
public:
    Stopwatch(bool start=false){
		#ifdef _WIN32
        	_start.QuadPart = 0;
        	_stop.QuadPart = 0;
		#endif
        if(start)
                Start();
    }

    //! Starts the stopwatch running
    void Start(){
		#ifdef _WIN32
        	QueryPerformanceCounter(&_start);
		#else
        	gettimeofday(&start, NULL);
		#endif
    }
    //! Run this when the event being timed is complete
    void Stop(){
		#ifdef _WIN32
        	QueryPerformanceCounter(&_stop);
		#else
        	gettimeofday(&end, NULL);
		#endif
    }
   //! Stops the timer and returns the result
   double StopResult(int scale){
      Stop();
      switch (scale) {
		case 0:
			// nanoseconds
			return ResultNanoseconds();
		case 1:
			// microseconds
			return ResultNanoseconds()/1000.0;
		case 2:
			// miliseconds
			return ResultNanoseconds()/1000000.0;
		case 3:
			// seconds
			return ResultNanoseconds()/1000000000.0;
		default:
			return -1.0;
	}
   }
    double ResultNanoseconds(){
		#ifdef _WIN32
			LARGE_INTEGER frequency;
			QueryPerformanceFrequency(&frequency);
			double secInNano = 1000000000.0;
			double cyclesPerNanosecond = static_cast<double>(frequency.QuadPart) / secInNano;
			LARGE_INTEGER elapsed;
			elapsed.QuadPart = _stop.QuadPart - _start.QuadPart;
			return elapsed.QuadPart / cyclesPerNanosecond;
		#else
			double mtime, seconds, useconds;
			seconds  = end.tv_sec  - start.tv_sec;
			seconds *= 1000000000.0;
			useconds = end.tv_usec - start.tv_usec;
			useconds *= 1000.0;
			mtime = seconds + useconds;
			return mtime;
		#endif
    }
    void PrintResultNanoseconds(){
        std::cout << ResultNanoseconds() << " nanosec" << std::endl;
    }
    void PrintResultMicroseconds(){
        std::cout << ResultNanoseconds()/1000 << " microsec" << std::endl;
    }
    void PrintResultMilliseconds(){
        std::cout << ResultNanoseconds()/1000000 << " millisec" << std::endl;
    }
    void PrintResultSeconds(){
        std::cout << ResultNanoseconds()/1000000000 << " sec" << std::endl;
    }
private:
	#ifdef _WIN32
    	LARGE_INTEGER _start;
    	LARGE_INTEGER _stop;
	#else
    	struct timeval start, end;
	#endif
};

#endif /* STOPWATCH_HPP */
