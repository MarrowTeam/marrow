/*
 * IAutoTuner.hpp
 *
 *	February 2013:
 *		Fernando Alexadre
 *		Creation of the partition auto-tuner concept
 *
 *  Created on: 24 de Fev de 2013
 *      Author: Fernando Alexandre
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef IAUTOTUNER_HPP_
#define IAUTOTUNER_HPP_

#include <vector>
#include <memory>

#include "marrow/runtime/PartitionedInfo.hpp"
#include "marrow/runtime/IWorkData.hpp"

/**
 * Component which reads performance values and defines partitions sizes
 */
class IAutoTuner {
public:
	/**
	 * Class destructor
	 */
	virtual ~IAutoTuner() {};

	/**
	 * Creates input partitions in relation to the required data.
	 *
	 * @param input - The computations vector of input variables.
	 * @param workSizes - The decomposed worksizes.
	 * @return The vector with the PartitionedInfo (containing all the partitions) of each argument.
	 */
	virtual std::vector<std::shared_ptr<PartitionedInfo>> * getInputPartitions(const std::vector<std::shared_ptr<IWorkData>> &inputData, const std::vector<std::vector<std::vector<size_t>>> &workSizes) = 0;

	/**
	 * Creates output partitions in relation to the required data.
	 *
	 * @param input - The computations vector of input variables.
	 * @param workSizes - The decomposed work sizes.
	 * @return The vector with the PartitionedInfo (containing all the partitions) of each argument.
	 */
	virtual std::vector<std::shared_ptr<PartitionedInfo>> * getOutputPartitions(const std::vector<std::shared_ptr<IWorkData>> &outputData, const std::vector<std::vector<std::vector<size_t>>> &workSizes) = 0;

	/**
	 * Decomposes the global work size submitted by the programmer, accordingly to the performance
	 * ratios (either single, or double precision).
	 *
	 * @param globalWorkSize - The programmer-submitted work size
	 * @param localWorkSize - The local work size, which forces the decomposition to by its multiples.
	 * @param isDoublePrecision - Whether it should be partitioned using single precision or double precision ratios.
	 * @param staticMemSize - the static memory required by this computation (for memory-aware decomposition).
	 * @param dynamicMemSize - The dynamic memory required by each work size element.
	 * @return The partitioned worksizes organized as: partition id => overlapping partition id.
	 */
	virtual std::shared_ptr<std::vector<std::vector<std::vector<size_t>>>> getWorkSizes(const std::vector<unsigned int> &globalWorkSize,
																			const std::vector<size_t> &localWorkSize,
																			const bool isDoublePrecision,
																			const unsigned long staticMemSize,
																			const unsigned long dynamicMemSize) = 0;

};

#endif /* IAUTOTUNER_HPP_ */
