/*
 * IOpenCLErrorParser.hpp
 *
 *	February 2012:
 *		Ricardo Marques
 *		Creation of the OpenCL error parser
 *
 *  Created on: 22 de Fev de 2012
 *      Author: Ricardo Marques
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef _MARROW_IOPENCLERRORPARSER_HPP_
#define _MARROW_IOPENCLERRORPARSER_HPP_


/**
 * Helper for transforming error codes into readable error strings.
 */
class IErrorParser {
public:
	/**
	 * Method parses an error interger value into a representative, and redable, string. The error codes are associated to the OpenCL language.
	 *
	 * @param error - The OpenCL error code.
	 * @return A string translation for the given error code.
	 */
	static const char* errorString(int error);
	 

	/**
	 * Class destructor
	 */
	virtual ~IErrorParser(){};

};

#endif /* _MARROW_IOPENCLERRORPARSER_H_ */
