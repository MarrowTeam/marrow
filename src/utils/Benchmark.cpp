/*
 * Benchmark.cpp
 *
 *	March 2014:
 *		Herve Paulino
 *		Specification and implementations of the Benchmark class
 *
 *
 *  Created on: March 1st, 2014
 *      Author: Herve Paulino
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
#include "marrow/Skeleton.hpp"
#include "marrow/utils/Benchmark.hpp"

#include <iostream>
#include <unistd.h>

#if defined __APPLE__ || defined MACOSX
#include <mach-o/dyld.h>
#endif

#define MAX_PATH_SIZE 65536

using namespace std;

static const char pathSeparator =
#if defined _WIN32 || defined _WIN64
                            '\\';
#else
                            '/';
#endif

marrow::utils::Benchmark::Benchmark(string name, int argc, char* argv[]) :
skeletonTree(NULL),
name(name)
{
	processArguments(argc, argv);
}

marrow::utils::Benchmark::~Benchmark() {
	if (skeletonTree != NULL)
		delete skeletonTree;
}

string marrow::utils::Benchmark::usage() {
	return "[-d number_of_devices] [-o overlap_buffers] [-n number_iterations]";
}

string marrow::utils::Benchmark::getName() {
	return name;
}

void marrow::utils::Benchmark::processArguments(int argc, char* argv[]) {
	int c;

	while ((c = getopt(argc, argv, "d:n:o:")) != -1) {
		int opt;

		switch (c) {
		case 'd':
			opt = atoi(optarg);
			if (opt < 0)
				cerr << "Warning: invalid number of devices. Reverted to \"all\"." << endl;
			numDevices = opt > 0 ? opt : numDevices;
			break;
		case 'n':
			opt = atoi(optarg);
			if (opt < 1)
				cerr << "Warning: invalid number of iterations. Reverted to 1." << endl;
			numIterations = opt > 1 ? opt : numIterations;
			break;
		case 'o':
			opt = atoi(optarg);
			if (opt < 1)
				cerr << "Warning: invalid number of buffers. Reverted to 1." << endl;
			numBuffers = opt > 1 ? opt : numBuffers;
			break;
		}
	}
}

void marrow::utils::Benchmark::interRunCleanup() { }

void marrow::utils::Benchmark::timedRuns(int scale) {
	try {
		init();

		int numberRuns = this->numIterations;
		Timer t(numberRuns);

		for (int i = 0; i < numberRuns; i++) {
			t.start();
			run();
			t.end();
			interRunCleanup();
		}


		int nDevs = numDevices ? numDevices : marrow::Skeleton::executionPlatform->getNumDevices();
		cout << "Benchmark: " << name << endl
				<< "\t" << "Number devices: " << nDevs << endl
				<< "\t" << "Number overlap buffers: " << numBuffers << endl
				<< "\t" << "Number iterations: " << numIterations << endl;
		if (validate())
			t.printStats(cout, scale);
		else
			cout << "Result not valid" << endl;

#ifdef PROFILING
		cout << skeletonTree->getProfilingInfo() << endl;
#endif

	} catch (std::runtime_error& e) {
		cerr << "Exception : " << e.what() << endl;
	}
}

// TODO: yet be tested on Windows and Linux
std::string marrow::utils::Benchmark::resolveKernel(std::string kernel) {
	char pathBuffer[MAX_PATH_SIZE];
	std::string path;

#if defined _WIN32 ||  defined _WIN64
	path = std::string(pathBuffer, GetModuleFileName(NULL, pathBuffer, MAX_PATH_SIZE));
#endif
#if defined __APPLE__  || defined MACOSX
	uint32_t size = MAX_PATH_SIZE;
	_NSGetExecutablePath(pathBuffer, &size);
	path = std::string(pathBuffer);
#endif
#ifdef __linux__
	ssize_t count = readlink("/proc/self/exe", pathBuffer, MAX_PATH_SIZE);
	path = std::string(pathBuffer, (size > 0) ? size : 0);
#endif

	int index = path.find_last_of(pathSeparator);
	return path.substr(0,index) + pathSeparator + kernel;
}
