/*
 * Timer.cpp
 *
 *	March 2014:
 *		Herve Paulino
 *		Specification and implementation of the Timer class (from pre-existent classes)
 *
 *
 *  Created on: March 1st, 2014
 *      Authors: Herve Paulino, Ricardo Marques, Fernando Alexandre
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifdef _WIN32
#include <windows.h>
#else
#include <sys/time.h>
#include <unistd.h>
#endif
#include <iostream>
#include <ostream>
#include <cmath>

#include "marrow/utils/Timer.hpp"

#define MEASURED_VALUES 3
#define MULT2(X) ((X) + (X))

static double *measurements;
static int accountedMeasurements;
static int numberMeasurements;

static int currentMeasurement = 0;
static double computedAverage = 0.0;

marrow::utils::Timer::Timer(int nrMeasurements) {
#ifdef _WIN32
	_start.QuadPart = 0;
	_end.QuadPart = 0;
#endif

	numberMeasurements = nrMeasurements;
	measurements = new double[numberMeasurements];
	accountedMeasurements = nrMeasurements/MEASURED_VALUES + 1;
}

marrow::utils::Timer::~Timer() {
	delete[] measurements;
}

void marrow::utils::Timer::start() {
#ifdef _WIN32
	QueryPerformanceCounter(&_wstart);
#else
	gettimeofday(&_start, NULL);
#endif
}

void marrow::utils::Timer::end() {
#ifdef _WIN32
	QueryPerformanceCounter(&_wend);
	LARGE_INTEGER frequency;
	QueryPerformanceFrequency(&frequency);
	double secInNano = 1000000000.0;
	double cyclesPerNanosecond = static_cast<double>(frequency.QuadPart) / secInNano;
	LARGE_INTEGER elapsed;
	elapsed.QuadPart = _wend.QuadPart - _wstart.QuadPart;
	measurements[currentMeasurement++] elapsed.QuadPart / cyclesPerNanosecond;
#else
	double seconds, useconds;
	gettimeofday(&_end, NULL);
	seconds = _end.tv_sec - _start.tv_sec;
	seconds *= 1000000000.0;
	useconds = _end.tv_usec - _start.tv_usec;
	useconds *= 1000.0;
	measurements[currentMeasurement++] = seconds + useconds;
#endif
	computedAverage = 0;
}

/**
 * Auxiliary function that applies the given scale to value
 */
static double applyScale(double value, int scale) {
	switch (scale) {
	case NANOSECONDS:
		return value;
	case MICROSECONDS:
		return value / 1000.0;
	case MILLISECONDS:
		return value / 1000000.0;
	case SECONDS:
		return value / 1000000000.0;
	default:
		return -1.0;
	}
}

/**
 * Auxiliary function that returns the unit associated to the given scale
 */
static std::string unit(int scale) {
	switch (scale) {
	case NANOSECONDS:
		return "nanoseconds";
	case MICROSECONDS:
		return "microseconds";
	case MILLISECONDS:
		return "milliseconds";
	case SECONDS:
		return "seconds";
	default:
		return "";
	}
}

/**
 * Auxiliary compare function
 */
int compare(const void *a, const void *b){
	if( ( *(double*)a < *(double*)b ))
		return -1;
	return *(double*)a > *(double*)b;
}

double marrow::utils::Timer::average(int scale) {
	if (computedAverage == 0) {
		qsort(measurements, numberMeasurements, sizeof(double), compare);
		for (int i = accountedMeasurements; i < MULT2(accountedMeasurements); i++)
			computedAverage += measurements[i];
		computedAverage /= accountedMeasurements;
	}

	return applyScale(computedAverage, scale);
}

double marrow::utils::Timer::stdDeviation(int scale) {
	average(scale);

	double variance = 0.0;
	for (int i = accountedMeasurements; i < MULT2(accountedMeasurements); i++)
		variance += ((measurements[i] - computedAverage) * (measurements[i] - computedAverage));
	if (accountedMeasurements > 1)
		variance /= (accountedMeasurements - 1);

	return applyScale(sqrt(variance), scale);
}

void marrow::utils::Timer::printStats(std::ostream& out, int scale) {
	const std::string u = " " + unit(scale);
	out << "Statistics (middle " << accountedMeasurements << " of " << numberMeasurements << " measurements) in " << unit(scale) << ":" << std::endl
			<< "\tMaximum: " << applyScale(measurements[MULT2(accountedMeasurements)-1], scale) << std::endl
			<< "\tMinimum: " << applyScale(measurements[accountedMeasurements], scale) << std::endl
			<< "\tAverage: " << average(scale) << std::endl
			<< "\tStandard deviation: " << stdDeviation(scale) << std::endl;
}




