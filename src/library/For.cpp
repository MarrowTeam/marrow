/*
 * marrow::For.cpp
 *
 *  Created on: 21 de Abr de 2012
 *      Author: Ricardo Marques
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "marrow/For.hpp"
#include "marrow/utils/Debug.hpp"

/**********************************/
/********* marrow::For Functions **********/
/**********************************/

marrow::For::For(unsigned int from, unsigned int to, std::unique_ptr<marrow::IExecutable> &exec):
marrow::Loop(exec, false),
startFrom(from),
to(to)
{}

marrow::For::~For(){}

marrow::ForState* marrow::For::createState() {
	return new marrow::ForState(startFrom, to);
}

/************************************/
/******** marrow::ForState Functions ********/
/************************************/

marrow::ForState::ForState(unsigned int from, unsigned int to):
from(from),
startFrom(from),
to(to)
{}

marrow::ForState::~ForState() {}

bool marrow::ForState::condition() {
	return from < to;
}

void marrow::ForState::step(std::vector<std::shared_ptr<marrow::Vector>> &previousOut) {
	UNUSED(previousOut);
	from++;
}

void marrow::ForState::reset() {
	from = startFrom;
}
