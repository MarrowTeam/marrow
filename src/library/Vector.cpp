/*
 * marrow::Vector.cpp
 *
 *  Created on: 15 de Mar de 2013
 *      Author: Fernando Alexandre
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "marrow/Vector.hpp"
#include "marrow/utils/Debug.hpp"

marrow::Vector::Vector(void* data, std::size_t elemSize) {
    marrow::Vector(data, elemSize, 1);
}

marrow::Vector::Vector(void* data, std::size_t elemSize, unsigned int nElems) {
    set(data, elemSize, nElems);
}

marrow::Vector::~Vector() {}

void* marrow::Vector::get() {
    return data;
}

void* marrow::Vector::get(unsigned int elem) {
    return (void*) (((char*) data) + elem * elemSize);
}

std::size_t marrow::Vector::getElemSize() {
    return elemSize;
}

unsigned int marrow::Vector::getNumElements() {
    return nElems;
}

void marrow::Vector::set(void* data, std::size_t elemSize) {
    set(data, elemSize, 1);
}

void marrow::Vector::set(void* data, std::size_t elemSize, unsigned int nElems) {
    this->data = data;
    this->nElems = nElems;
    this->elemSize = elemSize;
    setModified();
}

void marrow::Vector::setModified() {
    needsUpdate = true;
}

void marrow::Vector::setUpdated() {
    needsUpdate = false;
}

bool marrow::Vector::requiresUpdate() {
    return needsUpdate;
}
