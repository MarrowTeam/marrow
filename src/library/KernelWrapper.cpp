/*
 * marrow::KernelWrapper.cpp
 *
 *  Created on: 22 de Fev de 2012
 *      Author: Ricardo Marques
 *			Modified by Fernando Alexandre
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
#include "marrow/KernelWrapper.hpp"
#include "marrow/Skeleton.hpp"
#include "marrow/FinalData.hpp"
#include "marrow/utils/Debug.hpp"
#include "marrow/exceptions/NoImageSupportException.hpp"
#include "marrow/exceptions/TooManyDimensionsException.hpp"
#include "marrow/exceptions/NotEnoughMemoryException.hpp"
#include "marrow/exceptions/ExecutionException.hpp"

#include "runtime/OpenCLErrorParser.hpp"


// Parameterizable constructor
marrow::KernelWrapper::KernelWrapper(const std::string kernelFile, const std::string kernelFunc, const std::vector<std::shared_ptr<IWorkData>> &inputEntries, const std::vector<std::shared_ptr<IWorkData>> &outputEntries, const std::vector<unsigned int> &globalWork) :
globalMemSize(0),
localMemSize(0),
globalWorkSize(globalWork),
hasLocalWorkSize(false),
isInit(false),
hasImages(false),
hasPartitions(false),
staticMem(0),
dynamicMem(0),
shouldAllocInput(true),
shouldAllocOutput(true),
kernelFile(kernelFile),
kernelFunc(kernelFunc),
isDPrecision(false)
{
	initWrapper(inputEntries, outputEntries);
}

marrow::KernelWrapper::KernelWrapper(const std::string kernelFile, const std::string kernelFunc, const std::vector<std::shared_ptr<IWorkData>> &inputEntries, const std::vector<std::shared_ptr<IWorkData>> &outputEntries, const std::vector<unsigned int> &globalWork, const std::vector<unsigned int> &localWork) :
globalMemSize(0),
localMemSize(0),
globalWorkSize(globalWork),
hasLocalWorkSize(true),
isInit(false),
hasImages(false),
hasPartitions(false),
staticMem(0),
dynamicMem(0),
shouldAllocInput(true),
shouldAllocOutput(true),
kernelFile(kernelFile),
kernelFunc(kernelFunc),
isDPrecision(false)
{
	localWorkSize.resize(localWork.size());
	for (unsigned int i = 0; i < localWork.size(); i++) {
		localWorkSize[i] = localWork[i];
	}
	initWrapper(inputEntries, outputEntries);
}


void marrow::KernelWrapper::initWrapper(const std::vector<std::shared_ptr<IWorkData>> &inputEntries, const std::vector<std::shared_ptr<IWorkData>> &outputEntries) {
	setInOutInfo(inputEntries, outputEntries);
	this->numDimensions = globalWorkSize.size();
}

void marrow::KernelWrapper::setInOutInfo(const std::vector<std::shared_ptr<IWorkData>> &inputEntries, const std::vector<std::shared_ptr<IWorkData>> &outputEntries) {
	unsigned int i, numIn = inputEntries.size(), numOut = outputEntries.size();
	unsigned int numMemIn = 0;
	globalMemSize = 0;

	// set input info
	for(i = 0; i < numIn; i++) {
		switch (inputEntries[i]->getDataType()) {
		case IWorkData::LOCAL:
			localMemSize += inputEntries[i]->getMemSize();
			fixedWorkData.push_back(std::pair<int, std::shared_ptr<IWorkData>>(i, inputEntries[i]));
			break;
		case IWorkData::FINAL:
			fixedWorkData.push_back(std::pair<int, std::shared_ptr<IWorkData>>(i, inputEntries[i]));
			break;
		default:
			if(inputEntries[i]->getPartitionMode() == IWorkData::PARTITIONABLE) {
				unsigned int numElems = inputEntries[i]->getWidth() * inputEntries[i]->getHeight() * inputEntries[i]->getDepth();

				dynamicMem += inputEntries[i]->getIndivisibleSize() * (inputEntries[i]->getMemSize() / numElems);
			} else {
				staticMem += inputEntries[i]->getMemSize();
			}

			globalMemSize += inputEntries[i]->getMemSize();
			workData.push_back(std::pair<int, std::shared_ptr<IWorkData>>(i, inputEntries[i]));
			if(inputEntries[i]->getDataType() == IWorkData::IMAGE2D) {
				this->hasImages = true;
			} else {
				isDPrecision = isDPrecision || inputEntries[i]->isDoublePrecision();
			}
			numMemIn++;
			break;
		}
	}
	inWorkData.resize(numMemIn);
	std::list<std::pair<int, std::shared_ptr<IWorkData>>>::iterator it;
	for(i = 0, it = workData.begin(); it != workData.end(); it++, i++) {
		inWorkData[i] = it->second;
	}
	// set output info
	outWorkData.resize(numOut);
	for(i = 0; i < numOut; i++) {
		if(outputEntries[i]->getPartitionMode() == IWorkData::PARTITIONABLE) {
			unsigned int numElems = outputEntries[i]->getWidth() * outputEntries[i]->getHeight() * outputEntries[i]->getDepth();

			dynamicMem += outputEntries[i]->getIndivisibleSize() * (outputEntries[i]->getMemSize() / numElems);
		} else {
			staticMem += outputEntries[i]->getMemSize();
		}
		globalMemSize += outputEntries[i]->getMemSize();
		workData.push_back(std::pair<int, std::shared_ptr<IWorkData>>(i+numIn, outputEntries[i]));
		outWorkData[i] = outputEntries[i];
		if(outWorkData[i]->getDataType() == IWorkData::IMAGE2D) {
			this->hasImages = true;
		} else {
			isDPrecision = isDPrecision || outputEntries[i]->isDoublePrecision();
		}
	}

	if((*marrow::Skeleton::executionPlatform).getTotalFreeMemorySize() < globalMemSize) {
		throw NotEnoughMemoryException();
	}

	DEBUG_MSG("%s is %s", kernelFunc.data(), isDPrecision ? "double precision": "single precision");
}

marrow::KernelWrapper::~KernelWrapper() {
	unsigned int i, j;

	for(i = 0; i < programs.size(); i++) {
		for(j = 0; j < programs[i].size(); j++)
		clReleaseProgram(programs[i][j]);
	}

	workSize.reset();

	if(shouldAllocInput) {
		clearInputMem();
	}

	if(shouldAllocOutput) {
		clearOutputMem();
	}

	for (i = 0; i < partitionedInput.size(); i++) {
		partitionedInput[i].reset();
	}

	for (i = 0; i < partitionedOutput.size(); i++) {
		partitionedOutput[i].reset();
	}

	for(i = 0; i < inWorkData.size(); i++) {
		inWorkData[i].reset();
	}

	for(i = 0; i < outWorkData.size(); i++) {
		outWorkData[i].reset();
	}

	std::list<std::pair<int, std::shared_ptr<IWorkData>>>::iterator it;
	for(it = workData.begin(); it != workData.end(); it++) {
		it->second.reset();
	}

	for(it = fixedWorkData.begin(); it != fixedWorkData.end(); it++) {
		it->second.reset();
	}
}

//protected:
bool marrow::KernelWrapper::isInitialized() {
	return isInit;
}

void marrow::KernelWrapper::initExecutable() {
	if(isInit) {
		return;
	}

	unsigned int i, j;
	cl_uint numDim = this->numDimensions;
	cl_uint maxNumDim;

	// check if device can handle images and
	// check if device can handle desired number of dimensions
	for(i = 0; i < marrow::Skeleton::executionPlatform->getNumDevices(); i++) {
		if(hasImages && !marrow::Skeleton::executionPlatform->supportImages(i)) {
			throw NoImageSupportException("2D");
		}
		maxNumDim = marrow::Skeleton::executionPlatform->maxDimensions(i);
		if(numDim > maxNumDim) {
			throw TooManyDimensionsException();
		}
	}

	// Create the cl_program object to spawn cl_kernels for multiple executions.
	std::vector<cl_device_id> devices((*marrow::Skeleton::executionPlatform).getNumDevices());
	marrow::Skeleton::executionPlatform->getDevices(devices);

	std::vector<std::vector<cl_context>> * contexts = (*marrow::Skeleton::executionPlatform).getAllPlatformContexts();

	programs.resize((*contexts).size());

	for(i = 0; i < (*contexts).size(); i++) {
		programs[i].reserve((*contexts)[i].size());
		for(j = 0; j < (*contexts)[i].size(); j++) {
			std::vector<cl_device_id> curr(1);
			curr[0] = devices[i];
			std::vector<unsigned int> currIds(1);
			currIds[0] = i;
			programs[i][j] = builder.getProgram(kernelFile, (*contexts)[i][j], curr, currIds);
		}
	}

	unsigned int numOverlapPartitions = marrow::Skeleton::scheduler->getNumOverlapPartitions();
	unsigned int numPartitions = marrow::Skeleton::scheduler->getNumDevices();

	kernels.resize(numPartitions);
	for(i = 0; i < numPartitions; i++) {
		kernels[i].resize(numOverlapPartitions);
		for(j = 0; j < numOverlapPartitions; j++) {
			kernels[i][j] = builder.getNewKernelInstance(programs[i][j], kernelFunc);
		}
	}

	// Configure profiling if needed.
	#ifdef PROFILING

	runSum = new cl_ulong[devices.size()];
	numRuns = new cl_ulong[devices.size()];

	deviceRunDurations.clear();
	deviceRunDurations.resize(devices.size());

	for(i = 0; i < devices.size(); i++) {
		runSum[i] = 0L;
		numRuns[i] = 0L;
	}
	#endif

	isInit = true;
}

void marrow::KernelWrapper::clearConfiguration() {
	unsigned int i = 0, j = 0;

	if(shouldAllocInput) {
		clearInputMem();
	}

	for(i = 0; i < partitionedInput.size(); i++) {
		if(partitionedInput[i]) {
			partitionedInput[i].reset();
		}
	}

	if(shouldAllocOutput) {
		clearOutputMem();
	}

	for(i = 0; i < partitionedOutput.size(); i++) {
		if(partitionedOutput[i]) {
			partitionedOutput[i].reset();
		}
	}

	for(i = 0; i < programs.size(); i++) {
		for(j = 0; j < programs[i].size(); j++) {
			clReleaseProgram(programs[i][j]);
		}
	}

	for(i = 0; i < kernels.size(); i++) {
		for(j = 0; j < kernels[i].size(); j++) {
			clReleaseKernel(kernels[i][j]);
		}
	}

	programs.clear();
	kernels.clear();

	hasPartitions = false;
}

void marrow::KernelWrapper::reconfigureExecutable() {
	unsigned int i, j;
	unsigned int numOverlapPartitions = marrow::Skeleton::scheduler->getNumOverlapPartitions();
	unsigned int numPartitions = marrow::Skeleton::scheduler->getNumDevices();

	std::vector<cl_device_id> devices((*marrow::Skeleton::executionPlatform).getNumDevices());
	marrow::Skeleton::executionPlatform->getDevices(devices);
	std::vector<std::vector<cl_context>> * contexts = (*marrow::Skeleton::executionPlatform).getAllPlatformContexts();

	programs.resize((*contexts).size());

	for(i = 0; i < (*contexts).size(); i++) {
		programs[i].reserve((*contexts)[i].size());
		for(j = 0; j < (*contexts)[i].size(); j++) {
			std::vector<cl_device_id> curr(1);
			curr[0] = devices[i];
			std::vector<unsigned int> currIds(1);
			currIds[0] = i;
			programs[i][j] = builder.getProgram(kernelFile, (*contexts)[i][j], curr, currIds);
		}
	}


	kernels.resize(numPartitions);
	for(i = 0; i < numPartitions; i++) {
		kernels[i].resize(numOverlapPartitions);
		for(j = 0; j < numOverlapPartitions; j++) {
			kernels[i][j] = builder.getNewKernelInstance(programs[i][j], kernelFunc);
		}
	}

	#ifdef PROFILING
	delete[] runSum;
	delete[] numRuns;
	runSum = new cl_ulong[devices.size()];
	numRuns = new cl_ulong[devices.size()];

	deviceRunDurations.clear();
	deviceRunDurations.resize(devices.size());

	for(i = 0; i < (*marrow::Skeleton::executionPlatform).getNumDevices(); i++) {
		runSum[i] = 0L;
		numRuns[i] = 0L;
	}

	#endif
}

cl_event marrow::KernelWrapper::execute(cl_command_queue executionQueue, unsigned int deviceIndex, unsigned int uniqueId, unsigned int partitionIndex, unsigned int overlapPartition, std::vector<cl_mem> &inputData, std::vector<void*> &singletonInputValues, std::vector<cl_mem> &outputData, cl_event waitEvent, std::vector<std::shared_ptr<marrow::Vector>> &resultMem) {
	if(!isInit) {
		throw ExecutionException("Executable not initialized\n");
	}
	UNUSED(resultMem);
	UNUSED(uniqueId);
	unsigned int i, j, w;
	int errcode;
	cl_event event;
	cl_kernel kernel = kernels[deviceIndex][overlapPartition];
	unsigned int numEvents = waitEvent == NULL ? 0 : 1;
	cl_event* e = numEvents == 0 ? NULL : &waitEvent;
	std::vector<size_t> * globalWorkSize = getWorkSize(partitionIndex, overlapPartition);

	DEBUG_MSG("Executing kernel %s", kernelFunc.data());

	// set fixed kernel args
	std::list<std::pair<int, std::shared_ptr<IWorkData>>>::iterator it;
	for(it = fixedWorkData.begin(); it != fixedWorkData.end(); it++) {
		IWorkData* data = it->second.get();
		switch (data->getDataType()) {
		case IWorkData::LOCAL:
			errcode = clSetKernelArg(kernel, it->first, data->getMemSize(), NULL);
			break;
		case IWorkData::FINAL:
			// Swap Values to include the info
			{
				marrow::FinalData<char> * x = static_cast<marrow::FinalData<char>*>(data);
				unsigned int begin = 0, size = 0;
				for(i = 0; i < partitionedInput.size(); i++) {
					// If the partititioned is set
					if(partitionedInput[i]) {
						PartitionedInfo::DataSegment seg = partitionedInput[i]->getSegment(deviceIndex, overlapPartition);
						if(seg.segmentInfo->getPartitionMode() == IWorkData::PARTITIONABLE || seg.segmentInfo->getPartitionMode() == IWorkData::COPY) {
							// change this to the sum of the past worksizes?...
							begin = seg.begin_comp;
							size = seg.size;
							break;
						}
					}
				}

				switch((*x).getTrait()) {
				case marrow::FinalData<char>::COMP_OFFSET :
					errcode = clSetKernelArg(kernel, it->first, data->getMemSize(), &begin);
					DEBUG_MSG("Swapped Argument to OFFSET = %u...", begin);
					break;
				case marrow::FinalData<char>::COMP_SIZE :
					errcode = clSetKernelArg(kernel, it->first, data->getMemSize(), &size);
					DEBUG_MSG("Swapped Argument to SIZE = %u...", size);
					break;
				case marrow::FinalData<char>::COMP_PART_LOC:
					unsigned int result;
					if(partitionIndex == 0 && overlapPartition == 0) {
						result = marrow::FinalData<char>::LOC_TOP;
						errcode = clSetKernelArg(kernel, it->first, data->getMemSize(), &result);
					} else if (deviceIndex == (marrow::Skeleton::scheduler->getNumDevices() - 1) &&
						overlapPartition == (marrow::Skeleton::scheduler->getNumOverlapPartitions() - 1)) {
						result = marrow::FinalData<char>::LOC_BOTTOM;
						errcode = clSetKernelArg(kernel, it->first, data->getMemSize(), &result);
					} else {
						result = marrow::FinalData<char>::LOC_MIDDLE;
						errcode = clSetKernelArg(kernel, it->first, data->getMemSize(), &result);
					}
					DEBUG_MSG("Swapped Argument to COMP_PART_LOC = %u...", result);
					break;
				default:
					errcode = clSetKernelArg(kernel, it->first, data->getMemSize(), data->value());
				}
			}
			break;
		default:
			break;
		}
		if(errcode != CL_SUCCESS) {
			ERROR_MSG("Error while setting local and final arguments%s", ".");
			throw ExecutionException(OpenCLErrorParser::errorString(errcode));
		}
	}

	it = workData.begin();
	unsigned int numIn = inWorkData.size();
	unsigned int numOut = outWorkData.size();
	for(i = 0, j = 0, w = 0; i < numIn; i++, it++) {
		IWorkData* data = it->second.get();
		switch (data->getDataType()) {
		case IWorkData::SINGLETON:
			errcode = clSetKernelArg(kernel, it->first, data->getMemSize(), singletonInputValues[w]);
			w++;
			break;
		default:
			errcode = clSetKernelArg(kernel, it->first, sizeof(cl_mem), (void *) &(inputData[j]));
			j++;
			break;
		}
		if(errcode != CL_SUCCESS) {
			ERROR_MSG("Error while setting singleton and memory%s", ".");
			throw ExecutionException(OpenCLErrorParser::errorString(errcode));
		}
	}
	for(i = 0; i < numOut; i++, it++) {
		errcode = clSetKernelArg(kernel, it->first, sizeof(cl_mem), (void *) &(outputData[i]));
		if(errcode != CL_SUCCESS) {
			ERROR_MSG("Error setting cl_mem output Arguments idx = %u; addr = %p", it->first, &(outputData[i]));
			throw ExecutionException(OpenCLErrorParser::errorString(errcode));
		}
	}

	if(hasLocalWorkSize) {
		errcode = clEnqueueNDRangeKernel(executionQueue, kernel, numDimensions, NULL, (*globalWorkSize).data(), localWorkSize.data(), numEvents, e, &event);
	} else {
		errcode = clEnqueueNDRangeKernel(executionQueue, kernel, numDimensions, NULL, (*globalWorkSize).data(), NULL, numEvents, e, &event);
	}
	if(errcode != CL_SUCCESS) {
		DEBUG_MSG("Error while executing clEnqueueNDRangeKernel: %s", OpenCLErrorParser::errorString(errcode));
		throw ExecutionException(OpenCLErrorParser::errorString(errcode));
	}

	// Add profiler task to the queue to be eventually processed.
	#ifdef PROFILING
	ProfilerTask * t = new ProfilerTask();
	(*t).event = event;
	(*t).deviceIndex = deviceIndex;

	boost::lock_guard<boost::mutex> lock(profilerMutex);
	profilerQueue.push_back(t);
	#endif

	return event;
}

const std::vector<std::shared_ptr<IWorkData>> * marrow::KernelWrapper::getInputDataInfo() {
	return &inWorkData;
}

unsigned int marrow::KernelWrapper::getNumInputEntries() {
	return inWorkData.size();
}

const std::vector<std::shared_ptr<IWorkData>> * marrow::KernelWrapper::getOutputDataInfo() {
	return &outWorkData;
}

unsigned int marrow::KernelWrapper::getNumOutputEntries() {
	return outWorkData.size();
}

unsigned long marrow::KernelWrapper::requiredStaticMem() {
	return staticMem;
}

unsigned long marrow::KernelWrapper::requiredDynamicMem() {
	return dynamicMem;
}

unsigned long marrow::KernelWrapper::requiredGlobalMem() {
	return globalMemSize;
}


unsigned long marrow::KernelWrapper::requiredLocalMem() {
	return localMemSize;
}

bool marrow::KernelWrapper::readsData() {
	return false;
}

std::vector<unsigned int> * marrow::KernelWrapper::getGlobalWorkSize() {
	return &globalWorkSize;
}

std::vector<size_t> * marrow::KernelWrapper::getLocalWorkSize() {
	if(hasLocalWorkSize) {
		return &localWorkSize;
	}

	std::vector<size_t> * localWork = new std::vector<size_t>(0);
	return localWork;
}

void marrow::KernelWrapper::setPartitionedInput(const std::vector<std::shared_ptr<PartitionedInfo>> &partitionedInput) {
	if(partitionedInput.size() == 0) {
		return;
	}
	unsigned int i, j, w = 0;
	unsigned int numOverlapPartitions = marrow::Skeleton::scheduler->getNumOverlapPartitions();
	unsigned int numPartitions = marrow::Skeleton::scheduler->getNumDevices();

	this->hasPartitions = true;
	this->partitionedInput = partitionedInput;

	if(shouldAllocInput) {
		inputMem.resize(numPartitions);

		for(w = 0; w < numPartitions; w++) {
			if((*workSize)[w][0][globalWorkSize.size() - 1] == 0) {
				inputMem[w].resize(0);
				continue;
			} else {
				inputMem[w].resize(numOverlapPartitions);
			}
			for(j = 0; j < numOverlapPartitions; j++) {
				// No resize because some partitionedInput might be NULL because of singletons.
				//inputMem[w][j].resize(partitionedInput.size());
				for (i = 0; i < partitionedInput.size(); i++) {
					if(partitionedInput[i]) {
						PartitionedInfo::DataSegment curr = partitionedInput[i]->getSegment(w, j);

						// Avoid singletons' NULL values on this array
						inputMem[w][j].push_back((*marrow::Skeleton::executionPlatform).allocateDeviceMem(*curr.segmentInfo, w, j));
					}
				}
			}
		}
	}
}

std::vector<std::shared_ptr<PartitionedInfo>> * marrow::KernelWrapper::getPartitionedInput() {
	return &partitionedInput;
}

std::vector<cl_mem> * marrow::KernelWrapper::getInputMem(unsigned int partitionIndex, unsigned int overlapPartitionIndex) {
	if(inputMem.size() > 0) {
		return &inputMem[partitionIndex][overlapPartitionIndex];
	} else {
		return NULL;
	}
}

void marrow::KernelWrapper::setPartitionedOutput(const std::vector<std::shared_ptr<PartitionedInfo>> &partitionedOutput) {
	unsigned int i, j, w;
	unsigned int numOverlapPartitions = marrow::Skeleton::scheduler->getNumOverlapPartitions();
	unsigned int numPartitions = marrow::Skeleton::scheduler->getNumDevices();

	this->hasPartitions = true;
	this->partitionedOutput = partitionedOutput;

	if(shouldAllocOutput) {
		outputMem.resize(numPartitions);

		for(w = 0; w < numPartitions; w++) {
			if((*workSize)[w][0][globalWorkSize.size() - 1] == 0) {
				outputMem[w].resize(0);
				continue;
			} else {
				outputMem[w].resize(numOverlapPartitions);
			}
			for(j = 0; j < numOverlapPartitions; j++) {
				outputMem[w][j].resize(partitionedOutput.size());
				for (i = 0; i < partitionedOutput.size(); i++) {
					// There are no singletons on the output
					PartitionedInfo::DataSegment curr = partitionedOutput[i]->getSegment(w, j);

					outputMem[w][j][i] = (*marrow::Skeleton::executionPlatform).allocateDeviceMem(*curr.segmentInfo, w, j);
				}
			}
		}
	}
}

std::vector<std::shared_ptr<PartitionedInfo>> * marrow::KernelWrapper::getPartitionedOutput() {
	return &partitionedOutput;
}

std::vector<cl_mem> * marrow::KernelWrapper::getOutputMem(unsigned int partitionIndex, unsigned int overlapPartitionIndex) {
	return &outputMem[partitionIndex][overlapPartitionIndex];
}

std::vector<size_t> * marrow::KernelWrapper::getWorkSize(unsigned int partitionIndex, unsigned int overlapPartitionIndex) {
	return &(*workSize)[partitionIndex][overlapPartitionIndex];
}

void marrow::KernelWrapper::setWorkSize(std::shared_ptr<std::vector<std::vector<std::vector<size_t>>>> dimensionSize) {
	workSize = dimensionSize;
}

std::vector<void*> * marrow::KernelWrapper::getNestedExecutables() {
	std::vector<void*> * v = new std::vector<void*>(1);
	(*v)[0] = this;

	return v;
}

bool marrow::KernelWrapper::hasSetPartitions() {
	return this->hasPartitions;
}

bool marrow::KernelWrapper::isDoublePrecision() {
	return isDPrecision;
}

void marrow::KernelWrapper::requiresInputMemory(bool shouldAlloc) {
	shouldAllocInput = shouldAlloc;

	if(!shouldAllocInput && hasPartitions) {
		clearInputMem();
	}

	// Subtract the memory required for the input.
	for(unsigned int i = 0; i < inWorkData.size(); i++) {
		switch (inWorkData[i]->getDataType()) {
		case IWorkData::BUFFER:
			if(inWorkData[i]->getPartitionMode() == IWorkData::PARTITIONABLE) {
				unsigned int numElems = inWorkData[i]->getWidth() * inWorkData[i]->getHeight() * inWorkData[i]->getDepth();

				dynamicMem -= inWorkData[i]->getIndivisibleSize() * (inWorkData[i]->getMemSize() / numElems);
			} else {
				staticMem -= inWorkData[i]->getMemSize();
			}
			break;
		default:
			break;
		}
	}
}

void marrow::KernelWrapper::requiresOutputMemory(bool shouldAlloc) {
	shouldAllocOutput = shouldAlloc;

	if(!shouldAllocOutput && hasPartitions) {
		clearOutputMem();
	}

	// Subtract the memory required for the output.
	for(unsigned int i = 0; i < outWorkData.size(); i++) {
		if(outWorkData[i]->getPartitionMode() == IWorkData::PARTITIONABLE) {
			unsigned int numElems = outWorkData[i]->getWidth() * outWorkData[i]->getHeight() * outWorkData[i]->getDepth();

			dynamicMem -= outWorkData[i]->getIndivisibleSize() * (outWorkData[i]->getMemSize() / numElems);
		} else {
			staticMem -= outWorkData[i]->getMemSize();
		}
	}
}

#ifdef PROFILING
void marrow::KernelWrapper::setProfilingInfo(cl_event event, unsigned int deviceIndex) {
	cl_ulong time_start, time_end;
	double total_time;

	clWaitmarrow::ForEvents(1, &event);

	clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_START, sizeof(time_start), &time_start, NULL);
	clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, sizeof(time_end), &time_end, NULL);
	if(time_end > time_start) {
		total_time = time_end - time_start;
		//printf("\nExecution time in milliseconds = %0.3f us (%lu || %lu)\n", total_time, time_start, time_end);
		runSum[deviceIndex] += total_time;
		numRuns[deviceIndex]++;
		deviceRunDurations[deviceIndex].push_back(total_time / 1000000.0);
	}

	clReleaseEvent(event);
}

double stdDeviation(double * values, unsigned int numValues) {
    unsigned int i;
    double avg = 0.0;
    double variance = 0.0;

    for(i = 0; i < numValues; i++) {
        avg += values[i];
    }

    avg /= numValues;

    for(i = 0; i < numValues; i++) {
        variance += (values[i] - avg) * (values[i] - avg);
    }

    variance /= numValues-1;

    return sqrt(variance);
}

std::string marrow::KernelWrapper::getProfilingInfo(unsigned int indent) {
	std::stringstream s;

	std::list<ProfilerTask*>::iterator it;

	for(it = profilerQueue.begin(); it != profilerQueue.end(); it++) {
		setProfilingInfo((*it)->event, (*it)->deviceIndex);
	}

	for(unsigned int j = 0; j < indent; j++) {
		s << "  ";
	}

	s << kernelFunc << std::endl;

	for(unsigned int i = 0; i < (*marrow::Skeleton::executionPlatform).getNumDevices(); i++) {
		for(unsigned int j = 0; j < indent; j++) {
			s << "  ";
		}
		s << "Device #" << i << ": Avg runtime = " << (runSum[i] / (1.0 * numRuns[i]) / 1000000.0) << "||" << stdDeviation(deviceRunDurations[i].data(), deviceRunDurations[i].size()) << " ms (" << numRuns[i] << " runs || " <<deviceRunDurations[i].size() << ")" << std::endl;
	}

	return s.str();
}
#endif

/* Private functions */

void marrow::KernelWrapper::clearInputMem() {
	unsigned int i, j, w, z = 0;

	for(i = 0; i < inputMem.size(); i++) {
		for(j = 0; j < inputMem[i].size(); j++) {
			for(w = 0; w < inputMem[i][j].size(); w++) {
				for(; z < partitionedInput.size(); z++) {
					if(partitionedInput[z]) {
						break;
					}
				}

				(*marrow::Skeleton::executionPlatform).freeDeviceMem(*partitionedInput[z]->getSegment(i, j).segmentInfo, i, inputMem[i][j][w]);
				z++;
			}
			z = 0;
			inputMem[i][j].resize(0);
		}
	}
}

void marrow::KernelWrapper::clearOutputMem() {
	unsigned int i, j, w;

	for(i = 0; i < outputMem.size(); i++) {
		for(j = 0; j < outputMem[i].size(); j++) {
			for(w = 0; w < outputMem[i][j].size(); w++) {
				(*marrow::Skeleton::executionPlatform).freeDeviceMem(*partitionedOutput[w]->getSegment(i, j).segmentInfo, i, outputMem[i][j][w]);
			}
		}
	}
}
