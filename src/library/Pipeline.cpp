/*
 * marrow::Pipeline.cpp
 *
 *  Created on: 23 de Mar de 2012
 *      Author: Ricardo Marques
 *			Modified by Fernando Alexandre
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "marrow/Pipeline.hpp"
#include "marrow/utils/Debug.hpp"
#include "marrow/exceptions/ExecutionException.hpp"
#include "marrow/exceptions/NotEnoughMemoryException.hpp"

#include "runtime/OpenCLErrorParser.hpp"


marrow::Pipeline::Pipeline(std::unique_ptr<marrow::IExecutable> &stage1, std::unique_ptr<marrow::IExecutable> &stage2):
marrow::Skeleton(*(*stage1).getInputDataInfo(), *(*stage2).getOutputDataInfo()),
stage1(std::move(stage1)),
stage2(std::move(stage2)),
childReads(false),
globalMemSize(0),
middleInfo(*(*this->stage1).getOutputDataInfo())
{
	initPipe(false);
}

marrow::Pipeline::Pipeline(std::unique_ptr<marrow::IExecutable> &stage1, std::unique_ptr<marrow::IExecutable> &stage2, unsigned int numDevices):
marrow::Skeleton(*(*stage1).getInputDataInfo(), *(*stage2).getOutputDataInfo(), numDevices),
stage1(std::move(stage1)),
stage2(std::move(stage2)),
childReads(false),
globalMemSize(0),
middleInfo(*(*this->stage1).getOutputDataInfo())
{
	initPipe(true);
}

marrow::Pipeline::Pipeline(std::unique_ptr<marrow::IExecutable> &stage1, std::unique_ptr<marrow::IExecutable> &stage2, unsigned int numDevices, unsigned int numOverlapPartitions):
marrow::Skeleton(*(*stage1).getInputDataInfo(), *(*stage2).getOutputDataInfo(), numDevices, numOverlapPartitions),
stage1(std::move(stage1)),
stage2(std::move(stage2)),
childReads(false),
globalMemSize(0),
middleInfo(*(*this->stage1).getOutputDataInfo())
{
	initPipe(true);
}

void marrow::Pipeline::initPipe(bool isReconfigure) {
	unsigned int i, j, w;

	if(isReconfigure) {
		(*this->stage1).clearConfiguration();
		(*this->stage2).clearConfiguration();
	}

	std::vector<std::shared_ptr<IWorkData>> * stage2input = (std::vector<std::shared_ptr<IWorkData>>*) (*stage2).getInputDataInfo();
	std::vector<std::shared_ptr<IWorkData>> * stage1output = (std::vector<std::shared_ptr<IWorkData>>*) (*stage1).getOutputDataInfo();

	globalMemSize = (*stage1).requiredGlobalMem() + (*stage2).requiredGlobalMem();

	// Check compatibility between stages
	for(i = 0; i < (*stage1output).size(); i++){
		if(!(*stage1output)[i]->isEqual(*(*stage2input)[i].get())){
			throw DataMismatchException(PIPEDATAMISMATCH);
		}
		// subtract redundancy between stages
		globalMemSize -= (*stage1output)[i]->getMemSize();
	}

	childReads = (*stage2).readsData();
	for(i = 0; i < inputInfo.size(); i++) {
		if(inputInfo[i]->getDataType() == IWorkData::SINGLETON) {
			numSingleton++;
		}
	}

	localMemSize = (*stage1).requiredLocalMem() + (*stage2).requiredLocalMem();

	DEBUG_MSG("*marrow::Skeleton::executionPlatform).getTotalFreeMemorySize() %lu < globalMemSize %lu", (*marrow::Skeleton::executionPlatform).getTotalFreeMemorySize(), globalMemSize);
	if((*marrow::Skeleton::executionPlatform).getTotalFreeMemorySize() < globalMemSize) {
		throw NotEnoughMemoryException();
	}

	// Stage 1 does not require output allocation (it is the middleMem)
	(*stage1).requiresOutputMemory(false);
	// Stage 2 does not require input allocation (it is the middleMem)
	(*stage2).requiresInputMemory(false);

	setPartitions(isReconfigure);

	middleDynamicMem = 0L;
	middleStaticMem = 0L;

	for(i = 0; i < (*stage1output).size(); i++) {
		if((*stage1output)[i]->getDataType() == IWorkData::BUFFER) {
			if((*stage1output)[i]->getPartitionMode() == IWorkData::PARTITIONABLE) {
				unsigned int numElems = (*stage1output)[i]->getWidth() * (*stage1output)[i]->getHeight() * (*stage1output)[i]->getDepth();

				middleDynamicMem += (*stage1output)[i]->getIndivisibleSize() * ((*stage1output)[i]->getMemSize() / numElems);
			} else {
				middleStaticMem += (*stage1output)[i]->getMemSize();
			}
		}
	}

	// Alloc the middle memory
	std::vector<std::shared_ptr<PartitionedInfo>> middlePartitionedInfo = *(*stage1).getPartitionedOutput();
	// It is assumed there are numPartitions = numDevices
	unsigned int numPartitions = (*marrow::Skeleton::scheduler).getNumDevices();
	// And numOverlapPartitions per device
	unsigned int numOverlapPartitions = (*marrow::Skeleton::scheduler).getNumOverlapPartitions();

	middleMem.resize(numPartitions);

	for(w = 0; w < numPartitions; w++) {
		middleMem[w].resize(numOverlapPartitions);
		for(j = 0; j < numOverlapPartitions; j++) {
			middleMem[w][j].resize(middlePartitionedInfo.size());
			for (i = 0; i < middlePartitionedInfo.size(); i++) {
				PartitionedInfo::DataSegment curr = middlePartitionedInfo[i]->getSegment(w, j);

				middleMem[w][j][i] = (*marrow::Skeleton::executionPlatform).allocateDeviceMem(*curr.segmentInfo, w, j);
			}
		}
	}

	if(!(*stage1).isInitialized()) {
		(*stage1).initExecutable();
	}

	if(!(*stage2).isInitialized()) {
		(*stage2).initExecutable();
	}

	if(isReconfigure) {
		(*this->stage1).reconfigureExecutable();
		(*this->stage2).reconfigureExecutable();
	}

	isInit = true;
}

marrow::Pipeline::~Pipeline() {
	unsigned int i, j, w;
	closed = true;

	std::vector<std::shared_ptr<PartitionedInfo>> middlePartitionedInfo = *(*stage1).getPartitionedOutput();


	for(i = 0; i < middleMem.size(); i++) {
		for(j = 0; j < middleMem[i].size(); j++) {
			for(w = 0; w < middleMem[i][j].size(); w++) {
				DEBUG_MSG("Deleting middleMem[%u][%u][%u] = %p; middleMem.size() = %lu", i, j, w, middleMem[i][j][w], (unsigned long int) middleMem.size());
				(*marrow::Skeleton::executionPlatform).freeDeviceMem(*middlePartitionedInfo[w]->getSegment(i, j).segmentInfo, i,this->middleMem[i][j][w]);
			}
		}
	}

	for(i = 0; i < middlePartitionedInfo.size(); i++) {
		middlePartitionedInfo[i].reset();
	}

	for(i = 0; i < middleInfo.size(); i++) {
		middleInfo[i].reset();
	}

	for(i = 0; i < partitionedInput.size(); i++) {
		partitionedInput[i].reset();
	}

	for(i = 0; i < partitionedOutput.size(); i++) {
		partitionedOutput[i].reset();
	}

	stage1.reset();
	stage2.reset();
}

bool marrow::Pipeline::isInitialized() {
	return isInit;
}

void marrow::Pipeline::initExecutable() {
	(*stage1).initExecutable();
	(*stage2).initExecutable();

	isInit = true;
}

void marrow::Pipeline::clearConfiguration() {
	unsigned int i, j, w;

	std::vector<std::shared_ptr<PartitionedInfo>> middlePartitionedInfo = *(*stage1).getPartitionedOutput();

	(*stage1).clearConfiguration();
	(*stage2).clearConfiguration();

	for(i = 0; i < middleMem.size(); i++) {
		for(j = 0; j < middleMem[i].size(); j++) {
			for(w = 0; w < middleMem[i][j].size(); w++) {
				(*marrow::Skeleton::executionPlatform).freeDeviceMem(*middlePartitionedInfo[w]->getSegment(i, j).segmentInfo, i,this->middleMem[i][j][w]);
			}
		}
	}
}

void marrow::Pipeline::reconfigureExecutable() {
	unsigned int i, j, w;

	(*stage1).reconfigureExecutable();
	(*stage2).reconfigureExecutable();

	std::vector<std::shared_ptr<PartitionedInfo>> middlePartitionedInfo = *(*stage1).getPartitionedOutput();
	// It is assumed there are numPartitions = numDevices
	unsigned int numOverlapPartitions = (*marrow::Skeleton::scheduler).getNumOverlapPartitions();
	// Assumes numOverlapPartitions per device.
	unsigned int numPartitions = (*marrow::Skeleton::scheduler).getNumDevices();

	middleMem.resize(numPartitions);

	for(w = 0; w < numPartitions; w++) {
		middleMem[w].resize(numOverlapPartitions);
		for(j = 0; j < numOverlapPartitions; j++) {
			middleMem[w][j].resize(middlePartitionedInfo.size());
			for (i = 0; i < middlePartitionedInfo.size(); i++) {
				PartitionedInfo::DataSegment curr = middlePartitionedInfo[i]->getSegment(w, j);

				DEBUG_MSG("Allocation MiddleMem #%u for Partition #%u and OverlapPartition #%u", i, w, j);

				middleMem[w][j][i] = (*marrow::Skeleton::executionPlatform).allocateDeviceMem(*curr.segmentInfo, w, j);
			}
		}
	}
}


cl_event marrow::Pipeline::execute(cl_command_queue executionQueue, unsigned int deviceIndex, unsigned int uniqueId, unsigned int partitionIndex, unsigned int overlapPartition, std::vector<cl_mem> &inputData, std::vector<void*> &singletonInputValues, std::vector<cl_mem> &outputData, cl_event waitEvent, std::vector<std::shared_ptr<marrow::Vector>> &resultMem) {
	cl_event lastCommand;

	lastCommand = execPipe(executionQueue, deviceIndex, uniqueId, partitionIndex, overlapPartition, inputData, singletonInputValues, middleMem[partitionIndex][overlapPartition], outputData, waitEvent, resultMem);

	return lastCommand;
}

cl_event marrow::Pipeline::execPipe(const cl_command_queue &executionQueue, const unsigned int deviceIndex, const unsigned int uniqueId, const unsigned int partitionIndex, const unsigned int overlapPartition, std::vector<cl_mem> &inputMem, std::vector<void*> singletonInputValues, std::vector<cl_mem> &middleMem, std::vector<cl_mem> &outputMem, cl_event waitEvent, std::vector<std::shared_ptr<marrow::Vector>> &resultMem) {
	UNUSED(waitEvent);
	cl_event s1Event, s2Event;
	std::vector<std::shared_ptr<marrow::Vector>> empty(0);
	std::vector<void*> emptyPtr(0);

	s1Event = (*this->stage1).execute(executionQueue, deviceIndex, uniqueId, partitionIndex, overlapPartition, inputMem, singletonInputValues, middleMem, NULL, empty);
	s2Event = (*this->stage2).execute(executionQueue, deviceIndex, uniqueId, partitionIndex, overlapPartition, middleMem, emptyPtr, outputMem, NULL, resultMem);
	// If the profiling is not on the events must be released by the responsible entity
	#ifndef PROFILING
	clReleaseEvent(s1Event);
	#endif
	return s2Event;
}

void marrow::Pipeline::executeSkel(const cl_command_queue &executionQueue, const unsigned int deviceIndex, const unsigned int uniqueId, const unsigned int partitionIndex, const unsigned int overlapPartition, std::vector<std::shared_ptr<marrow::Vector>> &inputData, std::vector<std::shared_ptr<marrow::Vector>> &outputData, std::vector<cl_mem> &inputMem, std::vector<cl_mem> &outputMem) {
	unsigned int i, j = 0;
	unsigned int inputSize = (*stage1).getNumInputEntries();
	std::vector<void*> singletonValues(numSingleton);
	std::vector<std::shared_ptr<IWorkData>> * inputInfo = (std::vector<std::shared_ptr<IWorkData>>*) (*stage1).getInputDataInfo();

	DEBUG_MSG("Starting execution @ partitionIndex = %u overlapPartition = %u", partitionIndex, overlapPartition);

	for(i = 0, j = 0; i < inputSize; i++){
		if((*inputInfo)[i]->getDataType() == IWorkData::SINGLETON) {
			singletonValues[j] = inputData[i]->get(0);
			j++;
		}
	}

	cl_event exec = NULL;
	try {
		if(childReads) {
			exec = execPipe(executionQueue, deviceIndex, uniqueId, partitionIndex, overlapPartition, inputMem, singletonValues, middleMem[partitionIndex][overlapPartition], outputMem, NULL, outputData);
		} else {
			std::vector<std::shared_ptr<marrow::Vector>> empty(0);
			exec = execPipe(executionQueue, deviceIndex, uniqueId, partitionIndex, overlapPartition, inputMem, singletonValues, middleMem[partitionIndex][overlapPartition], outputMem, NULL, empty);
		}
	}
	catch (ExecutionException& e) {
		// TODO: perguntar como tratar em caso de exception: se deixo morrer o prog, ou meto o future a apontar para NULL
		// If the profilling is not on the events must be released by the responsible entity
		#ifndef PROFILING
		clReleaseEvent(exec);
		#endif

		throw;
	}
	// If the profilling is not on the events must be released by the responsible entity
	#ifndef PROFILING
	clReleaseEvent(exec);
	#endif

	DEBUG_MSG("Finished execution @ partitionIndex = %u", partitionIndex);
}

void marrow::Pipeline::uploadInputData(const cl_command_queue &executionQueue, const unsigned int deviceIndex, const unsigned int uniqueId, const unsigned int partitionIndex, const unsigned int overlapPartition, std::vector<std::shared_ptr<marrow::Vector>> &inputData, std::vector<cl_mem> &inputMem) {
	unsigned int i = 0, j = 0, w = 0;
	int errcode;
	UNUSED(uniqueId);
	UNUSED(deviceIndex);

	size_t origin[3] = { 0, 0, 0 };
	size_t region[3] = { 0, 0, 0 };
	unsigned int inputSize = getNumInputEntries();
	unsigned int outputSize = getNumOutputEntries();
	const std::vector<std::shared_ptr<IWorkData>> * inputInfo = getInputDataInfo();
	std::vector<std::shared_ptr<PartitionedInfo>> * partitionedInput = getPartitionedInput();

	// write the input data to device memory
	if(inputData.size() != outputSize) {
		w += numSingleton;
	}

	for(i = 0; i < inputSize; i++) {
		// Its either buffer or image2d
		if((*partitionedInput)[i]) {
			if(inputData[w]->requiresUpdate()) {
				PartitionedInfo::DataSegment curr = (*partitionedInput)[i]->getSegment(partitionIndex, overlapPartition);
				void* inData = inputData[w]->get(curr.begin_cpy);
				cl_mem inMem = inputMem[j];

				unsigned long memSize = curr.segmentInfo->getMemSize();

				DEBUG_MSG("Uploading%s", ":");
				DEBUG_MSG("inputMem.size = %u; inputData.size = %u", inputMem.size(), inputData.size());
				DEBUG_MSG("Set inputMem[%u] = %p; inputData[%u] = %p;", j, &(inputMem[j]), i, inData);
				DEBUG_MSG("begin = %u; size = %lu; address = %p", curr.begin_cpy, memSize, inData);

				w++;
				j++;

				switch ((*inputInfo)[i]->getDataType()) {
				case IWorkData::BUFFER:
					errcode = clEnqueueWriteBuffer(executionQueue, inMem, CL_FALSE, 0, memSize, inData, 0, NULL, NULL);
					break;
				case IWorkData::IMAGE2D:
					region[0] = (*inputInfo)[i]->getWidth();
					region[1] = (*inputInfo)[i]->getHeight();
					region[2] = (*inputInfo)[i]->getDepth();
					errcode = clEnqueueWriteImage(executionQueue, inMem, CL_FALSE, origin, region, 0, 0, inData, 0, NULL, NULL);
					break;
				default:
					break;
				}
				if(errcode != CL_SUCCESS) {
					throw ExecutionException(OpenCLErrorParser::errorString(errcode));
				}
			}
		}
	}

	DEBUG_MSG("Finished upload @ partitionIndex = %u", partitionIndex);
}

void marrow::Pipeline::downloadOutputData(const cl_command_queue &executionQueue, const unsigned int deviceIndex, const unsigned int uniqueId, const unsigned int partitionIndex, const unsigned int overlapPartition, std::vector<std::shared_ptr<marrow::Vector>> &outputData, std::vector<cl_mem> &outputMem) {
	unsigned int i;
	size_t origin[3] = { 0, 0, 0 };
	size_t region[3] = { 0, 0, 0 };
	int errcode;
	unsigned int outputSize = (*stage2).getNumOutputEntries();
	const std::vector<std::shared_ptr<IWorkData>> * outputInfo = getOutputDataInfo();
	std::vector<std::shared_ptr<PartitionedInfo>> * partitionedOutput = getPartitionedOutput();
	UNUSED(uniqueId);
	UNUSED(deviceIndex);

	DEBUG_MSG("Starting download @ partitionIndex = %u", partitionIndex);

	if(!childReads) {
		for(i = 0; i < outputSize; i++) {
			if((*partitionedOutput)[i]) {
				PartitionedInfo::DataSegment curr = (*partitionedOutput)[i]->getSegment(partitionIndex, overlapPartition);
				void* outData = outputData[i]->get(curr.begin_comp);
				cl_mem outMem = outputMem[i];
				unsigned long memSize = curr.segmentInfo->getMemSize();

				DEBUG_MSG("Downloading begin = %u; size = %lu; address = %p", curr.begin_comp, memSize, outData);

				switch ((*outputInfo)[i]->getDataType()) {
				case IWorkData::BUFFER:
					errcode = clEnqueueReadBuffer(executionQueue, outMem, CL_FALSE, 0, memSize, outData, 0, NULL, NULL);
					break;
				case IWorkData::IMAGE2D:
					region[0] = (*outputInfo)[i]->getWidth();
					region[1] = (*outputInfo)[i]->getHeight();
					region[2] = (*outputInfo)[i]->getDepth();
					errcode = clEnqueueReadImage(executionQueue, outMem, CL_FALSE, origin, region, 0, 0, outData, 0, NULL, NULL);
					break;
				default:
					break;
				}
				if(errcode != CL_SUCCESS) {
					throw ExecutionException(OpenCLErrorParser::errorString(errcode));
				}
			}
		}
	}
	DEBUG_MSG("Finished upload @ partitionIndex = %u", partitionIndex);
}

std::vector<void*> * marrow::Pipeline::getExecutables() {
	return getNestedExecutables();
}

void marrow::Pipeline::finishExecution(const unsigned int uniqueId, std::vector<std::shared_ptr<marrow::Vector>> &outputData) {
	UNUSED(uniqueId);
	UNUSED(outputData);
}

const std::vector<std::shared_ptr<IWorkData>> * marrow::Pipeline::getInputDataInfo() {
	return (*stage1).getInputDataInfo();
}

unsigned int marrow::Pipeline::getNumInputEntries() {
	return (*stage1).getNumInputEntries();
}

const std::vector<std::shared_ptr<IWorkData>> * marrow::Pipeline::getOutputDataInfo() {
	return (*stage2).getOutputDataInfo();
}

unsigned int marrow::Pipeline::getNumOutputEntries() {
	return (*stage2).getNumOutputEntries();
}

unsigned long marrow::Pipeline::requiredStaticMem() {
	return (*stage1).requiredStaticMem() + (*stage2).requiredStaticMem() - middleStaticMem;
}

unsigned long marrow::Pipeline::requiredDynamicMem() {
	return (*stage1).requiredDynamicMem() + (*stage2).requiredDynamicMem() - middleDynamicMem;
}

unsigned long marrow::Pipeline::requiredGlobalMem() {
	return globalMemSize;
}

unsigned long marrow::Pipeline::requiredLocalMem() {
	return localMemSize;
}

bool marrow::Pipeline::readsData() {
	return childReads;
}

std::vector<std::shared_ptr<PartitionedInfo>> * marrow::Pipeline::getPartitionedInput() {
	return (*stage1).getPartitionedInput();
}

std::vector<cl_mem> * marrow::Pipeline::getInputMem(unsigned int partitionIndex, unsigned int overlapPartitionIndex) {
	return (*stage1).getInputMem(partitionIndex, overlapPartitionIndex);
}

std::vector<std::shared_ptr<PartitionedInfo>> * marrow::Pipeline::getPartitionedOutput() {
	return (*stage2).getPartitionedOutput();
}

std::vector<cl_mem> * marrow::Pipeline::getOutputMem(unsigned int partitionIndex, unsigned int overlapPartitionIndex) {
	return (*stage2).getOutputMem(partitionIndex, overlapPartitionIndex);
}

std::vector<void*> * marrow::Pipeline::getNestedExecutables() {
	std::vector<void*> * v = (*stage1).getNestedExecutables();
	std::vector<void*> * v2 = (*stage2).getNestedExecutables();
	v->insert(v->end(), v2->begin(), v2->end());

	//delete v2;
	return v;
}

bool marrow::Pipeline::hasSetPartitions() {
	return this->hasPartitions;
}

bool marrow::Pipeline::isDoublePrecision() {
	return (*stage1).isDoublePrecision() || (*stage2).isDoublePrecision();
}

void marrow::Pipeline::requiresInputMemory(bool shouldAlloc) {
	(*stage1).requiresInputMemory(shouldAlloc);
}

void marrow::Pipeline::requiresOutputMemory(bool shouldAlloc) {
	(*stage2).requiresOutputMemory(shouldAlloc);
}


#ifdef PROFILING
std::string marrow::Pipeline::getProfilingInfo() {
	return getProfilingInfo(1);
}

std::string marrow::Pipeline::getProfilingInfo(unsigned int indent) {
	std::stringstream s;

	for(unsigned int j = 0; j < indent; j++) {
		s << "  ";
	}

	s << "marrow::Pipeline:" << std::endl;

	for(unsigned int j = 0; j < indent+1; j++) {
		s << "  ";
	}
	s << "1st Stage:" << std::endl << (*stage1).getProfilingInfo(indent+2);

	for(unsigned int j = 0; j < indent+1; j++) {
		s << "  ";
	}
	s << "2nd Stage:" << std::endl << (*stage2).getProfilingInfo(indent+2);

	return s.str();
}
#endif

