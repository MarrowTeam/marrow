/*
 * marrow::Future.cpp
 *
 *  Created on: 23 de Fev de 2012
 *      Author: Ricardo Marques
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "Future.hpp"

marrow::Future::Future():
isDataReady(false)
{
}

marrow::Future::~Future(){}

bool marrow::Future::isReady(){
	return isDataReady;
}

void marrow::Future::setData(std::vector<std::shared_ptr<marrow::Vector>> *data){
	if (isDataReady)
		return;

	boost::lock_guard<boost::mutex> lock(mut);
	this->dataVector = data;
	isDataReady = true;
	cond.notify_all();
}

std::vector<std::shared_ptr<marrow::Vector>>* marrow::Future::data(){
	if(!isDataReady){
		boost::unique_lock<boost::mutex> lock(mut);
		while(!isDataReady){
			cond.wait(lock);
		}
	}
	return dataVector;
}

void marrow::Future::wait(){
	boost::unique_lock<boost::mutex> lock(mut);
	while(!isDataReady){
		cond.wait(lock);
	}
}
