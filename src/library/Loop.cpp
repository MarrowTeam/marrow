/*
 * marrow::Loop.cpp
 *
 *  Created on: 19 de Abr de 2012
 *      Author: Ricardo Marques
 *			Modified by Fernando Alexandre
 *
 *  Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "marrow/Loop.hpp"
#include "marrow/utils/Debug.hpp"
#include "marrow/exceptions/ExecutionException.hpp"
#include "marrow/exceptions/DataMismatchException.hpp"
#include "marrow/exceptions/NotSupportedDatatypeException.hpp"

#include "runtime/OpenCLErrorParser.hpp"


marrow::Loop::Loop(std::unique_ptr<marrow::IExecutable> &exec):
marrow::Skeleton(*(*exec).getInputDataInfo(), *(*exec).getOutputDataInfo()),
exec(std::move(exec)),
partialRead(READBACKPARTIAL),
childReads(false),
stepSync(false)
{
	initCycle(false);
}

marrow::Loop::Loop(std::unique_ptr<marrow::IExecutable> &exec, unsigned int numDevices):
marrow::Skeleton(*(*exec).getInputDataInfo(), *(*exec).getOutputDataInfo(), numDevices),
exec(std::move(exec)),
partialRead(READBACKPARTIAL),
childReads(false),
stepSync(false)
{
	initCycle(true);
}

marrow::Loop::Loop(std::unique_ptr<marrow::IExecutable> &exec, unsigned int numDevices, unsigned int numOverlapPartitions):
marrow::Skeleton(*(*exec).getInputDataInfo(), *(*exec).getOutputDataInfo(), numDevices, numOverlapPartitions),
exec(std::move(exec)),
partialRead(READBACKPARTIAL),
childReads(false),
stepSync(false)
{
	setPartitions(true);
}

marrow::Loop::Loop(std::unique_ptr<marrow::IExecutable> &exec, bool readbackPartial):
marrow::Skeleton(*(*exec).getInputDataInfo(), *(*exec).getOutputDataInfo()),
exec(std::move(exec)),
partialRead(readbackPartial),
childReads(false),
stepSync(false)
{
	initCycle(false);
}

marrow::Loop::Loop(std::unique_ptr<marrow::IExecutable> &exec, bool readbackPartial, unsigned int numDevices):
marrow::Skeleton(*(*exec).getInputDataInfo(), *(*exec).getOutputDataInfo(), numDevices),
exec(std::move(exec)),
partialRead(readbackPartial),
childReads(false),
stepSync(false)
{
	initCycle(true);
}

marrow::Loop::Loop(std::unique_ptr<marrow::IExecutable> &exec, bool readbackPartial, unsigned int numDevices, unsigned int numOverlapPartitions):
marrow::Skeleton(*(*exec).getInputDataInfo(), *(*exec).getOutputDataInfo(), numDevices, numOverlapPartitions),
exec(std::move(exec)),
partialRead(readbackPartial),
childReads(false),
stepSync(false)
{
	initCycle(true);
}

marrow::Loop::Loop(std::unique_ptr<marrow::IExecutable> &exec, bool readbackPartial, bool stepSync):
marrow::Skeleton(*(*exec).getInputDataInfo(), *(*exec).getOutputDataInfo()),
exec(std::move(exec)),
partialRead(readbackPartial),
childReads(false),
stepSync(stepSync)
{
	initCycle(false);
}

marrow::Loop::Loop(std::unique_ptr<marrow::IExecutable> &exec, bool readbackPartial, bool stepSync, unsigned int numDevices):
marrow::Skeleton(*(*exec).getInputDataInfo(), *(*exec).getOutputDataInfo(), numDevices),
exec(std::move(exec)),
partialRead(readbackPartial),
childReads(false),
stepSync(stepSync)
{
	initCycle(true);
}

marrow::Loop::Loop(std::unique_ptr<marrow::IExecutable> &exec, bool readbackPartial, bool stepSync, unsigned int numDevices, unsigned int numOverlapPartitions):
marrow::Skeleton(*(*exec).getInputDataInfo(), *(*exec).getOutputDataInfo(), numDevices, numOverlapPartitions),
exec(std::move(exec)),
partialRead(readbackPartial),
childReads(false),
stepSync(stepSync)
{
	initCycle(true);
}

marrow::Loop::~Loop() {
	exec.reset();
}

void marrow::Loop::initCycle(bool isReconfigure) {
	childReads = (*exec).readsData();
	unsigned int numDataElems = inputInfo.size();
	unsigned int i, j;

	// parse the input argument information
	for(i = 0, j = 0; i < numDataElems; i++) {
		if(inputInfo[i]->getDataType() == IWorkData::SINGLETON) {
			numSingleton++;
		}
		else {
			if(!inputInfo[i]->isEqual(*outputInfo[j].get())) {
				throw DataMismatchException(LOOPDATAMISMATCH);
			}
			j++;
		}
	}

	if(isReconfigure) {
		(*exec).clearConfiguration();
	}

	setPartitions(isReconfigure);

	localMemSize = (*this->exec).requiredLocalMem();

	if(!(*exec).isInitialized()) {
		(*exec).initExecutable();
	}

	if(isReconfigure) {
		(*exec).reconfigureExecutable();
	}

	isInit = true;
}

void marrow::Loop::executeSkel(const cl_command_queue &executionQueue, const unsigned int deviceIndex, const unsigned int uniqueId, const unsigned int partitionIndex, const unsigned int overlapPartition, std::vector<std::shared_ptr<marrow::Vector>> &inputData, std::vector<std::shared_ptr<marrow::Vector>> &outputData, std::vector<cl_mem> &inputMem, std::vector<cl_mem> &outputMem) {
	unsigned int i,j;
	UNUSED(uniqueId);

	DEBUG_MSG("Begining ExecuteSkel%s", ".");

	std::vector<void*> singletonValues(numSingleton);
	std::vector<std::shared_ptr<IWorkData>> * inputInfo = (std::vector<std::shared_ptr<IWorkData>>*) (*exec).getInputDataInfo();
	bool reads = readsData();
	cl_event execEvent = NULL;

	for(i = 0, j = 0; i < (*inputInfo).size(); i++) {
		if((*inputInfo)[i]->getDataType() == IWorkData::SINGLETON) {
			singletonValues[j] = inputData[i]->get(0);
			DEBUG_MSG("singletonValues[%u] = %u", j, *((unsigned int *) singletonValues[j]));
			j++;
		}
	}

	try {
		DEBUG_MSG("reads = %i", reads);
		// if this marrow::Skeleton reads the data at the end of each iteration
		if(reads) {
			execEvent = execute(executionQueue, deviceIndex, uniqueId, partitionIndex, overlapPartition, inputMem, singletonValues, outputMem, NULL, outputData);
		}
		// otherwise
		else {
			std::vector<std::shared_ptr<marrow::Vector>> empty(0);
			execEvent = execute(executionQueue, deviceIndex, uniqueId, partitionIndex, overlapPartition, inputMem, singletonValues, outputMem, NULL, empty);
		}
	}
	catch (ExecutionException& e) {
		// If profiling mode is not on all events must be released by the responsible entity.
		#ifndef PROFILING
		clReleaseEvent(execEvent);
		#endif
		DEBUG_MSG("Exception: %s", e.what());
		throw e;
	}
	// If profiling mode is not on all events must be released by the responsible entity.
	#ifndef PROFILING
	clReleaseEvent(execEvent);
	#endif
}

void marrow::Loop::uploadInputData(const cl_command_queue &executionQueue, const unsigned int deviceIndex, const unsigned int uniqueId, const unsigned int partitionIndex, const unsigned int overlapPartition, std::vector<std::shared_ptr<marrow::Vector>> &inputData, std::vector<cl_mem> &inputMem) {
	unsigned int i = 0, j = 0, w = 0;
	int errcode;
	UNUSED(uniqueId);
	UNUSED(deviceIndex);

	size_t origin[3] = { 0, 0, 0 };
	size_t region[3] = { 0, 0, 0 };
	unsigned int inputSize = (*exec).getNumInputEntries();
	unsigned int outputSize = (*exec).getNumOutputEntries();
	const std::vector<std::shared_ptr<IWorkData>> * inputInfo = getInputDataInfo();
	std::vector<std::shared_ptr<PartitionedInfo>> * partitionedInput = (*exec).getPartitionedInput();

	// Offset the inputData index to pass by the singletons
	// Assumes there are no singletons after the buffer objects.
	if(inputData.size() != outputSize) {
		w += numSingleton;
	}

	for(i = 0; i < inputSize; i++) {
		// Its either buffer or image2d
		if((*partitionedInput)[i]) {
			if(inputData[w]->requiresUpdate()) {
				PartitionedInfo::DataSegment curr = (*partitionedInput)[i]->getSegment(partitionIndex, overlapPartition);
				void* inData = inputData[w]->get(curr.begin_cpy);
				cl_mem inMem = inputMem[j];

				unsigned long memSize = curr.segmentInfo->getMemSize();

				DEBUG_MSG("Uploading%s", ":");
				DEBUG_MSG("inputMem.size = %u; inputData.size = %u", inputMem.size(), inputData.size());
				DEBUG_MSG("Set inputMem[%u] = %p; inputData[%u] = %p;", j, &(inputMem[j]), i, inData);
				DEBUG_MSG("begin = %u; size = %lu; address = %p", curr.begin_cpy, memSize, inData);

				w++;
				j++;

				switch ((*inputInfo)[i]->getDataType()) {
				case IWorkData::BUFFER:
					errcode = clEnqueueWriteBuffer(executionQueue, inMem, CL_FALSE, 0, memSize, inData, 0, NULL, NULL);
					break;
				case IWorkData::IMAGE2D:
					region[0] = (*inputInfo)[i]->getWidth();
					region[1] = (*inputInfo)[i]->getHeight();
					region[2] = (*inputInfo)[i]->getDepth();
					errcode = clEnqueueWriteImage(executionQueue, inMem, CL_FALSE, origin, region, 0, 0, inData, 0, NULL, NULL);
					break;
				default:
					break;
				}
				if(errcode != CL_SUCCESS) {
					throw ExecutionException(OpenCLErrorParser::errorString(errcode));
				}
			}
		}
	}
}

void marrow::Loop::downloadOutputData(const cl_command_queue &executionQueue, const unsigned int deviceIndex, const unsigned int uniqueId, const unsigned int partitionIndex, const unsigned int overlapPartition, std::vector<std::shared_ptr<marrow::Vector>> &outputData, std::vector<cl_mem> &outputMem) {
	bool reads = readsData();
	UNUSED(uniqueId);
	UNUSED(deviceIndex);

	if(!reads) {
		readResults(executionQueue, partitionIndex, overlapPartition, NULL, outputMem, outputData);
	}
}

std::vector<void*> * marrow::Loop::getExecutables() {
	return getNestedExecutables();
}

void marrow::Loop::finishExecution(const unsigned int uniqueId, std::vector<std::shared_ptr<marrow::Vector>> &outputData) {
	UNUSED(outputData);
	currentStatesMutexes.erase(uniqueId);
	currentStates.erase(uniqueId);
	currentStatesSync.erase(uniqueId);
}

void marrow::Loop::readResults(cl_command_queue readQueue, unsigned int partitionIndex, const unsigned int overlapPartition, cl_event execEvent, const std::vector<cl_mem> &outputMem, std::vector<std::shared_ptr<marrow::Vector>> &outputData) {
	int errcode;
	size_t origin[3] = { 0, 0, 0 };
	size_t region[3] = { 0, 0, 0 };
	unsigned int numEvents = execEvent == NULL ? 0 : 1;
	unsigned int i, w = 0;
	unsigned int outputSize = getNumOutputEntries();
	unsigned long offset, memSize;
	cl_event* event = numEvents == 0 ? NULL : &execEvent;
	std::vector<std::shared_ptr<PartitionedInfo>> * partitionedOutput = getPartitionedOutput();
	const std::vector<std::shared_ptr<IWorkData>> * outputInfo = getOutputDataInfo();

	for(i = 0, w = 0; i < outputSize; i++) {
		PartitionedInfo::DataSegment curr = (*partitionedOutput)[i]->getSegment(partitionIndex, overlapPartition);
		void* outData = outputData[w]->get(curr.begin_comp);
		cl_mem outMem = outputMem[w];

		if(curr.segmentInfo->getPartitionMode() == IWorkData::COPY) {
			memSize = curr.size * outputData[w]->getElemSize();
			offset = curr.begin_comp * outputData[w]->getElemSize();
		} else {
			memSize = curr.segmentInfo->getMemSize();
			offset = 0;
		}

		DEBUG_MSG("Downloading%s", ":");
		DEBUG_MSG("outputMem.size = %u; outputData.size = %u",  outputMem.size(), outputData.size());
		DEBUG_MSG("Set outputMem[%u] = %p; outputData[%u] = %p;", w, &(outputMem[w]), w, outData);
		DEBUG_MSG("offset = %u; size = %lu;", offset, memSize);

		w++;

		switch ((*outputInfo)[i]->getDataType()) {
		case IWorkData::BUFFER:
			if(curr.segmentInfo->getPartitionMode() == IWorkData::PARTITIONABLE || curr.segmentInfo->getPartitionMode() == IWorkData::COPY) {
				errcode = clEnqueueReadBuffer(readQueue, outMem, CL_FALSE, offset, memSize, outData, numEvents, event, NULL);
			} else {
				void * tempData = calloc(sizeof(char), memSize);
				errcode = clEnqueueReadBuffer(readQueue, outMem, CL_FALSE, offset, memSize, tempData, numEvents, event, NULL);

				// Put this function already applying the correct function to the datasets.
				// Apply function with outdata <- tempData
				curr.segmentInfo->merge(outData, tempData);
			}
			break;
		case IWorkData::IMAGE2D:
			region[0] = (*outputInfo)[i]->getWidth();
			region[1] = (*outputInfo)[i]->getHeight();
			region[2] = (*outputInfo)[i]->getDepth();
			errcode = clEnqueueReadImage(readQueue, outMem, CL_FALSE, origin, region, 0, 0, outData, numEvents, event, NULL);
			break;
		default:
			break;
		}
		if(errcode != CL_SUCCESS) {
			DEBUG_MSG("Exception: curr.begin_comp %lu * outputData[i]->getElemSize() %lu = %lu :: memSize = %lu", curr.begin_comp, outputData[i]->getElemSize(), curr.begin_comp * outputData[i]->getElemSize(), memSize);
			throw ExecutionException(OpenCLErrorParser::errorString(errcode));
		}
	}
}

cl_event marrow::Loop::nonReadbackExec(cl_command_queue execQueue, unsigned int deviceIndex, unsigned int uniqueId, unsigned int partitionIndex, const unsigned int overlapPartition, std::vector<cl_mem> &inputData, std::vector<void*> &singletonInputValues, std::vector<cl_mem> &outputData, cl_event waitEvent, std::vector<std::shared_ptr<marrow::Vector>> &resultMem) {
	std::vector<cl_mem> *in = &inputData, *out = &outputData;
	cl_event execEvent = NULL;
	std::vector<std::shared_ptr<marrow::Vector>> empty(0);
	unsigned int i = 0;

	marrow::ILoopState* state = createState();

	while(state->condition()) {
		// If profiling mode is not on all events must be released by the responsible entity.
		#ifndef PROFILING
		if(execEvent != NULL) {
			cl_int errcode;
			errcode = clReleaseEvent(execEvent);
			if(errcode != CL_SUCCESS) {
				throw ExecutionException(OpenCLErrorParser::errorString(errcode));
			}
		}
		#endif

		execEvent = (*exec).execute(execQueue, deviceIndex, uniqueId, partitionIndex, overlapPartition, *in, singletonInputValues, *out, waitEvent, resultMem);
		state->step(empty);

		// swap input/output
		if(i % 2 == 0) {
			in = &outputData;
			out = &inputData;
		}
		else {
			in = &inputData;
			out = &outputData;
		}
		i++;
	}

	if(i % 2 == 0) {
		// vectors are swaped
		outputData.swap(inputData);
	}
	return execEvent;
}

cl_event marrow::Loop::readbackSyncdExec(cl_command_queue execQueue, unsigned int deviceIndex, unsigned int uniqueId, unsigned int partitionIndex, const unsigned int overlapPartition, std::vector<cl_mem> &inputData, std::vector<void*> &singletons, std::vector<cl_mem> &outputData, std::vector<std::shared_ptr<marrow::Vector>> &resultMem) {
	std::vector<cl_mem> *in = &inputData, *out = &outputData;
	unsigned int i = 0;
	cl_event execEvent = NULL;

	{
		boost::lock_guard<boost::mutex> lock(currentStatesMutex);

		if(currentStates.count(uniqueId) == 0) {
			currentStatesMutexes.insert(std::pair<unsigned int, boost::mutex*>(uniqueId, new boost::mutex()));
			currentStates.insert(std::pair<unsigned int, std::pair<marrow::ILoopState*, boost::barrier*>>(uniqueId, std::pair<marrow::ILoopState*, boost::barrier*>(createState(), new boost::barrier(numConcurrentWorkers))));
			DEBUG_MSG("numConcurrentWorkers = %u", numConcurrentWorkers);
			currentStatesSync.insert(std::pair<unsigned int, bool*>(uniqueId, new bool(false)));
		}
	}

	marrow::ILoopState* state = currentStates[uniqueId].first;

	DEBUG_MSG("Set condition = %i; deviceIndex = %u; overlapPartition = %u", state->condition(), deviceIndex, overlapPartition);
	while(state->condition()) {
		// If profiling mode is not on all events must be released by the responsible entity.
		#ifndef PROFILING
		if(execEvent != NULL) {
			cl_int errcode;
			errcode = clReleaseEvent(execEvent);
			if(errcode != CL_SUCCESS) {
				throw ExecutionException(OpenCLErrorParser::errorString(errcode));
			}
		}
		#endif

		execEvent = (*exec).execute(execQueue, deviceIndex, uniqueId, partitionIndex, overlapPartition, *in, singletons, *out, NULL, resultMem);
		readResults(execQueue, partitionIndex, overlapPartition, NULL, *out, resultMem);
		clFinish(execQueue);

		// Wait until all workers have finished
		(*currentStates[uniqueId].second).wait();

		// Only one thread should calculate the global step.
		{
			boost::lock_guard<boost::mutex> lock(*currentStatesMutexes[uniqueId]);
			if(!(*currentStatesSync[uniqueId])) {
				(*currentStatesSync[uniqueId]) = true;
				DEBUG_MSG("Entering step%s", "...");
				state->step(resultMem);
			}
		}

		// Checks which were modified and updates them.
		uploadInputData(execQueue, deviceIndex, uniqueId, partitionIndex, overlapPartition, resultMem, *out);
		clFinish(execQueue);

		for(unsigned j = 0; j < resultMem.size(); j++) {
			resultMem[j]->setUpdated();
		}

		if(i % 2 == 0) {
			in = &outputData;
			out = &inputData;
		}
		else {
			in = &inputData;
			out = &outputData;
		}
		i++;

		(*currentStatesSync[uniqueId]) = false;
		// Wait until all workers have finished
		(*currentStates[uniqueId].second).wait();
	}
	// if results are in the wrong buffers vectors are swaped
	if(i % 2 == 0) {
		outputData.swap(inputData);
	}

	return execEvent;
}

cl_event marrow::Loop::readbackExec(cl_command_queue execQueue, unsigned int deviceIndex, unsigned int uniqueId, unsigned int partitionIndex, const unsigned int overlapPartition, std::vector<cl_mem> &inputData, std::vector<void*> &singletons, std::vector<cl_mem> &outputData, std::vector<std::shared_ptr<marrow::Vector>> &resultMem) {
	std::vector<cl_mem> *in = &inputData, *out = &outputData;
	unsigned int i = 0, j = 0;
	cl_event execEvent = NULL;

	marrow::ILoopState* state = createState();

	std::vector<std::shared_ptr<PartitionedInfo>> * partitionedOutput = (*exec).getPartitionedOutput();
	DEBUG_MSG("resultMem.size = %u outputMem.size = %u; inputMem.size = %u", resultMem.size(), outputData.size(), inputData.size());

	// Create marrow::Vectors pointing to partitions (Output avoids singletons)
	std::vector<std::shared_ptr<marrow::Vector>> partitionedOutputVector(resultMem.size());
	for(i = 0; i < resultMem.size(); i++) {
		PartitionedInfo::DataSegment x = (*partitionedOutput)[i]->getSegment(partitionIndex, overlapPartition);
		partitionedOutputVector[i] = std::shared_ptr<marrow::Vector>(new marrow::Vector(resultMem[i]->get(x.begin_comp), resultMem[i]->getElemSize(), x.size));
	}

	i = 0;

	while(state->condition()) {
		// If profiling mode is not on all events must be released by the responsible entity.
		#ifndef PROFILING
		if(execEvent != NULL) {
			cl_int errcode;
			errcode = clReleaseEvent(execEvent);
			if(errcode != CL_SUCCESS) {
				throw ExecutionException(OpenCLErrorParser::errorString(errcode));
			}
		}
		#endif

		// create the vectors.
		execEvent = (*exec).execute(execQueue, deviceIndex, uniqueId, partitionIndex, overlapPartition, *in, singletons, *out, NULL, resultMem);
		readResults(execQueue, partitionIndex, overlapPartition, NULL, *out, resultMem);
		clFinish(execQueue);

		state->step(partitionedOutputVector);

		// Checks which were modified and updates them.
		for(j = 0; j < partitionedOutputVector.size(); j++) {
			if(partitionedOutputVector[j]->requiresUpdate()) {
				uploadInputData(execQueue, deviceIndex, uniqueId, partitionIndex, overlapPartition, resultMem, *out);
				clFinish(execQueue);
				break;
			}
		}

		if(i % 2 == 0) {
			in = &outputData;
			out = &inputData;
		}
		else {
			in = &inputData;
			out = &outputData;
		}

		i++;
	}
	// if results are in the wrong buffers vectors are swaped
	if(i % 2 == 0) {
		outputData.swap(inputData);
	}

	partitionedOutputVector.clear();

	return execEvent;
}

bool marrow::Loop::isInitialized() {
	return isInit;
}

void marrow::Loop::initExecutable() {
	(*exec).initExecutable();
	isInit = true;
}

void marrow::Loop::clearConfiguration() {
	//this->hasPartitions = false;
	(*exec).clearConfiguration();
}

void marrow::Loop::reconfigureExecutable() {
	//setPartitions(true);
	//this->hasPartitions = true;

	(*exec).reconfigureExecutable();
}


/**
 * Pre-condition: outputData and auxMem must be equivalent
 */
cl_event marrow::Loop::execute(cl_command_queue executionQueue, unsigned int deviceIndex, unsigned int uniqueId, unsigned int partitionIndex, unsigned int overlapPartition, std::vector<cl_mem> &inputData, std::vector<void*> &singletonInputValues, std::vector<cl_mem> &outputData, cl_event waitEvent, std::vector<std::shared_ptr<marrow::Vector>> &resultMem) {
	cl_event event = NULL;
	UNUSED(uniqueId);

	DEBUG_MSG("stepSync = %i; partialRead = %i", stepSync, partialRead);
	if(stepSync) {
		event = readbackSyncdExec(executionQueue, deviceIndex, uniqueId, partitionIndex, overlapPartition, inputData, singletonInputValues, outputData, resultMem);
	} else if(partialRead) {
		// Another marrow::Skeleton needs the data
		event = readbackExec(executionQueue, deviceIndex, uniqueId, partitionIndex, overlapPartition, inputData, singletonInputValues, outputData, resultMem);
	} else {
		event = nonReadbackExec(executionQueue, deviceIndex, uniqueId, partitionIndex, overlapPartition, inputData, singletonInputValues, outputData, waitEvent, resultMem);
	}
	return event;
}

const std::vector<std::shared_ptr<IWorkData>> * marrow::Loop::getInputDataInfo() {
	return (*exec).getInputDataInfo();
}

unsigned int marrow::Loop::getNumInputEntries() {
	return (*exec).getNumInputEntries();
}

const std::vector<std::shared_ptr<IWorkData>> * marrow::Loop::getOutputDataInfo() {
	return (*exec).getOutputDataInfo();
}

unsigned int marrow::Loop::getNumOutputEntries() {
	return (*exec).getNumOutputEntries();
}

unsigned long marrow::Loop::requiredStaticMem() {
	return (*exec).requiredStaticMem();
}

unsigned long marrow::Loop::requiredDynamicMem() {
	return (*exec).requiredDynamicMem();
}

unsigned long marrow::Loop::requiredGlobalMem() {
	return (*exec).requiredGlobalMem();
}

unsigned long marrow::Loop::requiredLocalMem() {
	return (*exec).requiredLocalMem();
}

bool marrow::Loop::readsData() {
	if(partialRead) {
		return true;
	}
	else {
		return childReads;
	}
}

std::vector<std::shared_ptr<PartitionedInfo>> * marrow::Loop::getPartitionedInput() {
	return (*exec).getPartitionedInput();
}

std::vector<cl_mem> * marrow::Loop::getInputMem(unsigned int partitionIndex, unsigned int overlapPartition) {
	return (*exec).getInputMem(partitionIndex, overlapPartition);
}

std::vector<std::shared_ptr<PartitionedInfo>> * marrow::Loop::getPartitionedOutput() {
	return (*exec).getPartitionedOutput();
}

std::vector<cl_mem> * marrow::Loop::getOutputMem(unsigned int partitionIndex, unsigned int overlapPartition) {
	return (*exec).getOutputMem(partitionIndex, overlapPartition);
}

std::vector<void*> * marrow::Loop::getNestedExecutables() {
	return (*exec).getNestedExecutables();
}

bool marrow::Loop::hasSetPartitions() {
	return (*exec).hasSetPartitions();
}

bool marrow::Loop::isDoublePrecision() {
	return (*exec).isDoublePrecision();
}

void marrow::Loop::requiresInputMemory(bool shouldAlloc) {
	(*exec).requiresInputMemory(shouldAlloc);
}

void marrow::Loop::requiresOutputMemory(bool shouldAlloc) {
	(*exec).requiresOutputMemory(shouldAlloc);
}

#ifdef PROFILING
std::string marrow::Loop::getProfilingInfo() {
	return getProfilingInfo(1);
}

std::string marrow::Loop::getProfilingInfo(unsigned int indent) {
	std::stringstream s;

	for(unsigned int j = 0; j < indent; j++) {
		s << "  ";
	}

	s << "marrow::Loop:" << std::endl;

	s << (*exec).getProfilingInfo(indent+1);

	return s.str();
}
#endif
