/*
 * marrow::MapReduce.cpp
 *
 *  Created on: 12 de Jun de 2012
 *      Author: Ricardo Marques
 *			Modified by Fernando Alexandre
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "marrow/MapReduce.hpp"
#include "marrow/Map.hpp"
#include "marrow/Pipeline.hpp"
#include "marrow/utils/Debug.hpp"

#include "runtime/ExecutionPlatform.hpp"
#include "runtime/OpenCLErrorParser.hpp"


marrow::MapReduce::MapReduce(std::unique_ptr<marrow::IExecutable> &map):
marrow::Skeleton(*(*map).getInputDataInfo(), *(*map).getOutputDataInfo()),
hasGPUReduce(false)
{
	init();
	s = new marrow::Map(map);
}

marrow::MapReduce::MapReduce(std::unique_ptr<marrow::IExecutable> &map, unsigned int numOverlapPartitions):
marrow::Skeleton(*(*map).getInputDataInfo(), *(*map).getOutputDataInfo(), numOverlapPartitions),
hasGPUReduce(false)
{
	init();
	s = new marrow::Map(map, numOverlapPartitions);
}

marrow::MapReduce::MapReduce(std::unique_ptr<marrow::IExecutable> &map, unsigned int numOverlapPartitions, unsigned int numDevices):
marrow::Skeleton(*(*map).getInputDataInfo(), *(*map).getOutputDataInfo(), numOverlapPartitions, numDevices),
hasGPUReduce(false)
{
	init();
	s = new marrow::Map(map, numOverlapPartitions, numDevices);
}


marrow::MapReduce::MapReduce(std::unique_ptr<marrow::IExecutable> &map, std::unique_ptr<marrow::IExecutable> &reduce):
marrow::Skeleton(*(*map).getInputDataInfo(), *(*reduce).getOutputDataInfo()),
hasGPUReduce(true)
{
	init();
	std::unique_ptr<marrow::IExecutable> pipeline (new marrow::Pipeline(map, reduce));
	s = new marrow::Map(pipeline);
}

marrow::MapReduce::MapReduce(std::unique_ptr<marrow::IExecutable> &map, std::unique_ptr<marrow::IExecutable> &reduce, unsigned int numOverlapPartitions):
marrow::Skeleton(*(*map).getInputDataInfo(), *(*reduce).getOutputDataInfo(), numOverlapPartitions),
hasGPUReduce(true)
{
	init();
	std::unique_ptr<marrow::IExecutable> pipeline (new marrow::Pipeline(map, reduce));
	s = new marrow::Map(pipeline, numOverlapPartitions);
}

marrow::MapReduce::MapReduce(std::unique_ptr<marrow::IExecutable> &map, std::unique_ptr<marrow::IExecutable> &reduce, unsigned int numOverlapPartitions, unsigned int numDevices):
marrow::Skeleton(*(*map).getInputDataInfo(), *(*reduce).getOutputDataInfo(), numOverlapPartitions, numDevices),
hasGPUReduce(true)
{
	init();
	std::unique_ptr<marrow::IExecutable> pipeline (new marrow::Pipeline(map, reduce));
	s = new marrow::Map(pipeline, numOverlapPartitions, numDevices);
}

marrow::MapReduce::~MapReduce(){
	closed = true;
	delete s;

	for(unsigned int i = 0; i < memorySet.size(); i++) {
		unsigned char * x = static_cast<unsigned char *>(memorySet[i]->get(0));
		delete [] x;
		memorySet[i].reset();
	}

}

void marrow::MapReduce::init(){
	unsigned int i, width = 0, height = 0, depth = 0;
	unsigned int inputEntries = inputInfo.size();
	unsigned int outputEntries = outputInfo.size();
	for(i = 0; i < inputEntries; i++){
		if(inputInfo[i]->getDataType() == IWorkData::SINGLETON){
			continue;
		}
		if(width == 0){
			width = inputInfo[i]->getWidth();
		}
		else if(width != inputInfo[i]->getWidth()){
			throw DataMismatchException(MAPDATAMISMATCH);
		}
		if(height == 0){
			height = inputInfo[i]->getHeight();
		}
		else if(height != inputInfo[i]->getHeight()){
			throw DataMismatchException(MAPDATAMISMATCH);
		}
		if(depth == 0){
			depth = inputInfo[i]->getDepth();
		}
		else if(depth != inputInfo[i]->getDepth()){
			throw DataMismatchException(MAPDATAMISMATCH);
		}
	}
	for(i = 0; i < outputEntries; i++){
		if(width != outputInfo[i]->getWidth()){
			throw DataMismatchException(MAPDATAMISMATCH);
		}
		if(height != outputInfo[i]->getHeight()){
			throw DataMismatchException(MAPDATAMISMATCH);
		}
		if(depth != outputInfo[i]->getDepth()){
			throw DataMismatchException(MAPDATAMISMATCH);
		}
	}

	memorySet.resize(outputInfo.size());
	unsigned int nElems, elemSize;

	for(i = 0; i < outputInfo.size(); i++) {
		nElems = outputInfo[i]->getWidth() * outputInfo[i]->getHeight() * outputInfo[i]->getDepth();
		elemSize = outputInfo[i]->getMemSize() / nElems;
		memorySet[i] = std::shared_ptr<marrow::Vector> (new marrow::Vector(new char[elemSize*nElems], elemSize, nElems));
	}

	isInit = true;
}

void marrow::MapReduce::executeSkel(const cl_command_queue &executionQueue, const unsigned int deviceIndex, const unsigned int uniqueId, const unsigned int partitionIndex, const unsigned int overlapPartition, std::vector<std::shared_ptr<marrow::Vector>> &inputData, std::vector<std::shared_ptr<marrow::Vector>> &outputData, std::vector<cl_mem> &inputMem, std::vector<cl_mem> &outputMem) {
	if(hasGPUReduce) {
		(*s).executeSkel(executionQueue, deviceIndex, uniqueId, partitionIndex, overlapPartition, inputData, outputData, inputMem, outputMem);
	} else {
		(*s).executeSkel(executionQueue, deviceIndex, uniqueId, partitionIndex, overlapPartition, inputData, memorySet, inputMem, outputMem);
	}
}

void marrow::MapReduce::uploadInputData(const cl_command_queue &executionQueue, const unsigned int deviceIndex, const unsigned int uniqueId, const unsigned int partitionIndex, const unsigned int overlapPartition, std::vector<std::shared_ptr<marrow::Vector>> &inputData, std::vector<cl_mem> &inputMem) {
	(*s).uploadInputData(executionQueue, deviceIndex, uniqueId, partitionIndex, overlapPartition, inputData, inputMem);
}

void marrow::MapReduce::downloadOutputData(const cl_command_queue &executionQueue, const unsigned int deviceIndex, const unsigned int uniqueId, const unsigned int partitionIndex, const unsigned int overlapPartition, std::vector<std::shared_ptr<marrow::Vector>> &outputData, std::vector<cl_mem> &outputMem) {
	if(hasGPUReduce) {
		(*s).downloadOutputData(executionQueue, deviceIndex, uniqueId, partitionIndex, overlapPartition, outputData, outputMem);
	} else {
		(*s).downloadOutputData(executionQueue, deviceIndex, uniqueId, partitionIndex, overlapPartition, memorySet, outputMem);
	}
}

unsigned long marrow::MapReduce::requiredGlobalMem() {
	return (*s).requiredGlobalMem();
}

std::vector<void*> * marrow::MapReduce::getExecutables() {
	return (*s).getExecutables();
}

void marrow::MapReduce::finishExecution(const unsigned int uniqueId, std::vector<std::shared_ptr<marrow::Vector>> &outputData) {
	UNUSED(uniqueId);

	if(!hasGPUReduce) {
		reduce(memorySet, outputData);
	}
}


std::vector<std::shared_ptr<PartitionedInfo>> * marrow::MapReduce::getPartitionedInput() {
	return (*s).getPartitionedInput();
}

std::vector<cl_mem> * marrow::MapReduce::getInputMem(unsigned int partitionIndex, unsigned int overlapPartitionIndex) {
	return (*s).getInputMem(partitionIndex, overlapPartitionIndex);
}


std::vector<std::shared_ptr<PartitionedInfo>> * marrow::MapReduce::getPartitionedOutput() {
	return (*s).getPartitionedOutput();
}

std::vector<cl_mem> * marrow::MapReduce::getOutputMem(unsigned int partitionIndex, unsigned int overlapPartitionIndex) {
	return (*s).getOutputMem(partitionIndex, overlapPartitionIndex);
}

#ifdef PROFILING
std::string marrow::MapReduce::getProfilingInfo() {
	std::stringstream stream;

	stream << "marrow::MapReduce:" << std::endl << (*s).getProfilingInfo();

	return stream.str();
}
#endif

