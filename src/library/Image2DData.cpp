/*
 * marrow::Image2DData.cpp
 *
 *  Created on: 4 de Abr de 2012
 *      Author: Ricardo Marques
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "marrow/Image2DData.hpp"
#include "marrow/utils/Debug.hpp"

marrow::Image2DData::Image2DData(unsigned int width, unsigned int height, imageChannelData channelData, imageChannelOrder order, IWorkData::partitionMode partitioningMode):
width(width),
height(height),
accessMode(IWorkData::DEFAULTACCESSMODE),
type(IWorkData::IMAGE2D),
order(order),
channelData(channelData),
partitioningMode(partitioningMode)
{
	this->f.clImageFormat.image_channel_order = order;
	this->f.clImageFormat.image_channel_data_type = channelData;
}

marrow::Image2DData::Image2DData(unsigned int width, unsigned int height, imageChannelData channelData, imageChannelOrder order, IWorkData::dataMode accessMode, IWorkData::partitionMode partitioningMode):
width(width),
height(height),
accessMode(accessMode),
type(IWorkData::IMAGE2D),
order(order),
channelData(channelData),
partitioningMode(partitioningMode)
{
	this->f.clImageFormat.image_channel_order = order;
	this->f.clImageFormat.image_channel_data_type = channelData;
}

marrow::Image2DData::~Image2DData(){}


unsigned int marrow::Image2DData::channelByteSize(imageChannelData channelData){
	switch (channelData) {
		case IWorkData::UNORM_INT8:
			return 1;
		default:
			return 0;
	}
}

unsigned int marrow::Image2DData::numberChannels(imageChannelOrder order){
	switch (order) {
	case IWorkData::RGBA:
		return 4;
	default:
		return 0;
	}
}

unsigned long marrow::Image2DData::getMemSize(){
	return numberChannels(order)*channelByteSize(channelData)*width*height;
}

IWorkData::dataMode marrow::Image2DData::getAccessMode(){
	return accessMode;
}

IWorkData::dataType marrow::Image2DData::getDataType(){
	return type;
}

IWorkData::format marrow::Image2DData::getFormat(){
	return f;
}

IWorkData::partitionMode marrow::Image2DData::getPartitionMode() {
	return partitioningMode;
}

unsigned int marrow::Image2DData::getIndivisibleSize() {
	return 1; // TODO: This should be changed for Image2D multi-GPU support
}

bool marrow::Image2DData::isDoublePrecision() {
	return false;
}


bool marrow::Image2DData::isEqual(IWorkData &other){
	return (type == other.getDataType() &&
			accessMode == other.getAccessMode() &&
			f.clImageFormat.image_channel_data_type == other.getFormat().clImageFormat.image_channel_data_type &&
			f.clImageFormat.image_channel_order == other.getFormat().clImageFormat.image_channel_order &&
			width == other.getWidth() && height == other.getHeight());
}

unsigned int marrow::Image2DData::getWidth(){
	return width;
}

unsigned int marrow::Image2DData::getHeight(){
	return height;
}

unsigned int marrow::Image2DData::getDepth(){
	return 1;
}

void* marrow::Image2DData::value(){
	return NULL;
}

void marrow::Image2DData::merge(void* values1, void* values2) {
	DEBUG_MSG("Shouldn't have been called%s", ".");
	UNUSED(values1);
	UNUSED(values2);
}
