/*
 * marrow::Skeleton.cpp
 *
 *  February 2012:
 *       Ricardo Marques
 *       Creation of the skeleton concept
 *
 *  September 2013:
 *       Fernando Alexandre
 *       Adaptation for multi-GPU execution
 *
 *  December 2013:
 *       Herve Paulino
 *       Marrow namespace definition
 *       Decoupling of the original ISkeleton into interface and implementation
 *
 *  Created on: 15 de Dec de 2013
 *      Author: Ricardo Marques
 *			Modified by Fernando Alexandre and Herve Paulino
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "marrow/Skeleton.hpp"
#include "marrow/runtime/IPlatformContext.hpp"
#include "marrow/runtime/IScheduler.hpp"
#include "marrow/runtime/IWorkData.hpp"
#include "marrow/utils/Debug.hpp"
#include "marrow/exceptions/WrongDeviceConfigurationException.hpp"

#include "runtime/Scheduler.hpp"
#include "runtime/ExecutionPlatform.hpp"

#include "Future.hpp"


bool marrow::Skeleton::canRestartScheduler = true;
std::unique_ptr<IPlatformContext> marrow::Skeleton::executionPlatform(new ExecutionPlatform());
std::unique_ptr<marrow::runtime::IScheduler> marrow::Skeleton::scheduler (new marrow::runtime::Scheduler());
boost::mutex marrow::Skeleton::uniqueCounterMutex;

unsigned int marrow::Skeleton::uniqueCounter = 0;

marrow::Skeleton::Skeleton(const std::vector<std::shared_ptr<IWorkData>> &inputInfo, const std::vector<std::shared_ptr<IWorkData>> &outputInfo):
inputInfo(inputInfo),
outputInfo(outputInfo),
isInit(false),
closed(false),
numSingleton(0),
localMemSize(0),
hasPartitions(false)
{
	numConcurrentWorkers = scheduler->getNumOverlapPartitions() * scheduler->getNumDevices();
}

marrow::Skeleton::Skeleton(const std::vector<std::shared_ptr<IWorkData>> &inputInfo, const std::vector<std::shared_ptr<IWorkData>> &outputInfo, unsigned int numDevices) :
inputInfo(inputInfo),
outputInfo(outputInfo),
isInit(false),
closed(false),
numSingleton(0),
localMemSize(0),
hasPartitions(false)
{
	if (numDevices > executionPlatform->getNumDevices())
			throw marrow::WrongDeviceConfigurationException("Number of devices demanded not supported by the underlying infrastructure.");

	if (numDevices == 0)
		numDevices = executionPlatform->getNumDevices();

	if(marrow::Skeleton::canRestartScheduler && scheduler->getNumDevices() != numDevices) {
		DEBUG_MSG("********** Starting Reconfigure %s", "**********");
		marrow::Skeleton::scheduler.reset();
		marrow::Skeleton::executionPlatform.reset();
		marrow::Skeleton::executionPlatform = std::unique_ptr<IPlatformContext>(new ExecutionPlatform());
		marrow::Skeleton::scheduler = std::unique_ptr<marrow::runtime::IScheduler>(new marrow::runtime::Scheduler(numDevices));
	} else {
		DEBUG_MSG("Ignored numOverlapPartitions as marrow::Skeleton::canRestartScheduler = %d",
				marrow::Skeleton::canRestartScheduler);
	}

	numConcurrentWorkers = scheduler->getNumOverlapPartitions() * scheduler->getNumDevices();
}

marrow::Skeleton::Skeleton(const std::vector<std::shared_ptr<IWorkData>> &inputInfo, const std::vector<std::shared_ptr<IWorkData>> &outputInfo, unsigned int numDevices, unsigned int numOverlapPartitions) :
inputInfo(inputInfo),
outputInfo(outputInfo),
isInit(false),
closed(false),
numSingleton(0),
localMemSize(0),
hasPartitions(false)
{
	if (numDevices > executionPlatform->getNumDevices())
			throw marrow::WrongDeviceConfigurationException("Number of devices demanded not supported by the underlying infrastructure.");

	if (numDevices == 0)
		numDevices = executionPlatform->getNumDevices();

	if(marrow::Skeleton::canRestartScheduler && (scheduler->getNumOverlapPartitions() != numOverlapPartitions || scheduler->getNumDevices() != numDevices)) {
		DEBUG_MSG("********** Starting Reconfigure %s", "**********");
		marrow::Skeleton::scheduler.reset();
		marrow::Skeleton::executionPlatform.reset();
		marrow::Skeleton::executionPlatform = std::unique_ptr<IPlatformContext>(new ExecutionPlatform());
		marrow::Skeleton::scheduler = std::unique_ptr<marrow::runtime::IScheduler>(new marrow::runtime::Scheduler(numDevices, numOverlapPartitions));
	} else {
		DEBUG_MSG("Ignored numDevices and numOverlapPartitions as marrow::Skeleton::canRestartScheduler = %d",
				marrow::Skeleton::canRestartScheduler);
	}

	numConcurrentWorkers = scheduler->getNumOverlapPartitions() * scheduler->getNumDevices();
}

marrow::IFuture* marrow::Skeleton::write(const std::vector<std::shared_ptr<marrow::Vector>> &inputData, const std::vector<std::shared_ptr<marrow::Vector>> &outputData) {
	if(outputData.size() == 0) {
		ERROR_MSG("outputData.size() == %lu", (unsigned long int) outputData.size());
		return NULL;
	}

	marrow::Skeleton::canRestartScheduler = false;

	marrow::Future* future = new marrow::Future();
	Task* task;

	{
		boost::lock_guard<boost::mutex> lock(marrow::Skeleton::uniqueCounterMutex);
		task = new Task(this,
						marrow::Skeleton::uniqueCounter,
						(std::vector<std::shared_ptr<marrow::Vector>>*) &inputData,
						(std::vector<std::shared_ptr<marrow::Vector>>*) &outputData,
						future);
		marrow::Skeleton::uniqueCounter++;
	}

	std::shared_ptr<Task> t (task);
	marrow::Skeleton::scheduler->submit(t);

	return future;
}

marrow::Skeleton::~Skeleton() {
	unsigned int i;
	closed = true;

	for(i = 0; i < inputInfo.size(); i++) {
		inputInfo[i].reset();
	}

	for(i = 0; i < outputInfo.size(); i++) {
		outputInfo[i].reset();
	}
}

void marrow::Skeleton::setPartitions(bool isReconfigure) {
	marrow::Skeleton::scheduler->setPartitions(this, isReconfigure);
	hasPartitions = true;
}
