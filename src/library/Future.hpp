/*
 * marrow::Future.hpp
 *  February 2012:
 *       Ricardo Marques
 *       Creation of the Future concept
 *
 *   September 2013:
 *       Fernando Alexandre
 *       Adaptation to support the new Vector concept
 *
 *  Created on: 23 de Fev de 2012
 *      Author: Ricardo Marques
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef __MARROW_FUTURE_HPP__
#define __MARROW_FUTURE_HPP__

#include <boost/thread.hpp>

#include "marrow/IFuture.hpp"

namespace marrow {

    /**
     * Custom deleter for the data associated to a future object instance.
     */
    class FutureDeleter {
    public:
      void operator()(void ** p) {
          delete [] p;
      }
    };
    
    /**
     * Class that implements the marrow::IFuture interface.
     */
    class Future: public marrow::IFuture {
    public:
        /**
         * Default constructor
         */
        Future();

        /**
         * Class destructor
         */
        ~Future();

        /** marrow::Imarrow::Future Interface Functions **/

        virtual bool isReady();

        virtual void setData(std::vector<std::shared_ptr<marrow::Vector>> *data);

        virtual std::vector<std::shared_ptr<marrow::Vector>>* data();

        virtual void wait();

    protected:
        // Data resulting from skeleton execution
        std::vector<std::shared_ptr<marrow::Vector>> *dataVector;
        // Set true if execution has completed
        bool isDataReady;
        // Mutex for access control to this marrow::Future object
        boost::mutex mut;
        // Condition variable for when the skeleton execution has not yet terminated
        boost::condition_variable cond;
    };
}
#endif /* __MARROW_FUTURE_HPP__ */
