/*
 * marrow::Map.cpp
 *
 *  Created on: 29 de Fev de 2012
 *      Author: Ricardo Marques
 *			Modified by Fernando Alexandre
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "marrow/Map.hpp"
#include "marrow/Vector.hpp"
#include "marrow/utils/Debug.hpp"
#include "marrow/exceptions/ExecutionException.hpp"

#include "runtime/OpenCLErrorParser.hpp"

#include <boost/thread.hpp>


marrow::Map::Map(std::unique_ptr<marrow::IExecutable> &exec):
marrow::Skeleton(*(*exec).getInputDataInfo(), *(*exec).getOutputDataInfo()),
exec(std::move(exec)),
childReads(false)
{
	initMap(false);
}

marrow::Map::Map(std::unique_ptr<marrow::IExecutable> &exec, unsigned int numDevices) :
marrow::Skeleton(*(*exec).getInputDataInfo(), *(*exec).getOutputDataInfo(), numDevices),
exec(std::move(exec)),
childReads(false)
{
	initMap(true);
}

marrow::Map::Map(std::unique_ptr<marrow::IExecutable> &exec, unsigned int numDevices, unsigned int numOverlapPartitions) :
marrow::Skeleton(*(*exec).getInputDataInfo(), *(*exec).getOutputDataInfo(), numDevices, numOverlapPartitions),
exec(std::move(exec)),
childReads(false)
{
	initMap(true);
}

void marrow::Map::initMap(bool isReconfigure) {
	unsigned int i;
	for(i = 0; i < inputInfo.size(); i++) {
		if(inputInfo[i]->getDataType() == IWorkData::SINGLETON) {
			numSingleton++;
		}
	}

	if(isReconfigure) {
		(*exec).clearConfiguration();
	}

	setPartitions(isReconfigure);

	localMemSize = (*this->exec).requiredLocalMem();

	if(!(*exec).isInitialized()) {
		(*exec).initExecutable();
	}

	if(isReconfigure) {
		(*exec).reconfigureExecutable();
	}

	isInit = true;
}

marrow::Map::~Map() {
	this->exec.reset();
}

void marrow::Map::executeSkel(const cl_command_queue &executionQueue, const unsigned int deviceIndex, const unsigned int uniqueId, const unsigned int partitionIndex, const unsigned int overlapPartition, std::vector<std::shared_ptr<marrow::Vector>> &inputData, std::vector<std::shared_ptr<marrow::Vector>> &outputData, std::vector<cl_mem> &inputMem, std::vector<cl_mem> &outputMem) {
	unsigned int i, j;
	std::vector<void*> singletonValues(numSingleton);
	std::vector<std::shared_ptr<IWorkData>> * inputInfo = (std::vector<std::shared_ptr<IWorkData>>*) (*exec).getInputDataInfo();

	DEBUG_MSG("Starting execution @ partitionIndex = %u", partitionIndex);


	for(i = 0, j = 0; i < (*inputInfo).size(); i++){
		if((*inputInfo)[i]->getDataType() == IWorkData::SINGLETON) {
			singletonValues[j] = inputData[i]->get(0);
			j++;
		}
	}

	cl_event exec = NULL;
	try {
		if(childReads) {
			exec = (*this->exec).execute(executionQueue, deviceIndex, uniqueId, partitionIndex, overlapPartition, inputMem, singletonValues, outputMem, NULL, outputData);
		}
		else {
			std::vector<std::shared_ptr<marrow::Vector>> empty(0);
			exec = (*this->exec).execute(executionQueue, deviceIndex, uniqueId, partitionIndex, overlapPartition, inputMem, singletonValues, outputMem, NULL, empty);
		}
	}
	catch (ExecutionException& e) {
		#ifndef PROFILING
		clReleaseEvent(exec);
		#endif
		throw;
	}

	#ifndef PROFILING
	clReleaseEvent(exec);
	#endif
	DEBUG_MSG("Finished execution @ partitionIndex = %u", partitionIndex);
}

void marrow::Map::uploadInputData(const cl_command_queue &executionQueue, const unsigned int deviceIndex, const unsigned int uniqueId, const unsigned int partitionIndex, const unsigned int overlapPartition, std::vector<std::shared_ptr<marrow::Vector>> &inputData, std::vector<cl_mem> &inputMem) {
	unsigned int i = 0, j = 0, w = 0;
	int errcode;
	UNUSED(uniqueId);
	UNUSED(deviceIndex);

	size_t origin[3] = { 0, 0, 0 };
	size_t region[3] = { 0, 0, 0 };
	unsigned int inputSize = (*exec).getNumInputEntries();
	unsigned int outputSize = (*exec).getNumOutputEntries();
	const std::vector<std::shared_ptr<IWorkData>> * inputInfo = (*exec).getInputDataInfo();
	std::vector<std::shared_ptr<PartitionedInfo>> * partitionedInput = (*exec).getPartitionedInput();

	// write the input data to device memory
	if(inputData.size() != outputSize) {
		w += numSingleton;
	}

	for(i = 0; i < inputSize; i++) {
		// Its either buffer or image2d
		if((*partitionedInput)[i]) {
			if(inputData[w]->requiresUpdate()) {
				PartitionedInfo::DataSegment curr = (*partitionedInput)[i]->getSegment(partitionIndex, overlapPartition);
				void* inData = inputData[w]->get(curr.begin_cpy);
				cl_mem inMem = inputMem[j];

				unsigned long memSize = curr.segmentInfo->getMemSize();

				DEBUG_MSG("Uploading%s", ":");
				DEBUG_MSG("inputMem.size = %u; inputData.size = %u", inputMem.size(), inputData.size());
				DEBUG_MSG("Set inputMem[%u] = %p; inputData[%u] = %p;", j, &(inputMem[j]), i, inData);
				DEBUG_MSG("begin = %u; size = %lu; address = %p", curr.begin_cpy, memSize, inData);

				w++;
				j++;

				switch ((*inputInfo)[i]->getDataType()) {
				case IWorkData::BUFFER:
					errcode = clEnqueueWriteBuffer(executionQueue, inMem, CL_FALSE, 0, memSize, inData, 0, NULL, NULL);
					break;
				case IWorkData::IMAGE2D:
					region[0] = (*inputInfo)[i]->getWidth();
					region[1] = (*inputInfo)[i]->getHeight();
					region[2] = (*inputInfo)[i]->getDepth();
					errcode = clEnqueueWriteImage(executionQueue, inMem, CL_FALSE, origin, region, 0, 0, inData, 0, NULL, NULL);
					break;
				default:
					break;
				}
				if(errcode != CL_SUCCESS) {
					throw ExecutionException(OpenCLErrorParser::errorString(errcode));
				}
			}
		}
	}

	DEBUG_MSG("Finished upload @ partitionIndex = %u", partitionIndex);
}

void marrow::Map::downloadOutputData(const cl_command_queue &executionQueue, const unsigned int deviceIndex, const unsigned int uniqueId, const unsigned int partitionIndex, const unsigned int overlapPartition, std::vector<std::shared_ptr<marrow::Vector>> &outputData, std::vector<cl_mem> &outputMem) {
	unsigned int i;
	int errcode;
	UNUSED(uniqueId);
	UNUSED(deviceIndex);

	size_t origin[3] = { 0, 0, 0 };
	size_t region[3] = { 0, 0, 0 };
	unsigned int outputSize = outputInfo.size();
	std::vector<std::shared_ptr<PartitionedInfo>> * partitionedOutput = getPartitionedOutput();

	DEBUG_MSG("Starting download @ partitionIndex = %u", partitionIndex);

	if(!childReads) {
		for(i = 0; i < outputSize; i++) {
			PartitionedInfo::DataSegment curr = (*partitionedOutput)[i]->getSegment(partitionIndex, overlapPartition);
			void* outData = outputData[i]->get(curr.begin_comp);
			cl_mem outMem = outputMem[i];
			unsigned long memSize = curr.segmentInfo->getMemSize();

			DEBUG_MSG("Downloading begin = %u; size = %lu; address = %p", curr.begin_comp, memSize, outData);

			switch (outputInfo[i]->getDataType()) {
				case IWorkData::BUFFER:
					errcode = clEnqueueReadBuffer(executionQueue, outMem, CL_FALSE, 0, memSize, outData, 0, NULL, NULL);
					break;
				case IWorkData::IMAGE2D:
					region[0] = outputInfo[i]->getWidth();
					region[1] = outputInfo[i]->getHeight();
					region[2] = outputInfo[i]->getDepth();
					errcode = clEnqueueReadImage(executionQueue, outMem, CL_FALSE, origin, region, 0, 0, outData, 0, NULL, NULL);
					break;
				default:
					break;
			}

			if(errcode != CL_SUCCESS) {
				throw ExecutionException(OpenCLErrorParser::errorString(errcode));
			}
		}
	}
	DEBUG_MSG("Finished download @ partitionIndex = %u", partitionIndex);
}

unsigned long marrow::Map::requiredGlobalMem() {
	return (*exec).requiredGlobalMem();
}

std::vector<void*> * marrow::Map::getExecutables() {
	return (*exec).getNestedExecutables();
}

void marrow::Map::finishExecution(const unsigned int uniqueId, std::vector<std::shared_ptr<marrow::Vector>> &outputData) {
	UNUSED(uniqueId);
	UNUSED(outputData);
}

std::vector<std::shared_ptr<PartitionedInfo>> * marrow::Map::getPartitionedInput() {
	return (*exec).getPartitionedInput();
}

std::vector<cl_mem> * marrow::Map::getInputMem(unsigned int partitionIndex, unsigned int overlapPartitionIndex) {
	return (*exec).getInputMem(partitionIndex, overlapPartitionIndex);
}

std::vector<std::shared_ptr<PartitionedInfo>> * marrow::Map::getPartitionedOutput() {
	return (*exec).getPartitionedOutput();
}

std::vector<cl_mem> * marrow::Map::getOutputMem(unsigned int partitionIndex, unsigned int overlapPartitionIndex) {
	return (*exec).getOutputMem(partitionIndex, overlapPartitionIndex);
}

#ifdef PROFILING
std::string marrow::Map::getProfilingInfo() {
	std::stringstream s;

	s << "marrow::Map:" << std::endl << (*exec).getProfilingInfo(1) << std::endl;

	return s.str();
}
#endif

