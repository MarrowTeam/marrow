# The arguments are passed directly to the cmake command

mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Debug $* ..
make -j4
cd ..
rm -rf build