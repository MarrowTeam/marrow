/*
 * Saxpy.cpp
 *
 *	June 2012:
 *		Ricardo Marques
 *		Marrow implementation of the Saxpy Benchmark
 *
 *	June 2013:
 *		Fernando Alexandre
 *		Adaptation for multi-GPU environments
 *
 *	March 2014:
 *		Herve Paulino
 *		Adapted to the marrow::utils::Benchmark class
 *
 *
 *  Created on: June 25th, 2012
 *      Author: Ricardo Marques
 *          Modified by Fernando Alexandre, Herve Paulino
 *
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "marrow/IFuture.hpp"
#include "marrow/KernelWrapper.hpp"
#include "marrow/Map.hpp"
#include "marrow/BufferData.hpp"
#include "marrow/FinalData.hpp"
#include "marrow/utils/Benchmark.hpp"

#define INPUT_VALUE 10.0f
#define A 10.0f

#define KERNEL_FILE "saxpy.cl"
#define KERNEL_FUNCTION "saxpy"

using namespace marrow;
using namespace std;

class Saxpy: public marrow::utils::Benchmark {
private:
	unsigned int numberElements;

	float* inVector1;
	float* inVector2;
	float* outVector;

public:
	Saxpy(int nrElements, int argc, char* argv[]) :
			marrow::utils::Benchmark("Saxpy", argc-1, &argv[1]),
			numberElements(nrElements),
			inVector1(new float[numberElements]),
			inVector2(new float[numberElements]),
			outVector(new float[numberElements]) {

		for (unsigned int i = 0; i < numberElements; i++) {
			inVector1[i] = INPUT_VALUE;
			inVector2[i] = INPUT_VALUE;
		}
	}

	~Saxpy() {
		delete inVector1;
		delete inVector2;
		delete outVector;
	}

	static std::string usage(std::string program) {
		return program + " number_elements " + Benchmark::usage();
	}

protected:
	void init() {
		std::vector<unsigned int> globalWorkSize(1);
		globalWorkSize[0] = numberElements;

		std::vector<std::shared_ptr<IWorkData>> inDataInfo(3);
		inDataInfo[0] = std::shared_ptr<IWorkData>(new BufferData<float>(numberElements));
		inDataInfo[1] = std::shared_ptr<IWorkData>(new BufferData<float>(numberElements));
		inDataInfo[2] = std::shared_ptr<IWorkData>(new FinalData<float>(A));

		std::vector<std::shared_ptr<IWorkData>> outDataInfo(1);
		outDataInfo[0] = std::shared_ptr<IWorkData>(new BufferData<float>(numberElements));

		// init wrappers
		std::unique_ptr<IExecutable> kernel(
				new KernelWrapper(resolveKernel(KERNEL_FILE), KERNEL_FUNCTION, inDataInfo, outDataInfo, globalWorkSize));

		skeletonTree = new Map(kernel, numDevices, numBuffers);
	}

	void run() {
		// input buffer
		std::vector<std::shared_ptr<Vector>> inputData(2);
		inputData[0] = std::shared_ptr<Vector>(
				new Vector(inVector1, sizeof(float), numberElements));
		inputData[1] = std::shared_ptr<Vector>(
				new Vector(inVector2, sizeof(float), numberElements));

		// output buffer
		std::vector<std::shared_ptr<Vector>> outputData(1);
		outputData[0] = std::shared_ptr<Vector>(
				new Vector(outVector, sizeof(float), numberElements));

		// execution request
		IFuture* future = skeletonTree->write(inputData, outputData);
		future->wait();

		// cleanup
		delete future;

		unsigned int i;
		for (i = 0; i < inputData.size(); i++)
			inputData[i].reset();

		for (i = 0; i < outputData.size(); i++)
			outputData[i].reset();
	}

	bool validate() {
		const float referenceValue = INPUT_VALUE*A + INPUT_VALUE;

		for (unsigned int i = 0; i < numberElements; i++)
			if (outVector[i] !=  referenceValue)
				return false;

		return true;
	}
};

int main(int argc, char* argv[]) {
	if (argc < 2 || argc % 2 != 0) {
		std::cout << Saxpy::usage(std::string (argv[0])) << std::endl;
		return 1;
	}

	Saxpy s(atoi(argv[1]), argc, argv);
	s.timedRuns();
	return 0;
}
