/*
 * FFT.cpp
 *
 *
 *	June 2013:
 *		Fernando Alexandre
 *		Implementation of the Marrow orchestration for the FFT OpenCL kernel featured in the SHOC benchmark suite
 *
 *	March 2014:
 *		Herve Paulino
 *		Adaption to the marrow::utils::Benchmark class
 *
 *  Created on: June 2013
 *      Author: Fernando Alexandre
 *          Modified by Herve Paulino
 *
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "marrow/IFuture.hpp"
#include "marrow/KernelWrapper.hpp"
#include "marrow/Pipeline.hpp"
#include "marrow/BufferData.hpp"
#include "marrow/FinalData.hpp"
#include "marrow/utils/Benchmark.hpp"

#define KERNEL_FILE "FFT.cl"
#define FFT_KERNEL "fft1D_512"
#define IFFT_KERNEL "ifft1D_512"

using namespace marrow;
using namespace std;

class FFT: public marrow::utils::Benchmark {
private:
	unsigned int memSize;
	unsigned int numberElements;
	unsigned int numberFFTs;
	cl_float2 * in, *out;
	unsigned long used_bytes;

public:
	FFT(unsigned int memSize, int argc, char* argv[]) :
			marrow::utils::Benchmark("FFT", argc - 1, &argv[1]),
			memSize(memSize) {

		unsigned long bytes = (memSize * 1024 * 1024);
		unsigned long half_n_ffts = bytes / (512 * sizeof(cl_float2) * 2);
		numberFFTs = half_n_ffts * 2;

		unsigned long half_n_cmplx = half_n_ffts * 512;
		used_bytes = half_n_cmplx * 2 * sizeof(cl_float2);
		numberElements = used_bytes / sizeof(cl_float2);

		in = new cl_float2[numberElements];
		out = new cl_float2[numberElements];

		unsigned int half_numberElements = numberElements / 2;
		for (unsigned int i = 0; i < half_numberElements; i++) {
			in[i].x = (rand() / (float) RAND_MAX) * 2 - 1;
			in[i].y = (rand() / (float) RAND_MAX) * 2 - 1;
			in[i + half_numberElements].x = in[i].x;
			in[i + half_numberElements].y = in[i].y;
		}
	}

	~FFT() {
		delete in;
		delete out;
	}

	static std::string usage(std::string program) {
		return program + " <memSize> " + Benchmark::usage();
	}

protected:
	void init() {
		// Specify global work size
		std::vector<unsigned int> globalWorkSize(1);
		globalWorkSize[0] = numberFFTs;

		std::shared_ptr<IWorkData> bufferInfo(
				new BufferData<cl_float2>(numberElements, (numberElements / numberFFTs)));

		// Type and size of the input arguments
		std::vector<std::shared_ptr<IWorkData>> inInfo(1);
		inInfo[0] = bufferInfo;

		// Type and size of the output arguments
		std::vector<std::shared_ptr<IWorkData>> outInfo(1);
		outInfo[0] = bufferInfo;

		// Create kernel wrapper
		const std::string kernelFile = resolveKernel(KERNEL_FILE);
		std::unique_ptr<IExecutable> fft(
				new KernelWrapper(kernelFile, FFT_KERNEL, inInfo, outInfo, globalWorkSize));
		std::unique_ptr<IExecutable> ifft(
				new KernelWrapper(kernelFile, IFFT_KERNEL, inInfo, outInfo, globalWorkSize));
		skeletonTree = new Pipeline(fft, ifft, numDevices, numBuffers);
	}

	void run() {
		// input buffer
		std::vector<std::shared_ptr<Vector>> inputData(1);
		inputData[0] = std::shared_ptr<Vector>(new Vector(in, sizeof(cl_float2), used_bytes));

		// output buffer
		std::vector<std::shared_ptr<Vector>> outputData(1);
		outputData[0] = std::shared_ptr<Vector>(new Vector(out, sizeof(cl_float2), used_bytes));

		IFuture* future = skeletonTree->write(inputData, outputData);
		future->wait();

		// cleanup
		delete future;
		inputData[0].reset();
		outputData[0].reset();
	}

	bool validate() {
		return true;
	}

};

int main(int argc, char* argv[]) {
	if (argc < 2 || (argc - 2) % 2 != 0) {
		std::cout << FFT::usage(std::string(argv[0])) << std::endl;
		return 1;
	}

	FFT s(atoi(argv[1]), argc, argv);
	s.timedRuns();
	return 0;
}
