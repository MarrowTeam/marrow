#define BLACK 0
#define WHITE 255
#define GREY  128

__kernel void segmentation(int grey_value, int l, int h, __global const unsigned char *in, __global unsigned char *out) {

	unsigned int pixel_x = get_global_id(0);
	unsigned int pixel_y = get_global_id(1);
	unsigned int pixel_z = get_global_id(2);
	unsigned int pos;

	pos = pixel_z*h*l + pixel_y*h + pixel_x;

	out[pos] = in[pos] < grey_value ? BLACK : WHITE;
}

__kernel void bi_segmentation(int greymin, int greymax, int l, int h, __global const unsigned char *in, __global unsigned char *out) {

	unsigned int pixel_x = get_global_id(0);
	unsigned int pixel_y = get_global_id(1);
	unsigned int pixel_z = get_global_id(2);
	unsigned int pos;

	pos = pixel_z*h*l + pixel_y*h + pixel_x;

	if (in[pos] > greymin)
		out[pos] = in[pos] < greymax ? GREY : WHITE;
	else out[pos] = BLACK;
}

