/*
 * Segmentation.cpp
 *
 *	June 2012:
 *		Ricardo Marques
 *		Marrow implementation of the Segmentation Benchmark
 *
 *	June 2013:
 *		Fernando Alexandre
 *		Adaptation for multi-GPU environments
 *
 *	March 2014:
 *		Herve Paulino
 *		Adapted to the marrow::utils::Benchmark class
 *
 *
 *  Created on: June 2012
 *      Author: Ricardo Marques
 *          Modified by Fernando Alexandre, Herve Paulino
 *
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "marrow/IFuture.hpp"
#include "marrow/KernelWrapper.hpp"
#include "marrow/Map.hpp"
#include "marrow/BufferData.hpp"
#include "marrow/FinalData.hpp"
#include "marrow/utils/Benchmark.hpp"

#define KERNEL_FILE "segmentation_reader.cl"
#define SEGMENTATION_KERNEL "segmentation"
#define BISEGMENTATION_KERNEL "bi_segmentation"

#define SIMPLE_SEG 0
#define DOUBLE_SEG 1

using namespace marrow;
using namespace std;

class Segmentation: public marrow::utils::Benchmark {
private:
	int l, h, p;
	unsigned char *i_matrix = NULL;
	unsigned char *o_matrix = NULL;
	int type, greymin, greymax;

public:
	Segmentation(std::string inputFile, int type, int greymin, int greymax, int argc, char* argv[]) :
			marrow::utils::Benchmark("Segmentation", argc-5, &argv[5]),
			type(type),
			greymin(greymin),
			greymax(greymax) {

		FILE* in = fopen(inputFile.c_str(), "rb");
		if (!in)
			throw std::runtime_error("Failed to read matrix from file " + inputFile);

		fscanf(in, "%d %d %d\n", &l, &h, &p);
		int size = l * h * p;

		i_matrix = (unsigned char*) malloc(size * sizeof(unsigned char));
		int bytesRead = fread(i_matrix, sizeof(unsigned char), size, in);
		fclose(in);

		if (bytesRead != size) {
			std::stringstream sout;
			sout << "Error reading file " << inputFile << ". (bytes read "
					<< bytesRead << " != size " << size + ")";
			throw std::runtime_error(sout.str());
		}
		o_matrix = (unsigned char*) malloc(size * sizeof(unsigned char));
	}

	~Segmentation() {
		if (i_matrix)
			delete i_matrix;
		if (o_matrix)
			delete o_matrix;
	}

	static std::string usage(std::string program) {
		return program + " <inputMatrixFile> <outputMatrixFile> <seg_type: 0 (single), 1 (bi)> <graymin> <graymax> " + Benchmark::usage();
	}

	/**
	 * Output result matrix to file
	 */
	void outputToFile(std::string outputFile) {
		FILE* out = fopen(outputFile.c_str(), "wb");
		if (!out)
		throw std::runtime_error("Failed to write matrix to file " + outputFile);

		fprintf(out, "%3d %3d %3d\n", l, h, p);
		fwrite(o_matrix, 1, l * h * p, out);
		fclose(out);
	}

protected:
	void init() {
		std::string funcName;
		std::vector<unsigned int> globalWorkSize(3);
		globalWorkSize[0] = l;
		globalWorkSize[1] = h;
		globalWorkSize[2] = p;

		std::vector<std::shared_ptr<IWorkData>> inputInfo;

		if (type == SIMPLE_SEG) { // Segmentation
			funcName = SEGMENTATION_KERNEL;
			inputInfo.resize(4);
			inputInfo[0] = std::shared_ptr<IWorkData>(new FinalData<int>(greymin));
			inputInfo[1] = std::shared_ptr<IWorkData>(new FinalData<int>(l));
			inputInfo[2] = std::shared_ptr<IWorkData>(new FinalData<int>(h));
			inputInfo[3] = std::shared_ptr<IWorkData>(new BufferData<unsigned char>(l*h*p, l*h));
		}
		else { // Bi-Segmentation
			funcName = BISEGMENTATION_KERNEL;
			inputInfo.resize(5);
			inputInfo[0] = std::shared_ptr<IWorkData>(new FinalData<int>(greymin));
			inputInfo[1] = std::shared_ptr<IWorkData>(new FinalData<int>(greymax));
			inputInfo[2] = std::shared_ptr<IWorkData>(new FinalData<int>(l));
			inputInfo[3] = std::shared_ptr<IWorkData>(new FinalData<int>(h));
			inputInfo[4] = std::shared_ptr<IWorkData>(new BufferData<unsigned char>(l*h*p, l*h));
		}

		std::vector<std::shared_ptr<IWorkData>> outputInfo(1);
		outputInfo[0] = std::shared_ptr<IWorkData>(new BufferData<unsigned char>(l*h*p, l*h));

		std::unique_ptr<IExecutable> kernel = std::unique_ptr<IExecutable> (
				new KernelWrapper(resolveKernel(KERNEL_FILE), funcName, inputInfo, outputInfo, globalWorkSize));
		skeletonTree = new Map(kernel, numDevices, numBuffers);
	}

	void run() {
		// input buffer
		std::vector<std::shared_ptr<Vector>> inputData(1);
		inputData[0] =  std::shared_ptr<Vector> (new Vector(i_matrix, sizeof(unsigned char), l*h*p));

		// output buffer
		std::vector<std::shared_ptr<Vector>> outputData(1);
		outputData[0] = std::shared_ptr<Vector> (new Vector(o_matrix, sizeof(unsigned char), l*h*p));

		IFuture* future = skeletonTree->write(inputData, outputData);
		future->wait();

		delete future;
	}

	bool validate() {
		return true;
	}

};

int main(int argc, char* argv[]) {
	if (argc < 6 || (argc - 6) % 2 != 0) {
		std::cout << Segmentation::usage(std::string (argv[0])) << std::endl;
		return EXIT_FAILURE;
	}

	try {
		Segmentation s(argv[1], atoi(argv[3]), atoi(argv[4]), atoi(argv[5]), argc, argv);
		s.timedRuns();

		/* save matrix to file */
		s.outputToFile(argv[2]);
	}
	catch (std::runtime_error& e) {
		std::cerr << "Error : " << e.what() << std::endl << std::endl;
		return EXIT_FAILURE;
	}
	return 0;
}
