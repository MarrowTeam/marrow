# Set include dir for library
#include_directories (${PROJECT_SOURCE_DIR}/include)
include_directories (${TESTS_COMMON_INCLUDE_DIRS})

if(${APPLE})
else(${APPLE})
add_library(imp_opencl SHARED IMPORTED)
set_target_properties(imp_opencl PROPERTIES
	IMPORTED_LOCATION ${OPENCL_LIBRARIES})
endif(${APPLE})

add_library(imp_marrow SHARED IMPORTED)
set_target_properties(imp_marrow PROPERTIES
	IMPORTED_LOCATION ${Marrow_LIBRARIES})


add_executable(segmentation_exec Segmentation.cpp)
if(${APPLE})

target_link_libraries(segmentation_exec imp_marrow)
else(${APPLE})
target_link_libraries(segmentation_exec imp_opencl imp_marrow)
endif(${APPLE})

