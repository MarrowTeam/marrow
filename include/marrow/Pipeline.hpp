/*
 * Pipeline.hpp
 *
 *  March 2012:
 *       Ricardo Marques
 *       Creation of the Pipeline skeleton
 *
 *   September 2013:
 *       Fernando Alexandre
 *       Adaptation for multi-GPU execution
 *
 *  December 2013:
 *      Herve Paulino
 *      marrow namespace
 *
 *  Created on: 23 de Mar de 2012
 *      Author: Ricardo Marques
 * 			Modified by Fernando Alexandre, Herve Paulino
 *
 *  Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef __MARROW_PIPELINE_HPP__
#define __MARROW_PIPELINE_HPP__


#include "IExecutable.hpp"
#include "Skeleton.hpp"
#include "IFuture.hpp"
#include "exceptions/DataMismatchException.hpp"

// Error string for when the two stages of the Pipeline are incompatible
#define PIPEDATAMISMATCH "Data must between stages must be equivalent"

namespace marrow {

    /**
     * Pipeline Skeleton. A Marrow Pipeline is composed of two data-dependable stages. The Pipeline skeleton is nestable, since it implements
     * the marrow::IExecutable interface.
     */
    class Pipeline: public marrow::Skeleton, public marrow::IExecutable {
    public:

        /**
         * This constructor is parametrized with two execution entities, one for each of the stages that compose the Pipeline.
         * The stages must be compatible between each other, that is, the output of stage one must be correctly accepted as input to the
         * second stage. To create Pipelines with more than two stages, the user should nest the required number of Pipelines.
         *
         * @param stage1 - The first stage of the Pipeline.
         * @param stage2 - The second stage of the Pipeline.
         */
        Pipeline(std::unique_ptr<marrow::IExecutable> &stage1, std::unique_ptr<marrow::IExecutable> &stage2);

        /**
         * This constructor is parametrized with two execution entities, one for each of the stages that compose the Pipeline.
         * The stages must be compatible between each other, that is, the output of stage one must be correctly accepted as input to the
         * second stage. To create Pipelines with more than two stages, the user should nest the required number of Pipelines.
         *
         * @param stage1 - The first stage of the Pipeline.
         * @param stage2 - The second stage of the Pipeline.
         * @param numDevices - number of devices to be used.
         */
        Pipeline(std::unique_ptr<marrow::IExecutable> &stage1, std::unique_ptr<marrow::IExecutable> &stage2, unsigned int numDevices);

        /**
         * This constructor is parametrized with two execution entities, one for each of the stages that compose the Pipeline.
         * The stages must be compatible between each other, that is, the output of stage one must be correctly accepted as input to the
         * second stage. To create Pipelines with more than two stages, the user should nest the required number of Pipelines.
         *
         * @param stage1 - The first stage of the Pipeline.
         * @param stage2 - The second stage of the Pipeline.
         * @param numDevices - number of devices to be used.
         * @param numOverlapPartitions - number of overlapping partitions to be used.
         */
        Pipeline(std::unique_ptr<marrow::IExecutable> &stage1, std::unique_ptr<marrow::IExecutable> &stage2, unsigned int numDevices, unsigned int numOverlapPartitions);

        /**
         * Class destructor.
         */
        virtual ~Pipeline();

        #ifdef PROFILING
        virtual std::string getProfilingInfo();
        virtual std::string getProfilingInfo(unsigned int indent);
        #endif

    protected:
        /** Pipeline variables **/
        // Stage one, exection entity
        std::unique_ptr<marrow::IExecutable> stage1;
        // Stage two, exection entity
        std::unique_ptr<marrow::IExecutable> stage2;
        // If any of the stages performs device memory read operations
        bool childReads;

        // Amount of global memory required for this pipeline.
        unsigned long globalMemSize;

        // Amount of static memory required by the middle memory.
        unsigned long middleStaticMem;

        // Amount of dynamic memory required by the middle memory.
        unsigned long middleDynamicMem;

        // Information regarding the intermediate data, that is the output kernel arguments of stage one
        // and the input kernel arguments of stage two
        std::vector<std::shared_ptr<IWorkData>> middleInfo;

        // Data partitions
        std::vector<std::shared_ptr<PartitionedInfo>> partitionedInput;
        std::vector<std::shared_ptr<PartitionedInfo>> partitionedOutput;

        std::vector<std::vector<std::vector<cl_mem>>> middleMem;

        /**
         * Auxiliar method that refracts part of the inicialization process, that is common to all constructors.
         */
        virtual void initPipe(bool isReconfigure);

        /**
         * Method that sincronizes the execution of both stages, of the Pipeline. It is somewhat similar to the marrow::IExecutable::execute method.
         * However, it orchestrates the communication and sincronization between both Pipeline stages, whose intermediate data is placed in @middleMem.
         * This auxiliar function is used, irrespective of the Pipeline beeing nested or not.
         *
         * @param executionQueue - The used command-queue.
         * @param deviceIndex - The index of the device where this is executed.
         * @param uniqueId - The unique id of the task this execution belongs to.
         * @param partitionIndex - The index of the partition to be executed.
         * @param overlapIndex - The index of the overlapping partition to be executed.
         * @param inputMem - The memory objects that hold the input data for the first stage.
         * @param singletonInputValues - Singleton values to be passed onto the first stage.
         * @param middleMem - The memory obects passed used as output for the first stage, and as input to the second stage.
         * @param outputMem - The memory objects where the results of the second stage should be stored.
         * @param waitEvent - Event used to synchronize the execution. The latter may only start after the first is completed.
         * @param resultMem - If the executable performs read operations, they should be performed to these memory references.
         * @return An event associated to this object's execution.
         */
        virtual cl_event execPipe(const cl_command_queue &executionQueue,
                                const unsigned int deviceIndex,
                                const unsigned int uniqueId,
                                const unsigned int partitionIndex,
                                const unsigned int overlapPartition,
                                std::vector<cl_mem> &inputMem,
                                std::vector<void*> singletonInputValues,
                                std::vector<cl_mem> &middleMem,
                                std::vector<cl_mem> &outputMem,
                                cl_event waitEvent,
                                std::vector<std::shared_ptr<marrow::Vector>> &resultMem);

        /** marrow::Skeleton pure virtual functions **/

        virtual void executeSkel(const cl_command_queue &executionQueue,
                                const unsigned int deviceIndex,
                                const unsigned int uniqueId,
                                const unsigned int partitionIndex,
                                const unsigned int overlapPartition,
                                std::vector<std::shared_ptr<marrow::Vector>> &inputData,
                                std::vector<std::shared_ptr<marrow::Vector>> &outputData,
                                std::vector<cl_mem> &inputMem,
                                std::vector<cl_mem> &outputMem);

        virtual void uploadInputData(const cl_command_queue &executionQueue,
                                const unsigned int deviceIndex,
                                const unsigned int uniqueId,
                                const unsigned int partitionIndex,
                                const unsigned int overlapPartition,
                                std::vector<std::shared_ptr<marrow::Vector>> &inputData,
                                std::vector<cl_mem> &inputMem);

        virtual void downloadOutputData(const cl_command_queue &executionQueue,
                                const unsigned int deviceIndex,
                                const unsigned int uniqueId,
                                const unsigned int partitionIndex,
                                const unsigned int overlapPartition,
                                std::vector<std::shared_ptr<marrow::Vector>> &outputData,
                                std::vector<cl_mem> &outputMem);

        virtual std::vector<void*> * getExecutables();

        virtual void finishExecution(const unsigned int uniqueId,
                                    std::vector<std::shared_ptr<marrow::Vector>> &outputData);

        /** marrow::IExecutable pure virtual functions **/

        virtual bool isInitialized();

        virtual void initExecutable();

        virtual void clearConfiguration();

        virtual void reconfigureExecutable();

        virtual cl_event execute(cl_command_queue executionQueue,
                                unsigned int deviceIndex,
                                unsigned int uniqueId,
                                unsigned int partitionIndex,
                                unsigned int overlapPartition,
                                std::vector<cl_mem> &inputData,
                                std::vector<void*> &singletonInputValues,
                                std::vector<cl_mem> &outputData,
                                cl_event waitEvent,
                                std::vector<std::shared_ptr<marrow::Vector>> &resultMem);

        virtual const std::vector<std::shared_ptr<IWorkData>> * getInputDataInfo();

        virtual unsigned int getNumInputEntries();

        virtual const std::vector<std::shared_ptr<IWorkData>> * getOutputDataInfo();

        virtual unsigned int getNumOutputEntries();

        virtual unsigned long requiredStaticMem();

        virtual unsigned long requiredDynamicMem();

        virtual unsigned long requiredGlobalMem();

        virtual unsigned long requiredLocalMem();

        virtual bool readsData();

        virtual std::vector<std::shared_ptr<PartitionedInfo>> * getPartitionedInput();

        virtual std::vector<cl_mem> * getInputMem(unsigned int partitionIndex, unsigned int overlapPartitionIndex);

        virtual std::vector<std::shared_ptr<PartitionedInfo>> * getPartitionedOutput();

        virtual std::vector<cl_mem> * getOutputMem(unsigned int partitionIndex, unsigned int overlapPartitionIndex);

        virtual std::vector<void*> * getNestedExecutables();

        virtual bool hasSetPartitions();

        virtual bool isDoublePrecision();

        virtual void requiresInputMemory(bool shouldAlloc);

        virtual void requiresOutputMemory(bool shouldAlloc);
    };
}

#endif /* PIPELINE_H_ */
