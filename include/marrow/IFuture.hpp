/*
 * IFuture.hpp
 *
 *  February 2013:
 *      Ricardo Marques
 *      Creation of the interface for the future concept
 *
 *  September 2013:
 *       Fernando Alexandre
 *       Adaptation to use Marrow's Vector concept
 *
 *  December 2013:
 *      Herve Paulino
 *      marrow namespace
 *
 *  Created on: 23 de Fev de 2012
 *      Author: Ricardo Marques
 *			Modified by Fernando Alexandre, Herve Paulino
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef __MARROW_IFUTURE_HPP__
#define __MARROW_IFUTURE_HPP__

#include "Vector.hpp"

#include <vector>
#include <memory>

namespace marrow {

    /**
     * Interface for defining a Future class. A Future is an object that is associated with a particular execution, and may
     * be used to query the state of that particular execution.
     */
    class IFuture {
    public:
        /**
         * Class destructor.
         */
        virtual ~IFuture(){};

        /**
         * Method can be used to query the state of the execution associated to this Future object.
         *
         * @return True is the execution has finished, or false otherwise.
         */
        virtual bool isReady() = 0;

        /**
         * Method that sets the Future to a given set of memory references, where the results are stored.
         * Using this method sets the Futures as ready.
         *
         * @param data - The output data of the execution to which the future is associated.
         */
        virtual void setData(std::vector<std::shared_ptr<marrow::Vector>> *data) = 0;

        /**
         * Retrieves the results of the execution, to which this Future is associated to. If the execution is still in progress
         * NULL is returned.
         *
         * @return The output data associated to this future's execution, or NULL if the execution has not yet completed.
         */
        virtual std::vector<std::shared_ptr<marrow::Vector>>* data() = 0;

        /**
         * Method that blocks the caller until the execution associated to this Future instance is completed. The caller is woken up
         * immediately after the results become available. If, when this method is invocate, the execution has already terminated, this
         * method does nothing.
         */
        virtual void wait() = 0;
    };
}
#endif /* __MARROW_IFUTURE_HPP__ */
