/*
 * Timer.hpp
 *
 *	March 2014:
 *		Herve Paulino
 *		Specification and implementation of the Timer class (from pre-existent classes)
 *
 *
 *  Created on: March 1st, 2014
 *      Authors: Herve Paulino, Ricardo Marques, Fernando Alexandre
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef _MARROW_TIMER_HPP_
#define _MARROW_TIMER_HPP_

#ifdef _WIN32
#include <windows.h>
#else
#include <sys/time.h>
#include <unistd.h>
#endif
#include <ostream>

#define NANOSECONDS 0
#define MICROSECONDS 1
#define MILLISECONDS 2
#define SECONDS 3

namespace marrow {
	namespace utils {
		class Timer {

		public:
			Timer(int numberIterations = 1);

			~Timer();

			//! Starts the stopwatch running
			void start();

			//! Run this when the event being timed is complete
			void end();

			//! Stops the timer and returns the result
			double average(int scale = MILLISECONDS);
			double stdDeviation(int scale = MILLISECONDS);

			void printStats(std::ostream& out, int scale = MILLISECONDS);

		private:
		#ifdef _WIN32
			LARGE_INTEGER _wstart;
			LARGE_INTEGER _wend;
		#else
			struct timeval _start;
			struct timeval _end;
		#endif
		};
	}
}

#endif /* _MARROW_TIMER_HPP_ */
