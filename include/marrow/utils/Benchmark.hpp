/*
 * Benchmark.cpp
 *
 *	March 2014:
 *		Herve Paulino
 *		Specification and implementations of the Benchmark class
 *
 *
 *  Created on: March 1st, 2014
 *      Author: Herve Paulino
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
#include "marrow/Skeleton.hpp"
#include "marrow/utils/Timer.hpp"

#include <string>

/**
 * The base class for the Marrow benchmark examples.
 */
namespace marrow {
	namespace utils {
		class Benchmark {
		
		public:

			/**
			 * Constructor
			 *
			 * @param name - Name of the benchmark.
			 * @param argc - Argument count.
			 * @param argv - Argument vector.
			 */
			Benchmark(std::string name, int argc, char* argv[]);

			/**
			 * Class destructor.
			 */
			virtual ~Benchmark() = 0 ;

			/**
			 * Retrieves the benchmark's command line usage
			 *
			 * @return The benchmark's command line usage
			 */
			static std::string usage();
		
			/**
			 * Retrieves the benchmark's name
			 *
			 * @return The name of the benchmark
			 */
			std::string getName();

			/**
			 * Function that is called in-between consecutive runs of the benchmark.
			 * May be used to clean intermediate data.
			 */
			void interRunCleanup();

			/**
			 * Executes the benchmark as many times as determined in the command line (the default is 1),
			 * and outputs the computed execution statistics.
			 */
			void timedRuns(int scale = MILLISECONDS);


		protected:
			/**
			 * Number of times the benchmark must be executed in function timedRuns (default is 1).
			 */
			int numIterations = 1;

			/**
			 * Number of devices to the used in the benchmark's execution (default is "all").
			 */
			int numDevices = 0; // ALL

			/**
			 * Number of overlap buffers to the used in the benchmark's execution (default is 1).
			 */
			int numBuffers = 1;

			/**
			 * The benchmark's skeleton tree
			 */
			marrow::Skeleton* skeletonTree;

			/**
			 * Retrieve the full path of the given kernel file, assumed to be in the same directory as the benchmark.
			 *
			 * @param kernel - Name of the kernel file
			 *
			 * @result - The absolute path to the kernel file
			 */
			std::string resolveKernel(std::string kernel);

			// Pure virtual functions

			/**
			 * The benchmark's initialization function.
			 * Must be used to set up the skeleton tree.
			 */
			virtual void init() = 0;

			/**
			 * The benchmark's behavior function.
			 * Must be used to prompt executions upon the skeleton tree.
			 */
			virtual void run() = 0;

			/**
			 * The benchmark's validation function.
			 * Must be used to validate the output of run (or timedRuns).
			 */
			virtual bool validate() = 0;


		private:
			/**
			 * The name of the benchmark
			 */
			const std::string name;

			/**
			 * Process the benchmark's command line arguments
			 *
			 * @param argc - Argument count.
			 * @param argv - Argument vector.
			 */
			void processArguments(int argc, char* argv[]);

		};
	}
}
