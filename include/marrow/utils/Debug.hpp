/*
 * Debug.hpp
 *
 *  March 2013:
 *		Fernando Alexandre
 *		Creation of the set of auxiliary debugging functions.
 *
 *
 *  Created on: March 31st, 2013
 *      Author: Fernando Alexandre
 *
 *  Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef __MARROW_DEBUG_HPP__
#define __MARROW_DEBUG_HPP__

#include <cstring>


#define UNUSED(expr) do { (void)(expr); } while(0);
// Whether there should be profiling features on the kernels/decomposition (Comment to avoid)
//#define PROFILING
// Whether there should be debug messages. (Comment to avoid)
//#define DEBUG_MESSAGES

#define FILE___ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

// The actual debug message macro
#ifdef DEBUG_MESSAGES
#define DEBUG_MSG(fmt, ...) \
 				do { fprintf(stderr, "%s:%s():%d: " fmt "\n", FILE___, \
 					__func__, __LINE__, __VA_ARGS__); } while(0)

#define ERROR_MSG(fmt, ...) \
 				do { fprintf(stderr, "%s:%s():%d: " fmt "\n", FILE___, \
 					__func__, __LINE__, __VA_ARGS__); } while(0)

#else
#define DEBUG_MSG(fmt, ...) ((void)0)
#define ERROR_MSG(fmt, ...) ((void)0)
#endif

#endif /* __MARROW_DEBUG_HPP__ */
