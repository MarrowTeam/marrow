/*
 * MapReduce.hpp
 *
 *   June 2012:
 *       Ricardo Marques
 *       Creation of the Map-Reduce Skeleton
 *
 *   September 2013:
 *       Fernando Alexandre
 *       Adaptation for multiple GPUs
 *
 *  December 2013:
 *      Herve Paulino
 *      marrow namespace
 *
 *  Created on: 12 de Jun de 2012
 *      Author: Ricardo Marques
 *			Modified by Fernando Alexandre, Herve Paulino
 *
 *  Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef __MARROW_MAPREDUCE_HPP__
#define __MARROW_MAPREDUCE_HPP__

#include <string>

#include "IExecutable.hpp"
#include "Skeleton.hpp"
#include "Vector.hpp"

#define MAPDATAMISMATCH "Data size must be constant throughout every input/output container"

namespace marrow {

    /**
     * This class represents the MapReduce skeleton which can not be nested.
     */
    class MapReduce: public marrow::Skeleton {
    public:

        /**
         * Initizalizes a MapReduce skeleton using a CPU reduction. Uses the
         * maximum number of devices and default number of overlapping partitions
         *
         * @param map - the map computation.
         */
        MapReduce(std::unique_ptr<marrow::IExecutable> &map);

        /**
         * Initizalizes a MapReduce skeleton using a CPU reduction. Uses the
         * maximum number of devices and numOverlapPartitions number of overlapping partitions.
         *
         * @param map - the map computation.
         * @param numOverlapPartitions - number of overlap partitions to be used.
         */
        MapReduce(std::unique_ptr<marrow::IExecutable> &map,
                unsigned int numOverlapPartitions);

        /**
         * Initizalizes a MapReduce skeleton using a CPU reduction. Uses the
         * numDevices number of devices and numOverlapPartitions number of overlapping partitions.
         *
         * @param map - the map computation.
         * @param numOverlapPartitions - number of overlap partitions to be used.
         * @param numDevices - number of devices to be used.
         */
        MapReduce(std::unique_ptr<marrow::IExecutable> &map,
                unsigned int numOverlapPartitions,
                unsigned int numDevices);

        /**
         * Initizalizes a MapReduce skeleton using a GPU/CPU reduction. Uses the
         * maximum number of devices and default number of overlapping partitions.
         *
         * @param map - the map computation.
         * @param reduce - the reduce computation.
         */
        MapReduce(std::unique_ptr<marrow::IExecutable> &map,
                std::unique_ptr<marrow::IExecutable> &reduce);

        /**
         * Initizalizes a MapReduce skeleton using a GPU/CPU reduction. Uses the
         * maximum number of devices and numOverlapPartitions number of overlapping partitions.
         *
         * @param map - the map computation.
         * @param reduce - the reduce computation.
         * @param numOverlapPartitions - number of overlap partitions to be used.
         */
        MapReduce(std::unique_ptr<marrow::IExecutable> &map,
                std::unique_ptr<marrow::IExecutable> &reduce,
                unsigned int numOverlapPartitions);

        /**
         * Initizalizes a MapReduce skeleton using a GPU/CPU reduction. Uses the
         * numDevices number of devices and numOverlapPartitions number of overlapping partitions.
         *
         * @param map - the map computation.
         * @param reduce - the reduce computation.
         * @param numOverlapPartitions - number of overlap partitions to be used.
         * @param numDevices - number of devices to be used.
         */
        MapReduce(std::unique_ptr<marrow::IExecutable> &map,
                std::unique_ptr<marrow::IExecutable> &reduce,
                unsigned int numOverlapPartitions,
                unsigned int numDevices);

        ~MapReduce();

        #ifdef PROFILING
        virtual std::string getProfilingInfo();
        #endif

    protected:
        // The root skeleton which shall be executed.
        marrow::Skeleton *s;
        // Whether there is a GPU reduce or not.
        bool hasGPUReduce;
        // Memory used for the reduction, when only a CPU reduction is used.
        std::vector<std::shared_ptr<marrow::Vector>> memorySet;

        /**
         * Initializes the MapReduce
         */
        virtual void init();

        /** marrow::Skeleton pure virtual functions **/

        virtual void executeSkel(const cl_command_queue &executionQueue,
                                const unsigned int deviceIndex,
                                const unsigned int uniqueId,
                                const unsigned int partitionIndex,
                                const unsigned int overlapPartition,
                                std::vector<std::shared_ptr<marrow::Vector>> &inputData,
                                std::vector<std::shared_ptr<marrow::Vector>> &outputData,
                                std::vector<cl_mem> &inputMem,
                                std::vector<cl_mem> &outputMem);

        virtual void uploadInputData(const cl_command_queue &executionQueue,
                                const unsigned int deviceIndex,
                                const unsigned int uniqueId,
                                const unsigned int partitionIndex,
                                const unsigned int overlapPartition,
                                std::vector<std::shared_ptr<marrow::Vector>> &inputData,
                                std::vector<cl_mem> &inputMem);

        virtual void downloadOutputData(const cl_command_queue &executionQueue,
                                const unsigned int deviceIndex,
                                const unsigned int uniqueId,
                                const unsigned int partitionIndex,
                                const unsigned int overlapPartition,
                                std::vector<std::shared_ptr<marrow::Vector>> &outputData,
                                std::vector<cl_mem> &outputMem);

        virtual unsigned long requiredGlobalMem();

        virtual std::vector<void*> * getExecutables();

        virtual std::vector<std::shared_ptr<PartitionedInfo>> * getPartitionedInput();

        virtual std::vector<cl_mem> * getInputMem(unsigned int partitionIndex, unsigned int overlapPartitionIndex);

        virtual std::vector<std::shared_ptr<PartitionedInfo>> * getPartitionedOutput();

        virtual std::vector<cl_mem> * getOutputMem(unsigned int partitionIndex, unsigned int overlapPartitionIndex);

        virtual void finishExecution(const unsigned int uniqueId, std::vector<std::shared_ptr<marrow::Vector>> &outputData);

        /** Pure virtual functions **/

        virtual void reduce(const std::vector<std::shared_ptr<marrow::Vector>> &results, std::vector<std::shared_ptr<marrow::Vector>> &output) = 0;
    };
}
#endif /* __MARROW_MAPREDUCE_HPP__ */
