/*
 * marrow::Vector.hpp
 *
 *  March 2013:
 *       Fernando Alexandre
 *       Creation of the Vector concept
 *
 *  December 2013:
 *      Herve Paulino
 *      marrow namespace
 *
 *  Created on: 15 de Mar de 2013
 *      Author: Fernando Alexandre
 *          Modified by Herve Paulino
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef __MARROW_VECTOR_HPP__
#define __MARROW_VECTOR_HPP__

#include <cstddef>

namespace marrow {

    /**
     * Class that	 represents a contiguous space of memory
     */
    class Vector {

    public:
        /**
         * Constructor for the vector which creates a single element vector with
         * a pointer to the data and a element size.
         *
         * @param data - The pointer to the data.
         * @param elemSize - The size of each element.
         */
        Vector(void* data, std::size_t elemSize);

        /**
         * Constructor for the vector, setting a data pointer, an element size
         * and a number of elements.
         *
         * @param data - The pointer to the data.
         * @param elemSize - The size of each element.
         * @param nElements - The number of elements.
         */
        Vector(void* data, std::size_t elemSize, unsigned int nElements);

        /**
         * Class deconstructor.
         */
        virtual ~Vector();

        /**
         * Gets the pointer to the first element of the data.
         *
         * @return the pointer to the data.
         */
        virtual void* get();

        /**
         * Gets the pointer to and element of the data.
         *
         * @param elem the index of the desired element.
         * @return the pointer to the address of the element.
         */
        virtual void* get(unsigned int elem);

        /**
         * Gets the number of elements.
         *
         * @return the number of elements.
         */
        virtual unsigned int getNumElements();

        /**
         * Gets the size of an element.
         *
         * @return the element size.
         */
        virtual std::size_t getElemSize();

        /**
         * Sets the data of this vector as a single element.
         *
         * @param data - The pointer to the data.
         * @param elemSize - The size of an element.
         */
        virtual void set(void* data, std::size_t elemSize);

        /**
         * Sets the data of this vector.
         *
         * @param data - The pointer to the data.
         * @param elemSize - The size of an element.
         * @param nElems - The number of elements.
         */
        virtual void set(void* data, std::size_t elemSize, unsigned int nElems);

        /**
         * Sets this vector as requiring update on the GPUs.
         */
        virtual void setModified();

        /**
         * Sets this vector as not requiring update on the GPUs.
         */
        virtual void setUpdated();

        /**
         * Gets whether this vector requires update on the GPUs.
         *
         * @return whether this vector requires updating on the GPUs.
         */
        virtual bool requiresUpdate();

    protected:
        // Number of elements.
        unsigned int nElems;

        // Size of each element.
        std::size_t elemSize;

        // Pointer to the data.
        void* data;

        // If it has been uploaded.
        bool isOnDevice;

        // If data requires updating on the devices.
        bool needsUpdate;
    };
}
#endif /* __MARROW_VECTOR_HPP__ */
