/*
 * KernelWrapper.hpp
 *
 *  February 2012:
 *      Ricardo Marques
 *       Creation of a Kernel's wrapper concept
 *
 *  September 2013:
 *       Fernando Alexandre
 *       Adaptation for multi-GPU execution
 *
 *  December 2013:
 *      Herve Paulino
 *      marrow namespace
 *
 *  Created on: 23 de Fev de 2012
 *      Author: Ricardo Marques
 *			Modified by Fernando Alexandre, Herve Paulino
 *
 *  Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef __MARROW_KERNELWRAPPER_HPP__
#define __MARROW_KERNELWRAPPER_HPP__

#include "IExecutable.hpp"

#include "runtime/IWorkData.hpp"
#include "runtime/KernelBuilder.hpp"

#include <string>
#include <memory>
#include <utility>
#include <vector>
#include <cstddef>
#include <boost/thread.hpp>

namespace marrow {

    namespace runtime {
        class Scheduler;
    }

    /**
     * Class that represents an executable kernel instance. This instance implements the marrow::IExecutable interface and, therefore, may be used in
     * the creation of a composition tree. Since kernels are not skeletons, they may only be used as leafs in the composition tree. As a result
     * a KernelWrapper object cannot be parametrized with other marrow::IExecutable instances.
     */
    class KernelWrapper: public marrow::IExecutable {
    public:
        // Default number of work-load dimensions per kernel
        static const int DEFAULT_GLOBAL_DIMENSIONS = 1;

        /**
         * Base constructor. It is parametrized with the values required to instantiate a KernelWrapper object, whose kernel has a unidimensional
         * work-load of size @numThreads. The work-load group definition is left as default. It obtains the kernel source code from the
         * @kernelFile file path, and compiles the function @kernelFunc. The kernel argument information for both input and output is given by
         * @inputEntries and @outputEntries, respectively.
         *
         * @param kernelFile - Kernel source file path.
         * @param kernelFunc - Kernel function name.
         * @param inputEntries - Input kernel argument definition.
         * @param outputEntries - Output kernel argument definition.
         * @param numThreads - Number of threads in that compose the kernel work-load. In this case, the work load is unidimensional.
         */
        KernelWrapper(const std::string kernelFile, const std::string kernelFunc, const std::vector<std::shared_ptr<IWorkData>> &inputEntries, const std::vector<std::shared_ptr<IWorkData>> &outputEntries, const std::vector<unsigned int> &globalWork);

        /**
         * Second constructor. It is parametrized with the values required to instantiate a KernelWrapper object, whose kernel has may have more than
         * one dimensions, defined by the @threads vector. The work-load group definition is left as default. The constructor obtains the kernel
         * source code from the @kernelFile file path, and compiles the function @kernelFunc. The kernel argument information for both
         * input and output is given by @inputEntries and @outputEntries, respectively.
         *
         * @param kernelFile - Kernel source file path.
         * @param kernelFunc - Kernel function name.
         * @param inputEntries - Input kernel argument definition.
         * @param outputEntries - Output kernel argument definition.
         * @param localWork - Number of threads in that compose the kernel work-load, in each dimension. Each position of the vector is associated to
         * a single dimension. The total number of dimensions for the work-load is defined as the same as the length of the vector, if possible.
         */
        KernelWrapper(const std::string kernelFile, const std::string kernelFunc, const std::vector<std::shared_ptr<IWorkData>> &inputEntries, const std::vector<std::shared_ptr<IWorkData>> &outputEntries, const std::vector<unsigned int> &globalWork, const std::vector<unsigned int> &localWork);

        /**
         * Class destructor
         */
        virtual ~KernelWrapper();

        #ifdef PROFILING
        virtual std::string getProfilingInfo(unsigned int indent);
        #endif

        friend class marrow::runtime::Scheduler;

    protected:
        // List of pairs: argument index - kernel memory object data-type arguments
        std::list<std::pair<int, std::shared_ptr<IWorkData>>> workData;
        // List of pairs: argument index - kernel non memory object data-type arguments
        std::list<std::pair<int, std::shared_ptr<IWorkData>>> fixedWorkData;
        // Input kernel data-type argument information objects
        std::vector<std::shared_ptr<IWorkData>> inWorkData;
        // Output kernel data-type argument information objects
        std::vector<std::shared_ptr<IWorkData>> outWorkData;
        // Total amount of global memory this kernel uses.
        unsigned long globalMemSize;
        // Total size, in bytes, of the required local memory space for the kernel
        unsigned long localMemSize;
        // Number of work-data dimensions used when computing the kernel
        unsigned int numDimensions;
        // Number of work-items per dimension
        std::vector<unsigned int> globalWorkSize;
        // Number of work-items per work-group, and dimension
        std::vector<size_t> localWorkSize;
        // Kernel has localWorkSize defined.
        bool hasLocalWorkSize;
        // Wrapper is inicialized
        bool isInit;
        // Kernel uses OpenCL image memory objects
        bool hasImages;
        // If the partitions are currently set on this KernelWrapper
        bool hasPartitions;
        // The amount of memory that does not change with partitioning (Copy and merge functions partition modes)
        unsigned long staticMem;
        // The amount of memory (in regards to partitionable buffers) a single work-item requires.
        unsigned long dynamicMem;

        // To determine of the input GPU memory should be alloc'd (Pipeline doesn't need some).
        bool shouldAllocInput;
        bool shouldAllocOutput;

        // OpenCL program -- used to spawn cl_kernels
        std::vector<std::vector<cl_program>> programs;
        // List of kernels used for each partition
        std::vector<std::vector<cl_kernel>> kernels;

        // OpenCL KernelBuilder
        KernelBuilder builder;

        // Data partitions
        // Input partitions
        std::vector<std::shared_ptr<PartitionedInfo>> partitionedInput;
        // Output partitions
        std::vector<std::shared_ptr<PartitionedInfo>> partitionedOutput;

        // GPU memory locations in regards to each partition
        // Input memory locations
        std::vector<std::vector<std::vector<cl_mem>>> inputMem;
        // Output memory locations
        std::vector<std::vector<std::vector<cl_mem>>> outputMem;

        // Kernel file path, and function name
        const std::string kernelFile, kernelFunc;

        // vector of partition => vector of overlap partitions => vector of worksize dimensions
        std::shared_ptr<std::vector<std::vector<std::vector<size_t>>>> workSize;

        // Mutex to perform access control on this wrapper
        boost::mutex wrapperMutex;

        // If this kernel uses double precision data-types.
        bool isDPrecision;

        // Profiling-related variables.
        #ifdef PROFILING
        // Structure used to house all the information of a profiling run
        typedef struct {
            cl_event event;
            unsigned int deviceIndex;
        } ProfilerTask;

        // Sum of the run durations of each device
        cl_ulong * runSum;
        // marrow::Vectors with all values to calculate the stdDeviation
        std::vector<std::vector<double>> deviceRunDurations;
        // Number of runs of each device
        cl_ulong * numRuns;
        // List containing all the profiling run results
        std::list<ProfilerTask*> profilerQueue;
        // Mutex to access the profilerQueue
        boost::mutex profilerMutex;
        #endif

        /**
         * Method that factorizes some common processes, done when instantiating a KernelWrapper. All of the constructors eventually call this
         * function to proceed the instantiation process.
         *
         * @param inputEntries - Input kernel argument definition.
         * @param outputEntries - Output kernel argument definition.
         * @param threads - Number of threads in that compose the kernel work-load, in each dimension. Each position of @threads is associated to
         * a single dimension. The total number of dimensions for the work-load is defined as the same as the length of the vector, if possible.
         * @param groupsSize - Number of threads that are grouped to form a work-group, per dimension. Each position of @groupsSize is associated
         * to a single dimension.
         */
        virtual void initWrapper(const std::vector<std::shared_ptr<IWorkData>> &inputEntries, const std::vector<std::shared_ptr<IWorkData>> &outputEntries);//, const std::vector<unsigned int> &threads, const std::vector<unsigned int> &groupsSize);

        /**
         * Auxiliar method that parses the input/output user defined information, and organizes the kernel argument information. For example, this process
         * separates kernel arguments that are represented as memory objects, from those that are simple values not placed in device global memory.
         * After this method returns, the kernel argument information is ready to be queried by other marrow::IExecutable nodes, of this node's composition tree.
         *
         * @param inputEntries - Input kernel argument definition.
         * @param outputEntries - Output kernel argument definition.
         */
        virtual void setInOutInfo(const std::vector<std::shared_ptr<IWorkData>> &inputEntries, const std::vector<std::shared_ptr<IWorkData>> &outputEntries);

        /** marrow::IExecutable Interface Functions **/

        virtual bool isInitialized();

        virtual void initExecutable();

        virtual void clearConfiguration();

        virtual void reconfigureExecutable();

        virtual cl_event execute(cl_command_queue executionQueue, unsigned int deviceIndex, unsigned int uniqueId, unsigned int partitionIndex, unsigned int overlapPartition, std::vector<cl_mem> &inputData, std::vector<void*> &singletonInputValues, std::vector<cl_mem> &outputData, cl_event waitEvent, std::vector<std::shared_ptr<marrow::Vector>> &resultMem);

        virtual const std::vector<std::shared_ptr<IWorkData>> * getInputDataInfo();

        virtual unsigned int getNumInputEntries();

        virtual const std::vector<std::shared_ptr<IWorkData>> * getOutputDataInfo();

        virtual unsigned int getNumOutputEntries();

        virtual unsigned long requiredStaticMem();

        virtual unsigned long requiredDynamicMem();

        virtual unsigned long requiredGlobalMem();

        virtual unsigned long requiredLocalMem();

        virtual bool readsData();

        virtual std::vector<unsigned int> * getGlobalWorkSize();

        virtual std::vector<size_t> * getLocalWorkSize();

        virtual void setPartitionedInput(const std::vector<std::shared_ptr<PartitionedInfo>> &partitionedInput);

        virtual std::vector<std::shared_ptr<PartitionedInfo>> * getPartitionedInput();

        virtual std::vector<cl_mem> * getInputMem(unsigned int partitionIndex, unsigned int overlapPartitionIndex);

        virtual void setPartitionedOutput(const std::vector<std::shared_ptr<PartitionedInfo>> &partitionedOutput);

        virtual std::vector<std::shared_ptr<PartitionedInfo>> * getPartitionedOutput();

        virtual std::vector<cl_mem> * getOutputMem(unsigned int partitionIndex, unsigned int overlapPartitionIndex);

        virtual std::vector<size_t> * getWorkSize(unsigned int partitionIndex, unsigned int overlapPartitionIndex);

        virtual void setWorkSize(std::shared_ptr<std::vector<std::vector<std::vector<size_t>>>> dimensionSize);

        virtual std::vector<void*> * getNestedExecutables();

        virtual bool hasSetPartitions();

        virtual bool isDoublePrecision();

        virtual void requiresInputMemory(bool shouldAlloc);

        virtual void requiresOutputMemory(bool shouldAlloc);

    private:
        #ifdef PROFILING
        /**
         * Adds the duration of this kernelwrapper's execution to the profiling
         * information using the execution's OpenCL event on a certain device.
         *
         * Pre-req: The current event must've finished whenthis function is
         * called.
         *
         * @param event The OpenCL event to be added (which contains the time
         *              stamps)
         * @param deviceIndex The device in which the execution occurred for
         *                    grouping purposes.
         */
        virtual void setProfilingInfo(cl_event event, unsigned int deviceIndex);
        #endif

        /**
         * Frees all the GPU memory used by the input arguments of this kernel.
         */
        virtual void clearInputMem();

        /**
         * Frees all the GPU memory used by the output arguments of this kernel.
         */
        virtual void clearOutputMem();
    };
}

#endif /* __MARROW_KERNELWRAPPER_HPP__ */
