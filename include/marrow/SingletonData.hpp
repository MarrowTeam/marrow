/*
 * SingletonData.hpp
 *
 *  April 2012:
 *       Ricardo Marques
 *       Creation of the singleton datatype
 *
 *  December 2013:
 *      Herve Paulino
 *      marrow namespace
 *
 *  Created on: 2 de Abr de 2012
 *      Author: Ricardo Marques
 *	        Modified by Herve Paulino
 *
 *  Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef __MARROW_SINGLETONDATA_HPP__
#define __MARROW_SINGLETONDATA_HPP__

#include "DataInfo.hpp"

namespace marrow {

    /**
     * Template class that represents a single element data-type. The value of this element, of type T, may vary between distinct executions.
     */
    template <class T>
    class SingletonData: public DataInfo<T> {
    public:
        /**
         * Default constructor.
         */
        SingletonData();

        /**
         * Class destructor.
         */
        virtual ~SingletonData();
    };

    template <class T> SingletonData<T>::SingletonData() : DataInfo<T>(1, IWorkData::DEFAULTACCESSMODE, IWorkData::SINGLETON, IWorkData::COPY, 1) {}

    template <class T> SingletonData<T>::~SingletonData(){}
}

#endif /* __MARROW_SINGLETONDATA_HPP__ */
