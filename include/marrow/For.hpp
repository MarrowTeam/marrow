/*
 * For.hpp
 *
 *  April 2013:
 *      Ricardo Marques
 *      Creation of the For loop definition
 *
 *  December 2013:
 *      Herve Paulino
 *      marrow namespace
 *
 *  Created on: 21 de Abr de 2012
 *      Author: Ricardo Marques
 *	       Modified by Herve Paulino
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef __MARROW_FOR_HPP__
#define __MARROW_FOR_HPP__

#include "Loop.hpp"

namespace marrow {

    /********************************************************************************************/
    /** LoopState - a For Loop State class that encapsulates the behaviour of a usual for loop **/
    /********************************************************************************************/
    class ForState: public ILoopState {
    public:
        /**
         * Constructor for the LoopState object. This for loop holds the following logic:
         *
         * -- for(unsigned int i = @from; i < @to, i++) { execute... } --
         *
         * After the LoopState is reset the counter is set again to the @from original value. And
         * the process repeats itself until the Skeleleton, and therefore the LoopState, is no longer
         * required.
         *
         * @param from - The first value for the counter.
         * @param to - The threshold.
         */
        ForState(unsigned int from, unsigned int to);

        /**
         * Class destructor
         */
        ~ForState();

        /** ILoopState pure virtual functions **/

        bool condition();

        void step(std::vector<std::shared_ptr<marrow::Vector>> &previousOut);

        void reset();

    protected:
        // the counter
        unsigned int from;
        // the original counter value
        const unsigned int startFrom;
        // the threshold
        const unsigned int to;
    };

    /***********************************************************************/
    /***** For - A Skeleton representation of a usual programming loop *****/
    /***********************************************************************/
    class For: public Loop {
    public:
        /**
         * Main constructor. The For is parametrized with an executable entity, @exec. The latter is an
         * instance of the marrow::IExecutable interface. Additionally, the initial value for the counter (@from) and
         * the threshold (@to) are defined.
         *
         * @param from - The original counter value.
         * @param to - The loop threshold
         * @param exec - The executable entity on which the For will prompt the executions.
         */
        For(unsigned int from, unsigned int to, std::unique_ptr<marrow::IExecutable> &exec);

        /**
         * Class destructor.
         */
        ~For();

    protected:
        // original counter value
        const unsigned int startFrom;
        // threshold
        const unsigned int to;

        /** Loop pure virtual function **/

        virtual ForState* createState();
    };
}

#endif /* __MARROW_FOR_HPP__ */
