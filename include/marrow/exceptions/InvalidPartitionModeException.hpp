/*
 * InvalidPartitionModeException.hpp
 *
 *	June 2013:
 *		Fernando Alexandre
 *		Creation of the exception
 *
 *  March 2014:
 *      Herve Paulino
 *      marrow namespace
 *
 *  Created on: June 19th, 2013
 *      Author: Fernando Alexandre
 *       Modified by Herve Paulino
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef _MARROW_INVALIDPARTITIONMODEEXCEPTION_HPP_
#define _MARROW_INVALIDPARTITIONMODEEXCEPTION_HPP_

#include <stdexcept>
#include <string.h>

/**
 * An exception that is thrown when the user attempts to use an invalid partitioning mode in FinalData.
 */
namespace {
	class InvalidPartitionModeException: public std::runtime_error {
	public:
		InvalidPartitionModeException() : std::runtime_error("Invalid Partition Mode") { }
	};
}

#endif /* _MARROW_INVALIDPARTITIONMODEEXCEPTION_HPP_ */
