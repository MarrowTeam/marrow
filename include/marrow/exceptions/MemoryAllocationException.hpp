/*
 * MemoryAllocationException.hpp
 *
 *	March 2012:
 *		Ricardo Marques
 *		Creation of the exception
 *
 *  March 2014:
 *      Herve Paulino
 *      marrow namespace
 *
 *  Created on: March 13th, 2012
 *      Author: Ricardo Marques
 *       Modified by Herve Paulino
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef _MARROW_MEMORYALLOCATIONEXCEPTION_HPP_
#define _MARROW_MEMORYALLOCATIONEXCEPTION_HPP_

#include <stdexcept>
#include <string.h>

/**
 * An exception that is thrown when one or more skeletons are unable to allocate the required memory objects.
 * Even if the platform has enough memory for a particular computation, Marrow does not hold exclusive access over the entire
 * target OpenCL device address space. Therefore, other applications may have allocated data on such memory, which reduces the actual size of the
 * available memory to a fraction of the total available memory.
 * Other issues can raise this exception. marrow::For example, the target OpenCL device may not support memory objects with the size intended by the user, even if
 * theoretically, there is enough memory to hold them.
 */
namespace marrow {
	class MemoryAllocationException: public std::runtime_error {
	public:
		MemoryAllocationException(const std::string &msg) : std::runtime_error(msg) { }
	};
}

#endif /* _MARROW_MEMORYALLOCATIONEXCEPTION_HPP_ */
