/*
 * InvalidGroupSizeException.hpp
 *
 *	March 2012:
 *		Ricardo Marques
 *		Creation of the exception
 *
 *  March 2014:
 *      Herve Paulino
 *      marrow namespace
 *
 *  Created on: March 19th, 2012
 *      Author: Ricardo Marques
 *       Modified by Herve Paulino
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef _MARROW_INVALIDGROUPSIZEEXCEPTION_HPP_
#define _MARROW_INVALIDGROUPSIZEEXCEPTION_HPP_

#include <stdexcept>

/**
 * An exception that is thrown when the target OpenCL device does not support work-groups with the required length, in a particular dimension.
 */
namespace {
	class InvalidGroupSizeException: public std::runtime_error {
	public:
		InvalidGroupSizeException(const std::string &msg) : std::runtime_error("Invalid Work-group size in dimension: " + msg) { }
	};
}

#endif /* _MARROW_INVALIDGROUPSIZEEXCEPTION_HPP_ */
