/*
 * NotSupportedDatatypeException.hpp
 *
 *	April 2012:
 *		Ricardo Marques
 *		Creation of the exception
 *
 *  March 2014:
 *      Herve Paulino
 *      marrow namespace
 *
 *  Created on: April 22nd, 2012
 *      Author: Ricardo Marques
 *       Modified by Herve Paulino
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef _MARROW_NOTSUPPORTEDDATATYPEEXCEPTION_HPP_
#define _MARROW_NOTSUPPORTEDDATATYPEEXCEPTION_HPP_

#include <stdexcept>

/**
 * An exception that is thrown when a particular kernel argument data-type is not supported by the selected skeletons.
 * This may be an issue, for instance, when a skeleton does not support image processing, and the user requires such a fuctionality.
 */
namespace marrow {
	class NotSupportedDatatypeException: public std::runtime_error {
	public:
		NotSupportedDatatypeException() : std::runtime_error("Given data information type is not supported by the one or more skeletons") { }
	};
}

#endif /* _MARROW_NOTSUPPORTEDDATATYPEEXCEPTION_HPP_ */
