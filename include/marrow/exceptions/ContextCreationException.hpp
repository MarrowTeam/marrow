/*
 * ContextCreationException.hpp
 *
 *	March 2012:
 *		Ricardo Marques
 *		Creation of the exception
 *
 * March 2014:
 *      Herve Paulino
 *      marrow namespace
 *
 *  Created on: March 19th, 2012
 *      Author: Ricardo Marques
 *       Modified by Herve Paulino
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef _MARROW_CONTEXTCREATIONEXCEPTION_HPP_
#define _MARROW_CONTEXTCREATIONEXCEPTION_HPP_

#include <stdexcept>
#include <string.h>

/**
 * An exception that is thrown when an error occurs while attempting to create an execution context, namely on the underlying library.
 */
namespace marrow {
	class ContextCreationException: public std::runtime_error {
	public:
		ContextCreationException(const std::string &msg) : std::runtime_error("Error while creating execution context: " + msg) { }
	};
}

#endif /* _MARROW_CONTEXTCREATIONEXCEPTION_HPP_ */
