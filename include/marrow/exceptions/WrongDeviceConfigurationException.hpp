/*
 * WrongDeviceConfigurationException.hpp
 *
 *  March 2014:
 *      Herve Paulino
 *      Creation of the exception
 *
 *  Created on: 8 de Mar de 2014
 *      Author: Herve Paulino
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef _MARROW_WRONGDEVICEEXCEPTION_HPP_
#define _MARROW_WRONGDEVICEEXCEPTION_HPP_

#include <stdexcept>
#include <string>

/**
 * An exception that is thrown when the demanded platform cannot be matched onto the available devices.
 */

namespace marrow {
	class WrongDeviceConfigurationException: public std::runtime_error {
	public:
		WrongDeviceConfigurationException(std::string message) : std::runtime_error(message) { }
	};
}

#endif /* _MARROW_WRONGDEVICEEXCEPTION_HPP_ */
