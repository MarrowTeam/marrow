/*
 * WorkgroupTooLargeException.hpp
 *
 *	March 2012:
 *		Ricardo Marques
 *		Creation of the exception
 *
 *  March 2014:
 *      Herve Paulino
 *      marrow namespace
 *
 *  Created on: March 19th, 2012
 *      Author: Ricardo Marques
 *       Modified by Herve Paulino
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef _MARROW_WORKGROUPTOOLARGEEXCEPTION_HPP_
#define _MARROW_WORKGROUPTOOLARGEEXCEPTION_HPP_

#include <stdexcept>

/**
 * An exception that is thrown when the user-required work-group is larger than the supported by the target OpenCL device.
 * marrow::For example, the number of work-items in the first dimension might be to large, or the product between the number of work-items in each dimension is larger
 * than the maximum work-item value.
 */

namespace marrow {
	class WorkgroupTooLargeException: public std::runtime_error {
	public:
		WorkgroupTooLargeException() : std::runtime_error("Work-group is too large for selected the device") { }
	};
}

#endif /* _MARROW_WORKGROUPTOOLARGEEXCEPTION_HPP_ */
