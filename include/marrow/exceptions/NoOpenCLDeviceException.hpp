/*
 * NoOpenCLDeviceException.hpp
 *
 *	March 2012:
 *		Ricardo Marques
 *		Creation of the exception
 *
 *  March 2014:
 *      Herve Paulino
 *      marrow namespace and subclass of WrongDeviceException
 *
 *  Created on: March 12th, 2012
 *      Author: Ricardo Marques
 *      	Modified by Herve Paulino
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef _MARROW_NOOPENCLDEVICEEXCEPTION_HPP_
#define _MARROW_NOOPENCLDEVICEEXCEPTION_HPP_

#include "WrongDeviceConfigurationException.hpp"

#include <stdexcept>

/**
 * An exception that is thrown when the target platform has no OpenCL supported device.
 * In addition, the platform may not have a particular kind of OpenCL device, such as a GPU, or a multi-core CPU.
 */

namespace marrow {
	class NoOpenCLDeviceException: public WrongDeviceConfigurationException {
	public:
		NoOpenCLDeviceException() : WrongDeviceConfigurationException("No available OpenCL device on this platform") { }
	};
}

#endif /* _MARROW_NOOPENCLDEVICEEXCEPTION_H_ */
