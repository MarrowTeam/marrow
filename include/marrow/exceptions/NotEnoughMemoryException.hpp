/*
 * NotEnoughMemoryException.hpp
 *
 *	March 2012:
 *		Ricardo Marques
 *		Creation of the exception
 *
 *  March 2014:
 *      Herve Paulino
 *      marrow namespace
 *
 *  Created on: March 12th, 2012
 *      Author: Ricardo Marques
 *       Modified by Herve Paulino
 *
 *  Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef _MARROW_NOTENOUGHMEMORYEXCEPTION_HPP_
#define _MARROW_NOTENOUGHMEMORYEXCEPTION_HPP_

#include <stdexcept>

/**
 * An exception that is thrown when the user requests the allocation of more global memory than the available in the target OpenCL device.
 */
namespace marrow {
	class NotEnoughMemoryException: public std::runtime_error {
	public:
		NotEnoughMemoryException() : std::runtime_error("Not enough device memory") { }
	};
}

#endif /* _MARROW_NOTENOUGHMEMORYEXCEPTION_HPP_ */
