/*
 * TooManyDimensionsException.hpp
 *
 *	March 2012:
 *		Ricardo Marques
 *		Creation of the exception
 *
 *  March 2014:
 *      Herve Paulino
 *      marrow namespace
 *
 *  Created on: March 17nd, 2012
 *      Author: Ricardo Marques
 *       Modified by Herve Paulino
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef _MARROW_TOOMANYDIMENSIONSEXCEPTION_HPP_
#define _MARROW_TOOMANYDIMENSIONSEXCEPTION_HPP_

#include <stdexcept>

/**
 * An exception that is thrown when the user specifies the work-size with more dimensions than the number of dimensions supported by the target OpenCL device.
 */
namespace marrow {
	class TooManyDimensionsException: public std::runtime_error {
	public:
		TooManyDimensionsException() : std::runtime_error("Device does not support given number of dimensions") { }
	};
}

#endif /* _MARROW_TOOMANYDIMENSIONSEXCEPTION_HPP_ */
