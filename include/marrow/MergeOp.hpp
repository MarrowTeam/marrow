/*
 * MergeOp.hpp
 *
 *  June 2013:
 *       Fernando Alexandre
 *       Creation of merge operation for partitioned datatypes
 *
 *  December 2013:
 *      Herve Paulino
 *      marrow namespace
 *
 *  Created on: 28 de June de 2012
 *      Author: Fernando Alexandre
 *	        Modified by Herve Paulino
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef __MARROW_MERGEOP_HPP__
#define __MARROW_MERGEOP_HPP__

#include "runtime/IWorkData.hpp"
#include "runtime/Task.hpp"

#include "exceptions/InvalidPartitionModeException.hpp"
#include "exceptions/NotSupportedMergeOperationException.hpp"

#include "utils/Debug.hpp"

#include <functional>
namespace marrow {
    template<class T, bool cond> class SumImpl;
    template<class Y, bool cond> class SubImpl;
    template<class U, bool cond> class MultImpl;
    template<class I, bool cond> class DivImpl;

    /**
     * Template class that offers the capability detection using SFINAE (substitution failure is not an error).
     */
    template<class Z> class MergeOp {
        public:
            /**
             * Constructor which simply receives a partitioning mode (cannot be used with custom
             * functions).
             *
             * @param partitioningMode - The partitioning mode (with operations) to be tested.
             */
            MergeOp(IWorkData::partitionMode partitioningMode);

            /**
             * Constructor to be used when a custom function is used.
             *
             * @param func - The function to be applied
             */
            MergeOp(std::function<void(Z*, Z*, unsigned int)> func);

            /**
             * Class deconstructor.
             */
            ~MergeOp();

            /**
             * The function which merges the data-set (if the operation is supported).
             *
             * @param v1 - first set of values.
             * @param v2 - second set of values.
             * @param numberElems - The number of elements in both sets.
             */
            virtual void merge(Z* v1, Z* v2, unsigned int numberElems);

        protected:
            // The partitining mode (operation).
            IWorkData::partitionMode partitioningMode;

            // Custom function (if it exists).
            std::function<void(Z*, Z*, unsigned int)> func;

            /* Support for Default Operation Detection */
            struct sfinae_base {
                typedef char no[2];
                typedef char yes;
            };

            // Sum Operation
            struct supports_sum : sfinae_base
            {
                template <class U>
                //Called if the operation + can be made (1 byte returned)
                static auto sum_test(const U* u) -> decltype(*u + *u, char(0));

                //Called if it can't (2 byte returned)
                static typename MergeOp<Z>::sfinae_base::no& sum_test(...);

                // Compares the sizes of return type of the previous test function to determine support.
                static const bool value = (sizeof(sum_test((Z*)0)) != sizeof(typename MergeOp<Z>::sfinae_base::no));
            };

            // Subtraction Operation
            struct supports_sub : sfinae_base
            {
                template <class U>
                //Called if the operation - can be made (1 byte returned)
                static auto sub_test(const U* u) -> decltype(*u - *u, char(0));

                //Called if it can't (2 byte returned)
                static typename MergeOp<Z>::sfinae_base::no& sub_test(...);

                // Compares the sizes of return type of the previous test function to determine support.
                static const bool value = (sizeof(sub_test((Z*)0)) != sizeof(typename MergeOp<Z>::sfinae_base::no));
            };

            // Multiplication Operation
            struct supports_mult : sfinae_base
            {
                template <class U>
                //Called if the operation * can be made (1 byte returned)
                static auto mult_test(const U* u) -> decltype(*u * *u, char(0));

                //Called if it can't (2 byte returned)
                static typename MergeOp<Z>::sfinae_base::no& mult_test(...);

                // Compares the sizes of return type of the previous test function to determine support.
                static const bool value = (sizeof(mult_test((Z*)0)) != sizeof(typename MergeOp<Z>::sfinae_base::no));
            };

            // Division operation
            struct supports_div : sfinae_base
            {
                template <class U>
                //Called if the operation / can be made (1 byte returned)
                static auto div_test(const U* u) -> decltype(*u / *u, char(0));

                //Called if it can't (2 byte returned)
                static typename MergeOp<Z>::sfinae_base::no& div_test(...);

                // Compares the sizes of return type of the previous test function to determine support.
                static const bool value = (sizeof(div_test((Z*)0)) != sizeof(typename MergeOp<Z>::sfinae_base::no));
            };
    };

    template<class Z> MergeOp<Z>::MergeOp(IWorkData::partitionMode partitioningMode) :
    partitioningMode(partitioningMode)
    {
        if(partitioningMode == IWorkData::CUSTOM_FTN) {
            DEBUG_MSG("This contructor mustn't be used for this partitioningMode%s", ".");
            throw InvalidPartitionModeException();
        }
    }

    template<class Z> MergeOp<Z>::MergeOp(std::function<void(Z*, Z*, unsigned int)> func):
    partitioningMode(IWorkData::CUSTOM_FTN),
    func(func)
    {}

    template<class Z> MergeOp<Z>::~MergeOp() {}

    template<class Z> void MergeOp<Z>::merge(Z* v1, Z* v2, unsigned int numberElems) {
        switch(partitioningMode) {
            case IWorkData::SUM_MERGE_FTN:
                SumImpl<Z, supports_sum::value>::merge(v1, v2, numberElems);
                break;
            case IWorkData::SUB_MERGE_FTN:
                SubImpl<Z, supports_sub::value>::merge(v1, v2, numberElems);
                break;
            case IWorkData::MULT_MERGE_FTN:
                MultImpl<Z, supports_mult::value>::merge(v1, v2, numberElems);
            break;
            case IWorkData::DIV_MERGE_FTN:
                DivImpl<Z, supports_div::value>::merge(v1, v2, numberElems);
                break;
            case IWorkData::CUSTOM_FTN:
                func(v1, v2, numberElems);
                break;
            default:
                throw InvalidPartitionModeException();
                break;
        }
    }

    /* Sum Operation */
    template<class T> class SumImpl<T, true> {
        public:
        static void merge(T* v1, T* v2, unsigned int numberElems) {
            for(unsigned int i = 0; i < numberElems; i++) {
                v1[i] = v1[i] + v2[i];
            }
        }
    };

    template<class T> class SumImpl<T,false> {
        public:
        static void merge(T* v1, T* v2, unsigned int numberElems) {
            UNUSED(v1);
            UNUSED(v2);
            UNUSED(numberElems);

            DEBUG_MSG("SUM function disabled%s", ".");
            throw NotSupportedMergeOperationException(typeid(T).name());
        }
    };

    /* Subtraction Operation */

    template<class Y> class SubImpl<Y, true> {
        public:
        static void merge(Y* v1, Y* v2, unsigned int numberElems) {
            for(unsigned int i = 0; i < numberElems; i++) {
                v1[i] = v1[i] - v2[i];
            }
        }
    };


    template<class Y> class SubImpl<Y,false> {
        public:
        static void merge(Y* v1, Y* v2, unsigned int numberElems) {
            UNUSED(v1);
            UNUSED(v2);
            UNUSED(numberElems);

            DEBUG_MSG("SUB function disabled%s", ".");
            throw NotSupportedMergeOperationException(typeid(Y).name());
        }
    };

    /* Multiplication Operation */

    template<class U> class MultImpl<U, true> {
        public:
        static void merge(U* v1, U* v2, unsigned int numberElems) {
            for(unsigned int i = 0; i < numberElems; i++) {
                v1[i] = v1[i] * v2[i];
            }
        }
    };

    template<class U> class MultImpl<U, false> {
        public:
        static void merge(U* v1, U* v2, unsigned int numberElems) {
            UNUSED(v1);
            UNUSED(v2);
            UNUSED(numberElems);

            DEBUG_MSG("MULT function disabled%s", ".");
            throw NotSupportedMergeOperationException(typeid(U).name());
        }
    };

    /* Division Operation */

    template<class I> class DivImpl<I, true> {
        public:
        static void merge(I* v1, I* v2, unsigned int numberElems) {
            for(unsigned int i = 0; i < numberElems; i++) {
                v1[i] = v1[i] / v2[i];
            }
        }
    };

    template<class I> class DivImpl<I,false> {
        public:
        static void merge(I* v1, I* v2, unsigned int numberElems) {
            UNUSED(v1);
            UNUSED(v2);
            UNUSED(numberElems);

            DEBUG_MSG("DIV function disabled%s", ".");
            throw NotSupportedMergeOperationException(typeid(I).name());
        }
    };
}
#endif /* __MARROW_MERGEOP_HPP__ */
