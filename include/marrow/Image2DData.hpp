/*
 * Image2DData.hpp
 *
 *  April 2012:
 *       Ricardo Marques
 *       Creation of the Image2D datatype
 *
 *  December 2013:
 *      Herve Paulino
 *      marrow namespace
 *
 *  Created on: 4 de Abr de 2012
 *      Author: Ricardo Marques
 *	       Modified by Herve Paulino
 *
 *  Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef __MARROW_IMAGE2DDATA_HPP__
#define __MARROW_IMAGE2DDATA_HPP__

#include "runtime/IWorkData.hpp"

namespace marrow {

    /**
     * Class that represents an OpenCL 2D image object.
     */
    class Image2DData: public IWorkData {
    public:

        /**
         * Base constructor for an Image2DData object. It specifies the dimensions of the image, along with its color channel data, and
         * color channel order. The supported color data, and color channel order are defined in IWorkData.
         *
         * @param width - The number of elements in the image's X dimension.
         * @param height - The number of elements in the image's Y dimension.
         * @param channelData - The color channel data for the image.
         * @param order - The color channel order for the image.
         */
        Image2DData(unsigned int width, unsigned int height, imageChannelData channelData, imageChannelOrder order, IWorkData::partitionMode partitioningMode);

        /**
         * Second constructor for an Image2DData object. It specifies the dimensions of the image, along with its color channel data, and
         * color channel order. The supported color data, and color channel order are defined in IWorkData. Moreover, the access permitions
         * for the image, are defined.
         *
         * @param width - Number of elements in the image's X dimension.
         * @param height - Number of elements in the image's Y dimension.
         * @param channelData - The color channel data for the image.
         * @param order - The color channel order for the image.
         * @param accessMode - Access permitions for the image object.
         */
        Image2DData(unsigned int width, unsigned int height, imageChannelData channelData, imageChannelOrder order, IWorkData::dataMode accessMode, IWorkData::partitionMode partitioningMode);

        /**
         * Class destructor.
         */
        virtual ~Image2DData();

        /** IWorkData Interface Functions **/

        virtual unsigned long getMemSize();

        virtual dataMode getAccessMode();

        virtual dataType getDataType();

        virtual format getFormat();

        virtual partitionMode getPartitionMode();

        virtual unsigned int getIndivisibleSize();

        virtual bool isDoublePrecision();

        virtual bool isEqual(IWorkData &other);

        virtual unsigned int getWidth();

        virtual unsigned int getHeight();

        virtual unsigned int getDepth();

        virtual void* value();

        virtual void merge(void * values1, void * values2);

    protected:
        // The width of the image
        unsigned int width;
        // The height of the image
        unsigned int height;
        // The access permitions for the image
        dataMode accessMode;
        // The type of the data-type object
        dataType type;
        // The image's color channel order
        imageChannelOrder order;
        // The image's color channel data
        imageChannelData channelData;
        // Format is the structure that contains both the image channel order
        // and the image channel data informtion
        format f;
        partitionMode partitioningMode;

        /**
         * The number of bytes that are needed to store a single color channel of type @channelData.
         *
         * @param channelData - The channel data information, used in the calculation.
         * @return The number for bytes that are needed to store a single channel.
         */
        unsigned int channelByteSize(imageChannelData channelData);

        /**
         * Method that returns the number of color channels in the color channel order @order.
         *
             * @param order - The color channel from which the number of channels if calculated.
         * @return The number of color channels of @order.
         */
        unsigned int numberChannels(imageChannelOrder order);
    };
}
#endif /* __MARROW_IMAGE2DDATA_HPP__ */
