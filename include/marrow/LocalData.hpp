/*
 * LocalData.hpp
 *
 *  April 2012:
 *       Ricardo Marques
 *       Creation of the local (GPU) datatype
 * 
 *  December 2013:
 *      Herve Paulino
 *      marrow namespace
 *
 *  Created on: 2 de Abr de 2012
 *      Author: Ricardo Marques
 *			Modified by Herve Paulino
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef __MARROW_LOCALDATA_HPP__
#define __MARROW_LOCALDATA_HPP__

#include "DataInfo.hpp"

namespace marrow {

    /**
     * Template class that represents an OpenCL local memory location. It is used when a kernel has a __local type argument.
     * If a skeleton encounters an LocalData data-type it pre-allocates a certain amount of local memory, if possible.
     * This memory space will be ready to use, when the kernel is launched.
     */
    template <class T>
    class LocalData: public DataInfo<T> {
    public:
        /**
         * Constructor that specifies the number of elements, of type T, that the local memory space may hold.
         * This memory space is defined as read-write.
         *
         * @param numberElems - Number of elements of type T that can be stored in the respective local memory space.
         */
        LocalData(unsigned int numberElems);

        /**
         * Class destructor
         */
        virtual ~LocalData();
    };

    template <class T> LocalData<T>::LocalData(unsigned int numberElems) : DataInfo<T>(numberElems, IWorkData::DEFAULTACCESSMODE, IWorkData::LOCAL, IWorkData::SUM_MERGE_FTN){}

    template <class T> LocalData<T>::~LocalData(){}
}

#endif /* __MARROW_LOCALDATA_HPP__ */
