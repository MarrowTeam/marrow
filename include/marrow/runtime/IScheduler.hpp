/*
 * IScheduler.hpp
 *
 *  September 2013:
 *      Fernando Alexandre
 *      Creation of the Scheduler interface
 *
 *  Created on: 24 de Fev de 2013
 *      Author: Fernando Alexandre
 *
 *  Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef __MARROW_ISCHEDULER_HPP__
#define __MARROW_ISCHEDULER_HPP__

#include <memory>

#include "Task.hpp"
#include "marrow/runtime/IWorkData.hpp"

namespace marrow {
    class IExecutable;


    namespace runtime {

        /**
         * Task scheduler for multi-GPU execution
         */
        class IScheduler {
        public:
            /**
             * Class destructor
             */
            virtual ~IScheduler() {};

            /**
             * Submits a marrow::Skeleton for GPU execution.
             *
             * @param task The computation to be executed.
             */
            virtual void submit(std::shared_ptr<Task> task) = 0;

            /**
             * Sets the partitions of a computation tree.
             *
             * @param s The computation executed.
             * @param isReconfigure Whether the partitions are already set and
             *		require re-allocation.
             */
            virtual void setPartitions(marrow::Skeleton* s, bool isReconfigure) = 0;

            /**
             * Gets the number of overlapping partitions currently configured.
             *
             * @return The number of overlapping partitions.
             */
            virtual unsigned int getNumOverlapPartitions() = 0;

            /**
             * Gets the number of GPU devices to be used for the computations.
             *
             * @return The number of GPU devices.
             */
            virtual unsigned int getNumDevices() = 0;
        };
    }
}
#endif /* __MARROW_ISCHEDULER_HPP__ */
