/*
 * PartitionedInfo.hpp
 *
 *  April 2013:
 *		Fernando Alexandre
 *		Creation of the partitioned data definition
 *
 *  Created on: 3 de Apr de 2013
 *      Author: Fernando Alexandre
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef PARTITIONEDINFO_HPP_
#define PARTITIONEDINFO_HPP_

#include <vector>
#include "IWorkData.hpp"

/**
 * Class that defines a partitioned argument element (IWorkData).
 */
class PartitionedInfo {
public:
	// Defines a data partition
	// - begin_cpy: beginning element to copy
	// - begin_comp: beginning element for computation.
	typedef struct {
		unsigned int begin_cpy;
		unsigned int begin_comp;
		unsigned int size;
		IWorkData* segmentInfo; // used for implicit allocation by the execution platform.
	} DataSegment;

	/**
	 * Constructor
	 *
	 * @param nPartitions
	 * @param numOverlappedPartitions
	 */
	PartitionedInfo(const unsigned int nPartitions, const unsigned int numOverlappedPartitions);

	/**
	 * Class destructor
	 */
	~PartitionedInfo();

	/**
	 * Gets the number of partitions configured.
	 *
	 * @return The number of partitions.
	 */
	virtual unsigned int getNumPartitions();

	/**
	 * Gets the number of overlapping partitions configured.
	 *
	 * @return The number of overlapping partitions.
	 */
	virtual unsigned int getNumOverlapPartitions();

	/**
	 * Get a data segment from this object.
	 *
	 * @param deviceIndex - The index of the device.
	 * @param segIndex - The index of the segment (normally the same as overlap partition).
	 * @return The data segment at the desired position.
	 */
	virtual DataSegment getSegment(const unsigned int deviceIndex, const unsigned int segIndex);

	/**
	 * Set a segment to this object.
	 *
	 * @param segment - The data segment to be set.
	 * @param deviceIndex - The index of the device.
	 * @param segIndex - The index of the segment (normally the same as overlap partition).
	 */
	virtual void setSegment(const DataSegment segment, const unsigned int deviceIndex, const unsigned int segIndex);

protected:
	// vector for each device => vector for overlapped partitions within each device execution.
	std::vector<std::vector<DataSegment>> segments;

	/**
	 * Number of overlapping partitions.
	 */
	unsigned int numOverlapPartitions;
	/**
	 * Number of partitions.
	 */
	unsigned int numPartitions;
};

#endif /* PARTITIONEDINFO_HPP_ */
