/*
 * IKernelBuilder.hpp
 *
 *  February 2012:
 *		Ricardo Marques
 *		Creation of the kernel builder interface
 *
 *  September 2013:
 *		Fernando Alexandre
 *		Adaptation for multi-GPU support
 *
 *
 *  Created on: 22 de Fev de 2012
 *      Author: Ricardo Marques
 *			Modified by Fernando Alexandre
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef IKERNELBUILDER_HPP_
#define IKERNELBUILDER_HPP_

#ifdef __APPLE__
#include <OpenCL/cl.h>
#else
#include <CL/cl.h>
#endif
#include <string>
#include <vector>

/**
 * Interface for a KernelBuilder type object. This object encapsulates the OpenCL kernel compilation process in a single entity.
 */
class IKernelBuilder {
public:
	/**
	 * Class destructor
	 */
	virtual ~IKernelBuilder(){};

	/**
	 * Method that creates an OpenCL kernel object from contextual information, and returns it for consequent usage.
	 * The kernel creation process may use pre-compiled binaries, if they are available.
	 * Otherwise, the source code, identified by @filename, is compiled and loaded into the devices, identified in @devices.
	 *
	 * @param filename - The path file for the kernel source code.
	 * @param kernelFunc - A representative string for the name of the __kernel function to be transformed into a kernel.
	 * @param context - The context on which the kernel is to be executed.
	 * @param devices - A vector containing all the device identifiers, that refer to the OpenCL devices on which the kernel is to be executed.
	 * @param devicesIndices - A vector containing the indexes of the devices in the execution platform (to name the compiled opencl files).
	 * @return An OpenCL kernel object.
	 */
	virtual cl_kernel createKernelFromFile(const std::string &fileName, const std::string &kernelFunc, cl_context context, std::vector<cl_device_id> &devices, std::vector<unsigned int> &deviceIndices) = 0;

	/**
	 * Creates a OpenCL kernel instance from a program.
	 *
	 * @param program - The OpenCL program.
	 * @param kernelFunc - The function's name.
	 * @return The OpenCL kernel object.
	 */
	virtual cl_kernel getNewKernelInstance(const cl_program program, const std::string &kernelFunc) = 0;

	/**
	 * Creates an OpenCL program.
	 *
	 * @param fileName - The name of the file with the OpenCL source code.
	 * @param context - The OpenCL context
	 * @param devices - A vector containing all the device identifiers, that refer to the OpenCL devices on which the kernel is to be executed.
	 * @param devicesIndices - A vector containing the indexes of the devices in the execution platform (to name the compiled opencl files).
	 * @return The OpenCL program object (which is used to spawn kernel objects).
	 */
	virtual cl_program getProgram(const std::string &fileName, const cl_context context, std::vector<cl_device_id> &devices, std::vector<unsigned int> &deviceIndices) = 0;

};

#endif /* IKERNELBUILDER_H_ */
