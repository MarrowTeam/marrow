/*
 * Task.hpp
 *
 *  February 2013:
 *		Fernando Alexandre
 *		Creation of the Task concept used by the scheduler
 *
 *  Created on: 24 de Fev de 2013
 *      Author: Fernando Alexandre
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef TASK_HPP_
#define TASK_HPP_

#include <string>
#include <vector>
#include <memory>
#include <boost/thread.hpp>

#include "marrow/runtime/IWorkData.hpp"
#include "marrow/Vector.hpp"
#include "marrow/IFuture.hpp"

namespace marrow {
    class Skeleton;
}

/**
 * Class that defines a task to be scheduled.
 */
class Task {
public:
	/**
	 * Constructor for the Task. As this object is mostly a container,
	 * this constructor is straightforward.
	 *
	 * @param skel - The pointer to the root of the computation tree.
	 * @param uniqueId - The unique id of this task.
	 * @param iData - The input data-set.
	 * @param oData - The output data-set.
	 * @param future - The future assigned to this execution.
	 */
	Task(marrow::Skeleton* skel,
		const unsigned int uniqueId,
		std::vector<std::shared_ptr<marrow::Vector>> *iData,
		std::vector<std::shared_ptr<marrow::Vector>> *oData,
		marrow::IFuture* future);

	/**
	 * Class deconstructor.
	 */
	virtual ~Task();

	/**
	 * Gets whether this task has finished (all overlap partitions have finished).
	 *
	 * @return Whether this task has finished.
	 */
	virtual bool hasFinished();

	/**
	 * Increments the number of overlapping partitions that have finished computation.
	 */
	virtual void incFinished();

	/**
	 * Gets a pointer to the root of the computation tree.
	 *
	 * @return Pointer to the root of the computation tree.
	 */
	virtual marrow::Skeleton* getComputation();

	/**
	 * Gets the input data-set.
	 *
	 * @return The input data-set.
	 */
	virtual std::vector<std::shared_ptr<marrow::Vector>> * getInputData();

	/**
	 * Gets the output data-set.
	 *
	 * @return the output data-set.
	 */
	virtual std::vector<std::shared_ptr<marrow::Vector>> * getOutputData();

	/**
	 * Gets the reference for the future of this task.
	 *
	 * @return the reference for the future object.
	 */
	virtual marrow::IFuture* getFuture();

	/**
	 * Gets this task's unique id.
	 *
	 * @return this task's unique id.
	 */
	virtual unsigned int getUniqueId();

	/**
	 * Sets the number of partitions of this task.
	 *
	 * @param numPartitions - the number of partitions.
	 */
	virtual void setNumPartitions(unsigned int numPartitions);

	/**
	 * Sets the number of overlapping partitions of this task.
	 *
	 * @param numOverlapPartitions - the number of overlapping partitions.
	 */
	virtual void setNumOverlapPartitions(unsigned int numOverlapPartitions);

	/**
	 * Gets a pointer to this task's mutex.
	 *
	 * @return pointer to this task's mutex.
	 */
	virtual boost::mutex * getTaskMutex();


protected:
	// The computation
	marrow::Skeleton* computation;
	// This task's unique id.
	unsigned int uniqueId;

	// Unpartitioned input data
	std::vector<std::shared_ptr<marrow::Vector>> *inputData;
	// Unpartitioned output data
	std::vector<std::shared_ptr<marrow::Vector>> *outputData;

	// Future reference assigned to this computation
	marrow::IFuture* future;
	// Whether this computation is finished.
	bool isCompleted;
	// This task's mutex
	boost::mutex taskMutex;
	// Number of finished overlapping partitions.
	unsigned int numComputed;
	// Number of partitions.
	unsigned int numPartitions;
	// Number of overlapping partitions per partition.
	unsigned int numOverlapPartitions;
};

#endif /* TASK_HPP_ */
