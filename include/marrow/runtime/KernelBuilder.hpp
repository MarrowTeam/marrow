/*
 * KernelBuilder.h
 *
 *  February 2012:
 *		Ricardo Marques
 *		Creation of the kernel builder component
 *
 *  September 2013:
 *		Fernando Alexandre
 *		Adaptation to support multiple GPU compilation
 *
 *  Created on: 23 de Fev de 2012
 *      Author: Ricardo Marques
 *			Modified by Fernando Alexandre
 *
 *  Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef KERNELBUILDER_HPP_
#define KERNELBUILDER_HPP_

#include <iostream>
#include <sstream>
#include <fstream>

#include "IKernelBuilder.hpp"	

// the default extension of a pre-compiled kernel source
#define EXTENSION ".bin"

/**
 * A class that implements the IKernelBuilder interface.
 */
class KernelBuilder: public IKernelBuilder {
public:
	/**
	 * Default constructor
	 */
	KernelBuilder();

	/**
	 * Class destructor
	 */
	virtual ~KernelBuilder();

	/**
	 * Implementation of the IKernelBuilder interface function
	 */
	virtual cl_kernel createKernelFromFile(const std::string &fileName, const std::string &kernelFunc, cl_context context, std::vector<cl_device_id> &devices, std::vector<unsigned int> &deviceIndices);

	/**
	* Obtain of a new instance of the given kernel
	*/
	virtual cl_kernel getNewKernelInstance(const cl_program program, const std::string &kernelFunc);

	/**
	 * Method that creates a program object from the kernel source code, or pre-compiled binaries if they are available.
	 * Moreover, if no pre-compiled binaries are available, or the ones that are not compatible with the devices identified by @devices, 
	 * then new binaries are compiled and stored in the same directory as the kernel source.
	 *
	 * @param fileName - The path file for the kernel source code.
	 * @param context - The context on which the kernel is to be executed.
	 * @param devices - A vector containing all the device identifiers, that refer to the OpenCL devices on which the kernel is to be executed.
	 * @return A compiled OpenCL program object, for the specified kernel source file.
	 */
	virtual cl_program getProgram(const std::string &fileName, const cl_context context, std::vector<cl_device_id> &devices, std::vector<unsigned int> &deviceIndices);

protected:
	// Error parser helper
//	OpenCLErrorParser parser;


	/**
	 * Method that creates a program object from the pre-compiled binaries if they are available.
	 * If no binaries are available, or the ones that are are not compatible with the devices, identified by @devices, then NULL is returned.
	 *
	 * @param fileName - The path file for the kernel source code.
	 * @param context - The context on which the kernel is to be executed.
	 * @param devices - A vector containing all the device identifiers, that refer to the OpenCL devices on which the kernel is to be executed.
	 * @return A OpenCL program, generated using pre-compiled binaries. Or NULL if the binaries are not available,
	 * or are not compatible with the devices.
	 */
	virtual cl_program programFromBinaries(const std::string &fileName, const cl_context context, std::vector<cl_device_id> &devices);

	/**
	 * Method that creates a program object from the kernel source code. This function does not require the specification of the devices, since
	 * the underlying OpenCL function infers this information directly from the @context object.
	 * If it is not possible to generate a correct OpenCL program object from the source code, this function returns NULL.
	 *
	 * @param fileName - The path file for the kernel source code.
	 * @param context - The context on which the kernel is to be executed.
	 * @return A OpenCL program, generated using the source code. Or NULL if it was not possible to use the source to generate a program object.
	 */
	virtual cl_program programFromSource(const std::string &fileName, const cl_context context);

	/**
	 * Method that generates kernel binaries from an, assumed, valid OpenCL program. After these binaries are generated, they are stored
	 * in the same directory as the kernel, whose path is specified by @filename. The binaries are created for each of the devices associated
	 * to the @program object.
	 *
	 * @param fileName - The path file for the kernel source code.
	 * @param program - The valid OpenCL program that is to be used to create pre-compiled kernel binaries.
	 * @param numDevices - The number of devices associated to the program object.
	 * @return True if the binaries where stored successefully, or false otherwise.
	 */
	virtual bool saveKernelBinaries(const std::string &fileName, const cl_program program, unsigned int numDevices);

	/**
	 * Method that is able to verify the size, in bytes, of a given file. The file is represented by a STL ifsteam object.
	 *
	 * @param stream - The stream object that is associated to the file.
	 * @return The file size, in bytes, of the file associated to @stream.
	 */
	virtual unsigned int fileSize(std::ifstream &stream);
};

#endif /* KERNELBUILDER_H_ */
