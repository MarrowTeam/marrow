/*
 * IWorkData.h
 *
 *  March 2012:
 *		Ricard Marques
 *		Creation of the interface of a computation argument
 *
 *  September 2013:
 *		Fernando Alexandre
 *		Adaptation for additional argument functionality regarding multi-GPUs
 *
 *  Created on: 13 de Mar de 2012
 *      Author: Ricardo Marques
 *			Modified by Fernando Alexandre
 *
 *  Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef IWORKDATA_HPP_
#define IWORKDATA_HPP_

#ifdef __APPLE__
	#include <OpenCL/cl.h>
#else
	#include <CL/cl.h>
#endif

#include <vector>

/**
 * Interface for objects that are used to define the kernel argument data-types
 */
class IWorkData {
protected:
	// Strcut used to define a IMAGE object
	struct format {
		cl_image_format clImageFormat;
	};

public:
	// Access modes
	enum dataMode {READ_ONLY = CL_MEM_READ_ONLY, WRITE_ONLY = CL_MEM_WRITE_ONLY, READ_WRITE = CL_MEM_READ_WRITE};
	static const dataMode DEFAULTACCESSMODE = READ_WRITE;

	// Available image data
	enum imageChannelData {UNORM_INT8 = CL_UNORM_INT8};
	enum imageChannelOrder {RGBA = CL_RGBA};

	// Available OpenCL kernel argument data-types
	enum dataType {BUFFER = 0, LOCAL = 1, SINGLETON = 2, FINAL = 3, IMAGE2D = 4};
	// The various partitioning option available.
	enum partitionMode {PARTITIONABLE = 0, COPY = 1, SUM_MERGE_FTN = 2, SUB_MERGE_FTN = 3, MULT_MERGE_FTN = 4, DIV_MERGE_FTN = 5, CUSTOM_FTN = 6};

	/**
	 * Class destructor
	 */
	virtual ~IWorkData(){};

	/**
	 * Method that returns the size in bytes of this data-type kernel argument.
	 *
	 * @return The number of bytes that make up this data-type object.
	 */
	virtual unsigned long getMemSize() = 0;

	/**
	 * Method that returns the access permitions for this data-type argument.
	 *
	 * @return A enumerion of type dataMode, that represents the access permitions for this data-type object. Usual permitions are, for instance,
	 * read-only, write-only, read-write.
	 */
	virtual dataMode getAccessMode() = 0;

	/**
	 * Method that returns a value that defines the data-structure. This returned value may be used to identify what kind of data-structure is
	 * represented by this data-type object. These include such data-structures as - buffers, images, singleton values, as well as others.
	 *
	 * @return A enumeration type dataType, representing a data-structure supported by the runtime.
	 */
	virtual dataType getDataType() = 0;

	/**
	 * Gets the partitioning mode of this data-type.
	 *
	 * @return The partitioning mode of this data-type.
	 */
	virtual partitionMode getPartitionMode() = 0;

	/**
	 * Method that returns the format of this object. This method is usefull is the data-type represents a Image object. The contains
	 * image associated information such as the color channels, or channel data organization. On non-image data-types, and exception is raised.
	 *
	 * @return Format information about this image data-type object.
	 */
	virtual format getFormat() = 0;

	/**
	 * Gets the indivisible size of this data-type.
	 *
	 * @return the indivisible size of this data-type
	 */
	virtual unsigned int getIndivisibleSize() = 0;

	/**
	 * Gets a boolean which indicates whether this data-type has a double
	 * precision or not.
	 *
	 * @return whether this is a double-precision data-type or not.
	 */
	virtual bool isDoublePrecision() = 0;

	/**
	 * Method that asserts if this data-type is equal to a given one.
	 *
	 * @return True if both are equal, or false otherwise.
	 */
	virtual bool isEqual(IWorkData &other) = 0;

	/**
	 * Method that returns the width of this data-type object. The width refers to the number of elements, in the first dimension, that this
	 * data-type is able to store. For example, a unidimensional buffer data-type simply returns the number of elements that it may hold.
	 *
	 * @return The width of this data-type kernel argument.
	 */
	virtual unsigned int getWidth() = 0;

	/**
	 * Method that returns the height of this data-type object. The height refers to the number of elements, in the second dimension, that this
	 * data-type is able to store. For example, a 2D image object data-type returns the number of elements that it may hold in the y dimension.
	 *
	 * @return The height of this data-type kernel argument.
	 */
	virtual unsigned int getHeight() = 0;

	/**
	 * Method that returns the depth of this data-type object. The depth refers to the number of elements, in the third dimension, that this
	 * data-type is able to store. For example, a 3D image object data-type returns the number of elements that it may hold in the z dimension.
	 *
	 * @return The depth of this data-type kernel argument.
	 */
	virtual unsigned int getDepth() = 0;

	/**
	 * Method that returns the concrete data associated to this data-type. For instance, a integer final singleton data-type may hold the
	 * actual integer that defines it. Data-types that do not support this kind of information query will raise an exception.
	 *
	 * @return The actual value associated to this data-type kernel argument.
	 */
	virtual void* value() = 0;

	/**
	 * Merges the set of values. This function is supplied by the programmer.
	 *
	 * @param values1 - first set of values.
	 * @param values2 - second set of values.
	 */
	virtual void merge(void * values1, void * values2) = 0;
};

#endif /* IWORKDATA_H_ */
