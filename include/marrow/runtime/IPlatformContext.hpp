/*
 * IPlatformContext.hpp
 *
 *  April 2012:
 *		Ricardo Marques
 *		Creation of the Platform context interface
 *
 *	September 2013:
 *		Fernando Alexandre
 *		Adaptation for multi-GPU support
 *
 *  Created on: 3 de Abr de 2012
 *      Author: Ricardo Marques
 *			Modified by Fernando Alexandre
 *
 *  Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef IPLATFORMCONTEXT_HPP_
#define IPLATFORMCONTEXT_HPP_

#ifdef __APPLE__
	#include <OpenCL/cl.h>
#else
	#include <CL/cl.h>
#endif

#include <vector>

#include "IWorkData.hpp"


/**
 * Interface that represents part of a shared execution environment. It is used to allocate some of the most common OpenCL resources, and
 * performs automatic resource managment, and cleanup. Furthermore, it provides information about what devices are to be used in the execution,
 * as well as, specific information about each device.
 */
class IPlatformContext {
public:
	/**
	 * Class destructor
	 */
	virtual ~IPlatformContext(){};

	/**
	 * Initializes the platform while taking into account the number of devices, overlapping partitions and type.
	 *
	 * @param numDevices - number of devices to be used.
	 * @param types - type(s) of the devices.
	 * @param numOverlap - number of overlapping partitions.
	 */
	virtual void initialize(const std::vector<unsigned int> &numDevices, const std::vector<cl_device_type> &types, unsigned int numOverlap) = 0;

	/**
	 * Initializes the platform while using the maximum number of available devices of a certain type and a number of overlapping partitions.
	 *
	 * @param types - type(s) of the devices.
	 * @param numOverlap - number of overlapping partitions.
	 */
	virtual void initialize(const std::vector<cl_device_type> &types, unsigned int numOverlap) = 0;

	/**
	 * Method that returns the OpenCL context object associated to this PlatformContext.
	 *
	 * @param deviceIndex - index of the device.
	 * @return The OpenCL context object associated to the desired device.
	 */
	virtual cl_context getPlatformContext(unsigned int deviceIndex, unsigned int numOverlap) = 0;

	/**
	 * Gets all the platform contexts (one per overlap partition).
	 *
	 * @return A vector containing the set of contexts within each device.
	 */
	virtual std::vector<std::vector<cl_context>> * getAllPlatformContexts() = 0;

	/**
	 * Method that returns the number of devices associated to this PlatformContext, and implicitly, to the underlying OpenCL context.
	 *
	 * @return The number of devices associated to this PlatformContext.
	 */
	virtual unsigned int getNumDevices() = 0;

	/**
	 * Method that returns the device identifiers, for the devices associated to this PlatformContext. The identifiers are stored in a vector,
	 * parametrized to this function. The number of device identifiers returned in @devices is the arithmetic minimum between the total number of devices
	 * associated to the PlatformContext, and the size of the @devices vector. In this way, the user can limit the query to a specific number of devices.
	 * The order by which the identifiers are returned, is the same as the order in which they are available in the underlying OpenCL context.
	 *
	 * @param devices - The device identifier placeholder, to be affected with the identifiers for the devices associated to this PlatformContext.
	 */
	virtual void getDevices(std::vector<cl_device_id> &devices) = 0;

	/**
	 * Method which gets all the devices of a @type. These are added to the @devices vector.
	 *
	 * @param type - The type of device which is to be queried, such as GPU, CPU or accelerator.
	 * @param devices - The device indentifier placeholder, to be affected to contain the devices of @type.
	 */
	virtual void getDevicesOfType(cl_device_type type, std::vector<cl_device_id> &devices) = 0;

	/**
	 * Method that returns the number of devices of type @type, associated to this PlatformContext.
	 *
	 * @param type - The type of the devices. For instance, is may represent a GPU, or a multi-core CPU.
	 * @return The number of devices of type @type, associated to this PlatformContext.
	 */
	virtual unsigned int getNumDevicesOfType(cl_device_type type) = 0;

	/**
	 * Method that allocates a memory object, based on the information retrived from the @dataInfo data-type object. A reference to this memory object
	 * is returned to the caller. However, if @dataInfo does not reference a data-structure representable as an OpenCL memory object (e.g., buffer),
	 * then NULL is returned. This method also helps keep track of the amount of free memory.
	 *
	 * @param dataInfo - The data-type object that contains the necessary information to allocate the memory object.
	 * @param deviceIndex - index of the device to allocate the memory.
	 * @param overlapIndex - the index of the overlapping partition (to use the correct context).
	 * @return A reference to the newly created memory object, or NULL if an error ocurred.
	 */
	virtual cl_mem allocateDeviceMem(IWorkData &dataInfo, unsigned int deviceIndex, unsigned int overlapIndex) = 0;

	/**
	 * Frees an opencl memory object. Helps keep track of the free memory using the device index and the original IWorkdata object.
	 *
	 * @param dataInfo - The data-type object that contains the necessary information to allocate the memory object.
	 * @param deviceIndex - index of the device the memory is at.
	 * @param deviceMem - Memory location to be freed.
	 */
	virtual void freeDeviceMem(IWorkData &dataInfo, unsigned int deviceIndex, cl_mem deviceMem) = 0;

	/**
	 * Method that allocates an OpenCL command-queue for the device number @deviceIndex, associated to this PlatformContext. The indexes range from zero
	 * to the number of devices associated to the PlatformContext minus 1. A reference to this command-queue is subsequently returned to the caller. If
	 * @deviceIndex is larger than the number of devices associated to the PlatformContext minus 1, then NULL is returned. Similarly, if it is impossible
	 * to create the command-queue, NULL is returned.
	 *
	 * @param deviceIndex - The index of the device to which the newly created command-queue is bound.
	 * @param overlapIndex - The index of the overlapping partition.
	 * @return A reference to the newly created command-queue, or NULL if the runtime was unable to allocate such structure.
	 */
	virtual cl_command_queue createCommandQueue(unsigned int deviceIndex, unsigned int overlapIndex) = 0;

	/**
	 * Method that returns the type of the device number @deviceIndex of this PlatformContext, if such a devic exists. If the index does not point to a
	 * valid OpenCL device, then NULL is returned. The index may range from zero to the number of devices associated to this PlatformContex minus one.
	 *
	 * @param deviceIndex - The index of the device to be queried.
	 * @return The type of the device number @deviceIndex, or NULL if such device is not available in this PlatformContext.
	 */
	virtual cl_device_type getType(unsigned int deviceIndex) = 0;

	/**
	 * Method that returns the total size, in bytes, of the global device memory, available in device number @deviceIndex. This number of bytes only
	 * represents the size of the global memory adress space, and does not inform the caller if the actual space if available for use, or already contains
	 * any allocated data. The device index may range from zero to the number of devices associated to this PlatformContex minus one. If the device index
	 * does not point to a valid device, zero is returned.
	 *
	 * @param deviceIndex - The index of the device to be queried.
	 * @return The byte size of the global memory adress space, of device number @deviceIndex. Or -1, if an error occurs.
	 */
	virtual cl_ulong getGlobalMemorySize(unsigned int deviceIndex) = 0;

	/**
	 * Method the returns the amount of free memory of a device.
	 *
	 * @param deviceIndex - index of the device.
	 * @return The amount of free memory in bytes.
	 */
	virtual cl_ulong getFreeMemorySize(unsigned int deviceIndex) = 0;

	/**
	 * Method the returns the total amount of free memory.
	 *
	 * @return The amount of free memory in bytes.
	 */
	virtual cl_ulong getTotalFreeMemorySize() = 0;

	/**
	 * Method that returns the maximum number for work-items for each work-group in each dimension, supported by the device number @deviceIndex.
	 * The values are stored in the @localSize vector. This vector is resized to the maximum number of dimensions supported by the target device,
	 * and then filled with the values, for the maximum number for work-items for each work-group, in each dimension. Note that, these values do not
	 * represent the maximum size of the local work-load, only show the limitations of each particular dimension in terms of work-group length.
	 * The size of the work-load is calculated by multipliying the number of work-items to be used in each dimension of the work-group.
	 * To obtain the maximum work-load size value, the user should invoque IPlatformContext::maxWorkgroupLenght.
	 * The device index may range from zero to the number of devices associated to this PlatformContex minus one. If the device index does not point
	 * to a valid device, then zero is returned.
	 *
	 * @param deviceIndex - The index of the device to be queried.
	 * @param localSize - The placeholder for the maximum work-item per work-group values, in each dimension, supported by device number @deviceIndex.
	 * If an error occurs, @localSize is not altered.
	 */
	virtual void maxWorkgroupLenghtPerDim(unsigned int deviceIndex, std::vector<size_t> &localSize) = 0;

	/**
	 * Method that returns the maximum number of work-items that can be grouped to form a work-group, supported by the device number @deviceIndex.
	 * A size of a work-group is obtained by multipliying the number of work-items to be used in each dimension of that work-group. For example,
	 * work-group[512][2] and work-group[1024] have the same size, which is 1024 work-items. This value is irrespective of the number of
	 * work-items supported in each of the work-group's dimensions.
	 * The device index may range from zero to the number of devices associated to this PlatformContex minus one. If the device index does not point
	 * to a valid device, then zero is returned.
	 *
	 * @param deviceIndex - The index of the device to be queried.
	 * @return The maximum size of a work-group, supported by device number @deviceIndex. If an error occurs, -1 is returned.
	 */
	virtual size_t maxWorkgroupLenght(unsigned int deviceIndex) = 0;

	/**
	 * Method that returns the maximum work-load dimensions, supported by the device number @deviceIndex.
	 *
	 * @param deviceIndex - The index of the device to be queried.
	 * @return The maximum number of dimensions, supported by device number @deviceIndex. If an error occurs, -1 is returned.
	 */
	virtual cl_uint maxDimensions(unsigned int deviceIndex) = 0;

	/**
	 * Method that returns the total size, in bytes, of the local device memory, available in device number @deviceIndex. This number of bytes only
	 * represents the size of the local memory adress space, and does not inform the caller if the actual space if available for use, or already contains
	 * any allocated data. The device index may range from zero to the number of devices associated to this PlatformContex minus one. If the device index
	 * does not point to a valid device, zero is returned.
	 *
	 * @param deviceIndex - The index of the device to be queried.
	 * @return The byte size of the local memory adress space, of device number @deviceIndex. Or -1, if an error occurs.
	 */
	virtual cl_ulong getLocalMemorySize(unsigned int deviceIndex) = 0;

	/**
	 * Method states whether the device number @deviceIndex supports, or not, image processing. If the device index does not point to a valid device,
	 * false is returned.
	 *
	 * @param deviceIndex - The index of the device to be queried.
	 * @return True if the device supports image processing, or false otherwise. In case of an error, false is returned.
	 */
	virtual bool supportImages(unsigned int deviceIndex) = 0;
};

#endif /* IPLATFORMCONTEXT_HPP_ */
