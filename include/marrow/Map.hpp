/*
 * Map.hpp
 *
 *  February 2012:
 *       Ricardo Marques
 *       Creation of the Map skeleton
 *
 *  September 2013:
 *       Fernando Alexandre
 *       Adaptation for multi-GPU execution
 *
 *  December 2013:
 *      Herve Paulino
 *      marrow namespace
 *
 *  Created on: 29 de Fev de 2012
 *      Author: Ricardo Marques
 *			Modified by Fernando Alexandre, Herve Paulino
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef __MARROW_MAP_HPP__
#define __MARROW_MAP_HPP__

#include "Skeleton.hpp"
#include "IExecutable.hpp"

namespace marrow {

    /**
     * Map skeleton. This skeleton simply applies an execution of an IExecutable to our
     * execution platform.
     */
    class Map: public marrow::Skeleton {
    public:
        /**
         * The Map is parametrized with an executable entity, @exec. The latter is an
         * instance of the marrow::IExecutable interface.
         *
         * @param exec - The executable entity on which the stream will prompt the executions.
         */
        Map(std::unique_ptr<marrow::IExecutable> &exec);

        /**
         * The Map is parametrized with an executable entity, @exec. The latter is an
         * instance of the marrow::IExecutable interface.
         *
         * @param exec - The executable entity on which the stream will prompt the executions.
         * @param numDevices - The number of devices to be used.
         */
        Map(std::unique_ptr<marrow::IExecutable> &exec, unsigned int numDevices);

        /**
         * The Map is parametrized with an executable entity, @exec. The latter is an
         * instance of the marrow::IExecutable interface.
         *
         * @param exec - The executable entity on which the stream will prompt the executions.
         * @param numDevices - The number of devices to be used.
         * @param numOverlapPartitions - The number of overlapping partitions to be used.
         */
        Map(std::unique_ptr<marrow::IExecutable> &exec, unsigned int numDevices, unsigned int numOverlapPartitions);

        /**
         * Class destructor.
         */
        virtual ~Map();

        #ifdef PROFILING
        virtual std::string getProfilingInfo();
        #endif

    protected:
        // Child execution entity
        std::unique_ptr<marrow::IExecutable> exec;
        // If child performs read operations from device memory
        bool childReads;

        /**
         * Auxiliar method that refracts part of the inicialization process, that is common to all constructors.
         *
         * @param isReconfigure - Whether this call is from a reconfigure context or not.
         */
        virtual void initMap(bool isReconfigure);

        /** marrow::Skeleton pure virtual functions **/

        virtual void executeSkel(const cl_command_queue &executionQueue,
                                const unsigned int deviceIndex,
                                const unsigned int uniqueId,
                                const unsigned int partitionIndex,
                                const unsigned int overlapPartition,
                                std::vector<std::shared_ptr<marrow::Vector>> &inputData,
                                std::vector<std::shared_ptr<marrow::Vector>> &outputData,
                                std::vector<cl_mem> &inputMem,
                                std::vector<cl_mem> &outputMem);

        virtual void uploadInputData(const cl_command_queue &executionQueue,
                                const unsigned int deviceIndex,
                                const unsigned int uniqueId,
                                const unsigned int partitionIndex,
                                const unsigned int overlapPartition,
                                std::vector<std::shared_ptr<marrow::Vector>> &inputData,
                                std::vector<cl_mem> &inputMem);

        virtual void downloadOutputData(const cl_command_queue &executionQueue,
                                const unsigned int deviceIndex,
                                const unsigned int uniqueId,
                                const unsigned int partitionIndex,
                                const unsigned int overlapPartition,
                                std::vector<std::shared_ptr<marrow::Vector>> &outputData,
                                std::vector<cl_mem> &outputMem);

        virtual unsigned long requiredGlobalMem();

        virtual std::vector<void*> * getExecutables();

        virtual void finishExecution(const unsigned int uniqueId,
                                std::vector<std::shared_ptr<marrow::Vector>> &outputData);

        virtual std::vector<std::shared_ptr<PartitionedInfo>> * getPartitionedInput();

        virtual std::vector<cl_mem> * getInputMem(unsigned int partitionIndex, unsigned int overlapPartitionIndex);

        virtual std::vector<std::shared_ptr<PartitionedInfo>> * getPartitionedOutput();

        virtual std::vector<cl_mem> * getOutputMem(unsigned int partitionIndex, unsigned int overlapPartitionIndex);
    };
}

#endif /* __MARROW_MAP_H__ */
