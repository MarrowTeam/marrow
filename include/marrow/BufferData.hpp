/*
 * BufferData.hpp
 *
 *  April 2012:
 *      Ricardo Marques
 *      Creation of the buffer argument type
 *
 *  September 2013:
 *       Fernando Alexandre
 *       Adaptation to support partitions
 *
 *  December 2013:
 *      Herve Paulino
 *      marrow namespace
 *
 *  Created on: 2 de Abr de 2012
 *      Author: Ricardo Marques
 *			Modified by Fernando Alexandre, Herve Paulino
 *
 *  Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef __MARROW_BUFFERDATA_HPP__
#define __MARROW_BUFFERDATA_HPP__

#include "DataInfo.hpp"

namespace marrow {

    /**
     * Template class that represents a buffer kernel argument, of a certain type T.
     */
    template <class T>
    class BufferData: public DataInfo<T> {
    public:
        /**
         * Constructor that specifies the number of elements, of type T, that the buffer may hold.
         * This buffer is defined as read-write and PARTITIONABLE, by default.
         *
         * @param numberElems - Number of elements of type T that the buffer can hold.
         */
        BufferData(unsigned int numberElems);

        /**
         * Constructor that specifies the number of elements, of type T, that the buffer may hold.
         * This buffer is defined as read-write with indivisible size of 1, by default.
         *
         * @param numberElems - Number of elements of type T that the buffer can hold.
         * @param partitioningMode - The partitioning mode for this buffer.
         */
        BufferData(unsigned int numberElems, IWorkData::partitionMode partitioningMode);

        /**
         * Constructor that specifies the number of elements, of type T, that the buffer may hold.
         * This buffer is defined as read-write, by default.
         *
         * @param numberElems - Number of elements of type T that the buffer can hold.
         * @param partitioningMode - The partitioning mode for this buffer.
         * @param indivisibleSize - The minimum size this decomposition must have (only usefull for PARTITIONABLE).
         */
        BufferData(unsigned int numberElems, IWorkData::partitionMode partitioningMode, unsigned int indivisibleSize);

        /**
         * Constructor that specifies the number of elements, of type T, that the buffer may hold.
         * This buffer is defined as read-write and PARTITIONABLE, by default.
         *
         * @param numberElems - Number of elements of type T that the buffer can hold.
         * @param indivisibleSize - The minimum size this decomposition must have (only usefull for PARTITIONABLE).
         */
        BufferData(unsigned int numberElems, unsigned int indivisibleSize);


        /**
         * Constructor that specifies the number of elements, of type T, that the buffer may hold.
         * Additionally, the user can specify the access permitions for the data-type buffer object.
         * It has a partitioning mode PARTITIONABLE by default and indivisible size of 1.
         *
         * @param numberElems - Number of elements of type T that the buffer can hold.
         * @param accessMode - Access permitions for the buffer.
         */
        BufferData(unsigned int numberElems, IWorkData::dataMode accessMode);

        /**
         * Constructor that specifies the number of elements, of type T, that the buffer may hold.
         * Additionally, the user can specify the access permitions and partitining mode for the
         * data-type buffer object.
         * It has a partitioning mode PARTITIONABLE by default and indivisible size of 1.
         *
         * @param numberElems - Number of elements of type T that the buffer can hold.
         * @param accessMode - Access permitions for the buffer.
         * @param partitioningMode - The partitioning mode for this buffer.
         */
        BufferData(unsigned int numberElems, IWorkData::dataMode accessMode, IWorkData::partitionMode partitioningMode);

        /**
         * Constructor that specifies the number of elements, of type T, that the buffer may hold.
         * Additionally, the user can specify the access permitions, partitioning mode and indivisible size
         * for the data-type buffer object.
         *
         * @param numberElems - Number of elements of type T that the buffer can hold.
         * @param accessMode - Access permitions for the buffer.
         * @param partitioningMode - The partitioning mode for this buffer.
         * @param indivisibleSize - The minimum size this decomposition must have (only useful for PARTITIONABLE).
         */
        BufferData(unsigned int numberElems, IWorkData::dataMode accessMode, IWorkData::partitionMode partitioningMode, unsigned int indivisibleSize);

        /**
         * Constructor that specifies the number of elements, of type T, that the buffer may hold.
         * Additionally, the user can specify the access permitions and indivisible size
         * for the data-type buffer object. The PARTITIONABLE partition mode is used by default.
         *
         * @param numberElems - Number of elements of type T that the buffer can hold.
         * @param accessMode - Access permitions for the buffer.
         * @param indivisibleSize - The minimum size this decomposition must have (only useful for PARTITIONABLE).
         */
        BufferData(unsigned int numberElems, IWorkData::dataMode accessMode, unsigned int indivisibleSize);

        /**
         * Class destructor.
         */
        virtual ~BufferData();
    };

    template <class T> BufferData<T>::BufferData(unsigned int numberElems) : DataInfo<T>(numberElems, IWorkData::DEFAULTACCESSMODE, IWorkData::BUFFER) {}

    template <class T> BufferData<T>::BufferData(unsigned int numberElems, IWorkData::partitionMode partitioningMode) : DataInfo<T>(numberElems, IWorkData::DEFAULTACCESSMODE, IWorkData::BUFFER, partitioningMode) {}

    template <class T> BufferData<T>::BufferData(unsigned int numberElems, IWorkData::partitionMode partitioningMode, unsigned int indivisibleSize) : DataInfo<T>(numberElems, IWorkData::DEFAULTACCESSMODE, IWorkData::BUFFER, partitioningMode, indivisibleSize) {}

    template <class T> BufferData<T>::BufferData(unsigned int numberElems, unsigned int indivisibleSize) : DataInfo<T>(numberElems, IWorkData::DEFAULTACCESSMODE, IWorkData::BUFFER, IWorkData::PARTITIONABLE, indivisibleSize) {}

    template <class T> BufferData<T>::BufferData(unsigned int numberElems, IWorkData::dataMode accessMode) : DataInfo<T>(numberElems, accessMode, IWorkData::BUFFER) {}

    template <class T> BufferData<T>::BufferData(unsigned int numberElems, IWorkData::dataMode accessMode, IWorkData::partitionMode partitioningMode) : DataInfo<T>(numberElems, accessMode, IWorkData::BUFFER, partitioningMode) {}

    template <class T> BufferData<T>::BufferData(unsigned int numberElems, IWorkData::dataMode accessMode, IWorkData::partitionMode partitioningMode, unsigned int indivisibleSize) : DataInfo<T>(numberElems, accessMode, IWorkData::BUFFER, partitioningMode, indivisibleSize) {}

    template <class T> BufferData<T>::BufferData(unsigned int numberElems, IWorkData::dataMode accessMode, unsigned int indivisibleSize) : DataInfo<T>(numberElems, accessMode, IWorkData::BUFFER, IWorkData::PARTITIONABLE, indivisibleSize) {}

    template <class T> BufferData<T>::~BufferData() {}

}

#endif /* __MARROW_BUFFERDATA_HPP__ */
