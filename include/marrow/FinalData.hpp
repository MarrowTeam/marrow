/*
 * FinalData.hpp
 *
 *  April 2012:
 *       Ricardo Marques
 *       Creation of the final data concept
 *
 *  September 2013:
 *       Fernando Alexandre
 *       Adaptation to support marrow's runtime value substitution
 *
 *  December 2013:
 *      Herve Paulino
 *      marrow namespace
 *
 *  Created on: 2 de Abr de 2012
 *      Author: Ricardo Marques
 *			Modified by Fernando Alexandre, Herve Paulino
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef __MARROW_FINALDATA_HPP__
#define __MARROW_FINALDATA_HPP__

#include "SingletonData.hpp"

namespace marrow {

    /**
     * Template class that represents a constant single element data-type. The value of this element, of type T, does not vary between distinct executions.
     */
    template <class T>
    class FinalData: public SingletonData<T> {
    public:
        // The traits available to the FinalData (The default does nothing).
        enum Trait {DEFAULT = 0, COMP_OFFSET = 1, COMP_SIZE = 2, COMP_PART_LOC = 3};

        // The values associated to COMP_PART_LOC(CATION). Whether partition is on top, middle or bottom.
        enum PartitionLocation {LOC_TOP = 0, LOC_MIDDLE = 1, LOC_BOTTOM = 2};

        /**
         * Constructor that is parametrized with the data value of this constant data-type. The value @value is used in the execution
         * of the kernel(s), associated to this FinalData object.
         *
         * @param value - The data value used associated to this constant data-type kernel argument.
         */
        FinalData(T value);

        /**
         * Constructor that is parametrized with the data value of this constant data-type. The value @value is used in the execution
         * of the kernel(s), associated to this FinalData object.
         *
         * @param value - The data value used associated to this constant data-type kernel argument.
         * @param trait - The trait associated to this FinalData.
         */
        FinalData(T value, Trait trait);

        /**
         * Class destructor.
         */
        virtual ~FinalData();

        /**
         * @Override DataInfor::value
         *
         * @return The value associated to this instance of FinalData.
         */
        virtual void* value();

        /**
         * Gets this FinalData's trait.
         *
         * @return this FinalData's trait.
         */
        virtual Trait getTrait();

    protected:
        // The data value
        T dataVal;
        Trait trait;
    };

    template <class T> FinalData<T>::FinalData(T value) :
    SingletonData<T>(),
    dataVal(value),
    trait(FinalData<T>::DEFAULT)
    {
        this->type = IWorkData::FINAL;
    }

    template <class T> FinalData<T>::FinalData(T value, FinalData<T>::Trait trait) :
    SingletonData<T>(),
    dataVal(value),
    trait(trait)
    {
        this->type = IWorkData::FINAL;
    }


    template <class T> FinalData<T>::~FinalData(){}

    template <class T> void* FinalData<T>::value(){
        return &dataVal;
    }

    template <class T> typename FinalData<T>::Trait FinalData<T>::getTrait() {
        return trait;
    }
}

#endif /* __MARROW_FINALDATA_HPP__ */
