/*
 * IExecutable.hpp
 *
 *  February 2012:
 *       Ricardo Marques
 *       Creation of the executable concept of a computation tree
 *
 *   September 2013:
 *       Fernando Alexandre
 *       Adaptation for multi-GPU execution
 *
 *  December 2013:
 *      Herve Paulino
 *      marrow namespace
 *
 *  Created on: 22 de Fev de 2012
 *      Author: Ricardo Marques
 *			Modified by Fernando Alexandre, Herve Paulino
 *
 *  Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef __MARROW_IEXECUTABLE_HPP__
#define __MARROW_IEXECUTABLE_HPP__


#include "Vector.hpp"

#include "runtime/IWorkData.hpp"
#include "runtime/PartitionedInfo.hpp"

#include <memory>

namespace marrow {

    /**
     * Interface for defining a executional entity that is eligible to be a part of a compostion tree node, not exclusively its root.
     * The interface defines several functions that enable the exportation of a node's execution behaviour. Consequently, other nodes
     * may prompt marrow::IExecutable instances to perform a certain computation, upon a given set of executional structures.
     */
    class IExecutable {

    public:
        /**
         * Class destructor.
         */
        virtual ~IExecutable() {};

        /*###################### Executional Functions ############################*/

        /**
         * Initializes this executable according to its ancestor's computational environment. The environment given by the ancestor
         * is also shared with this executable's children. On the other hand, the ancestor can provide information about the number
         * of memory object sets to be used, when overlaping communication with computation.
         * After this step the executable is fully functional.
         */
        virtual void initExecutable() = 0;

        /**
         * Takes all the necessary steps to clear the current configuration,
         * relating to the number of devices and number of overlap partitions
         * to be used for the computation.
         */
        virtual void clearConfiguration() = 0;

        /**
         * Takes the necessary steps to reconfigure the executable for a newly configured execution platform.
         * (e.g. requests new partitions in accordance to the new number of devices, etc..)
         */
        virtual void reconfigureExecutable() = 0;

        /**
         * Method that exports the execution behaviour offered by the executable.
         * This executable issues its execution to the received command-queue, @executionQueue, using the pre-inicialized input/output
         * memory objects, namely @inputData and @outputData.
         * Moreover, the executable synchronizes its computations, with the received event @waitEvent, so that the execution may only start
         * after the latter is deemed as completed.
         * Lastly, if the executable performs read operations, they should be targeted at the values stored in @resultMem.
         * This may remove unnecessary read operations, performed by the root node.
         * The function returns an event object. This object is associated to this node's execution, and can be used by other tree nodes to
         * syncronize with this node's execution.
         *
         * @param executionQueue - The used command-queue.
         * @param deviceIndex - The index of the device.
         * @param uniqueId - The unique id of the task this skeleton execution belongs to.
         * @param partitionIndex - The index of the partition.
         * @param overlapPartition - The index of the overlapping partition.
         * @param inputData - The memory objects that hold the input data.
         * @param singletonInputValues - Singleton values to be passed onto the kernels.
         * @param outputData - The memory objects where the results should be stored.
         * @param waitEvent - Event used to synchronize the execution. The latter may only start after the first is completed.
         * @param resultMem - If the executable performs read operations, they should be performed to these memory references.
         * @return An event associated to this object's execution.
         */
        virtual cl_event execute(cl_command_queue executionQueue,
                                unsigned int deviceIndex,
                                unsigned int uniqueId,
                                unsigned int partitionIndex,
                                unsigned int overlapPartition,
                                std::vector<cl_mem> &inputData,
                                std::vector<void*> &singletonInputValues,
                                std::vector<cl_mem> &outputData,
                                cl_event waitEvent,
                                std::vector<std::shared_ptr<marrow::Vector>> &resultMem) = 0;


        /*##################### Informative Functions #############################*/

        /**
         * Method that accesses if this execution entity is initialized, or not.
         *
         * @return True if the objects has been initialized, or false otherwise.
         */
        virtual bool isInitialized() = 0;

        /**
         * Method that is used to obtain the information about this node's input arguments.
         * This information may be given directly by node itself, or by its children.
         *
         * @return A vector of IWorkData objects associated to the input arguments.
         */
        virtual const std::vector<std::shared_ptr<IWorkData>> * getInputDataInfo() = 0;

        /**
         * Method that returns the number of input kernel arguments, associated to this node's execution.
         *
         * @return The number of input kernel arguments.
         */
        virtual unsigned int getNumInputEntries() = 0;

        /**
         * Method that is used to obtain the information about this node's output arguments.
         * This information may be given directly by node itself, or by its children.
         *
         * @return A vector of IWorkData objects associated to the output arguments.
         */
        virtual const std::vector<std::shared_ptr<IWorkData>> * getOutputDataInfo() = 0;

        /**
         * Method that returns the number of output kernel arguments, associated to this node's execution.
         *
         * @return The number of output kernel arguments.
         */
        virtual unsigned int getNumOutputEntries() = 0;

        /**
         * Method that returns the amount of fixed memory required by this computation (unpartitioned buffers).
         *
         * @return The amount of bytes required by unpartitioned buffers.
         */
        virtual unsigned long requiredStaticMem() = 0;

        /**
         * Method that returns the amount of memory required by the indivisible sized chunk.
         *
         * @return The amount of bytes required for an unitary (indivisible) sized chunk.
         */
        virtual unsigned long requiredDynamicMem() = 0;

        /**
         * Method that returns the amount, in bytes, of device global memory that this executional entity
         * requires to accomplish its computations. This amount of memory includes, the node's own
         * requirements, as well as its children's.
         *
         * @return The amount of global memory required to carry out the execution.
         */
        virtual unsigned long requiredGlobalMem() = 0;

        /**
         * Method that returns the amount, in bytes, of device local memory that this executional entity
         * requires to accomplish its computations. This amount of memory includes, the node's own
         * requirements, as well as its children's.
         *
         * @return The amount of local memory required to carry out the execution.
         */
        virtual unsigned long requiredLocalMem() = 0;

        /**
         * Method that asserts if this execution node performs read operations from device address space, to host address space.
         *
         * @return True if OpenCL read operations are performed during execution, or false otherwise.
         */
        virtual bool readsData() = 0;

        /**
         * Gets the partitions of the executable's input.
         *
         * @return A pointer to a vector containing the executable's partitionings.
         */
        virtual std::vector<std::shared_ptr<PartitionedInfo>> * getPartitionedInput() = 0;

        /**
         * Gets the input memory locations of a partitions on this executable.
         *
         * @param partitionIndex - the index of the partition.
         * @param overlapPartitionIndex - the index of the overlap partition.
         * @return pointer to vector containing the input memory locations for a partition.
         */
        virtual std::vector<cl_mem> * getInputMem(unsigned int partitionIndex, unsigned int overlapPartitionIndex) = 0;

        /**
         * Gets the partitions of the executable's output.
         *
         * @return A pointer to a vector containing the executable's partitionings.
         */
        virtual std::vector<std::shared_ptr<PartitionedInfo>> * getPartitionedOutput() = 0;

        /**
         * Gets the output memory locations of a partitions on this executable.
         *
         * @param partitionIndex - the index of the partition.
         * @param overlapPartitionIndex - the index of the overlap partition.
         * @return pointer to vector containing the output memory locations for a partition.
         */
        virtual std::vector<cl_mem> * getOutputMem(unsigned int partitionIndex, unsigned int overlapPartitionIndex) = 0;

        /**
         * Gets all the KernelWrappers of this execution.
         *
         * @return vector with pointers to marrow::IExecutables (void* to avoid cyclic dependencies)
         */
        virtual std::vector<void*> * getNestedExecutables() = 0;

        /**
         * Method that returns whether the executable has set partitions.
         *
         * @return Whether the executable has partitions.
         */
        virtual bool hasSetPartitions() = 0;

        /**
         * Method that returns if this executable has double precision. (for the auto-tuner)
         *
         * @return Whether this executable has double precision.
         */
        virtual bool isDoublePrecision() = 0;

        /**
         * Method that returns whether this executable requires to allocate the input
         * memory. (not necessary currently for second stage of pipeline)
         *
         * @param shouldAlloc - whether this executable need to alocate input memory.
         */
        virtual void requiresInputMemory(bool shouldAlloc) = 0;

        /**
         * Method that returns whether this executable requires to allocate the output
         * memory. (not necessary currently for first stage of pipeline)
         *
         * @param shouldAlloc - whether this executable need to alocate output memory.
         */
        virtual void requiresOutputMemory(bool shouldAlloc) = 0;

        #ifdef PROFILING
        /**
         * Gets the profiling information of the executable, adding
         * a number of tabs for formatting.
         *
         * @param indent - number of tabs to add to the profiling string.
         * @return The string containing the profiling information.
         */
        virtual std::string getProfilingInfo(unsigned int indent) = 0;
        #endif
    };
}
#endif /* __MARROW_IEXECUTABLE_HPP__ */
