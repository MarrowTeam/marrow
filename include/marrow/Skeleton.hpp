/*
 * Skeleton.hpp
 *
 *  February 2012:
 *       Ricardo Marques
 *       Creation of the skeleton concept
 *
 *  September 2013:
 *       Fernando Alexandre
 *       Adaptation for multi-GPU execution
 *
 *  December 2013:
 *      Herve Paulino
 *      marrow namespace
 *      Decoupling into interface and implementation
 *
 *  Created on: 23 de Fev de 2012
 *      Author: Ricardo Marques
 *			Modified by Fernando Alexandre, Herve Paulino
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef __MARROW_SKELETON_HPP__
#define __MARROW_SKELETON_HPP__

#include "IFuture.hpp"
#include "Vector.hpp"

#include "runtime/IPlatformContext.hpp"
#include "runtime/PartitionedInfo.hpp"
#include "runtime/IScheduler.hpp"

namespace marrow {

    class KernelWrapper;
    class MapReduce;

    namespace runtime {
        class Scheduler;
        class TaskLauncher;
    }

    /**
     * Abstract class that represents a basic skeleton. It defines some core functionalities that are common to all skeltons.
     * Such as, an asyncronous execution request mechanism. Additionally, some functionalities are left as abstract, since
     * they are particular to every skeleton. An actual skeleton should extend this abstract class, and implement its pure virtual
     * functions, so as to become a valid Marrow skeleton.
     */
    class Skeleton {
    public:
        /**
         * Class destructor.
         */
        virtual ~Skeleton();

        /**
         * This is the principal function of the Marrow skeleton API. It issues a skeleton execution that uses, as input,
         * the memory references from @inputData, and writes the results to the memory references in @outputData. The input and output
         * memory references must point to valid main memory locations. The latter must contain, or be able to store, data that is compatible
         * with the input/output kernel argument data-types, defined when instantiating the KernelWrappers that comprise a specific
         * composition tree.
         * On the other hand, this method is asynchronous. However, if the uses requests more skeleton executions than the runtime is able to
         * queue at a given moment, this function will block the caller until a new request slot becomes available.
         * Upon its completion, the function returns a reference to an IFuture object. The latter is associated to that specific execution request.
         *
         * @param data - marrow::Vector containing the data memory references (follows the order defined in the IWorkdata for the non-defined arguments such as Buffer).
         * @return  A Future object associated to the execution. When the latter is completed the Future is notified.
         */
        virtual IFuture* write(const std::vector<std::shared_ptr<marrow::Vector>> &inputData,
                            const std::vector<std::shared_ptr<marrow::Vector>> &outputData);

        #ifdef PROFILING
        /**
         * Gets the profiling info of all the computations of this tree.
         *
         * @return formatted string containing the performance values.
         **/
        virtual std::string getProfilingInfo() = 0;
        #endif

        // Set which classes require full access to the Skeleton.
        friend class marrow::runtime::Scheduler;
        friend class marrow::runtime::TaskLauncher;
        friend class marrow::KernelWrapper;
        friend class marrow::MapReduce;

        // The execution platform which can be accessible by any class
        // (To interact with the OpenCL devices)
        static std::unique_ptr<IPlatformContext> executionPlatform;

    protected:
        // Input kernel data-type argument information objects
        std::vector<std::shared_ptr<IWorkData>> inputInfo;
        // Output kernel data-type argument information objects
        std::vector<std::shared_ptr<IWorkData>> outputInfo;
        // Skeleton is inicialized
        bool isInit;
        // Skeleton is being destroyed
        bool closed;
        // Number of kernel input arguments that are singleton values
        unsigned int numSingleton;
        // Size, in bytes, of required device local memory address space
        unsigned long localMemSize;
        // Number of concurrent workers (threads) on the system
        unsigned int numConcurrentWorkers;
        // If this skeleton has partitions set
        bool hasPartitions;

        // *** The marrow "persistent" system ****

        // Scheduler can be re-started (if no computations have been submitted yet)
        static bool canRestartScheduler;
        // Scheduler object, accessibly by any "friend" class
        static std::unique_ptr<marrow::runtime::IScheduler> scheduler;

        // Value used to define a unique request identifier
        static unsigned int uniqueCounter;
        // Mutex used to regulate the access of the unique request counter
        static boost::mutex uniqueCounterMutex;


        /**
         * Constructor function. This constructor inicializes the resources associtated
         * to the Skeleton instance. One of the constructors should be called by
         * any base class that extends Skeleton.
         *
         * @param inputInfo - Information regarding the kernel's input arguments.
         * @param outputInfo - Information regarding the kernel's output arguments.
         */
        Skeleton(const std::vector<std::shared_ptr<IWorkData>> &inputInfo,
                const std::vector<std::shared_ptr<IWorkData>> &outputInfo);


        /**
         * Constructor function. This constructor inicializes the resources associtated
         * to the Skeleton instance. One of the constructors should be called by
         * any base class that extends Skeleton.
         * Reconfigures the Marrow's persistent platform to use numDevices (no checks
         * of validity are made).
         *
         * @param inputInfo - Information regarding the kernel's input arguments.
         * @param outputInfo - Information regarding the kernel's output arguments.
         * @param numDevices - Number of devices to be used for the computation.
         */
        Skeleton(const std::vector<std::shared_ptr<IWorkData>> &inputInfo,
                const std::vector<std::shared_ptr<IWorkData>> &outputInfo,
                unsigned int numDevices);

        /**
         * Constructor function. This constructor inicializes the resources associtated
         * to the Skeleton instance. One of the constructors should be called by
         * any base class that extends Skeleton.
         * Reconfigures the Marrow's persistent platform to use numDevices (no checks
         * of validity are made) and numOverlapPartitions.
         *
         * @param inputInfo - Information regarding the kernel's input arguments.
         * @param outputInfo - Information regarding the kernel's output arguments.
         * @param numDevices - Number of devices to be used for the computation.
         * @param numOverlapPartitions - Number of partitions within each device.
         */
        Skeleton(const std::vector<std::shared_ptr<IWorkData>> &inputInfo,
                const std::vector<std::shared_ptr<IWorkData>> &outputInfo,
                unsigned int numDevices,
                unsigned int numOverlapPartitions);

        /**
         * Sets the partitions associated to the skeleton's computation tree, triggering
         * a partitioning call to the Scheduler.
         *
         * @param isReconfigure - if this call is a result of a reconfiguration
         *					(requiring cleanup)
         */
        virtual void setPartitions(bool isReconfigure);


        /** ########################## Pure virtual functions ############################ **/

        /**
         * Used to adapt the execution behaviour to each skeleton. It is responsible
         * only for the execution of the skeleton, as the explicit transfers are defined
         * on other functions.
         *
         * @param executionQueue - the command queue used to issue the execution commands.
         * @param deviceIndex - The index of the device.
         * @param uniqueId - The indentifier of the computation
         * @param partitionIndex - The partitions index.
         * @param overlapPartition - The overlap partition.
         * @param inputData - The input marrow::Vectors.
         * @param outputData - The output marrow::Vectors.
         * @param inputMem - The input memory locations.
         * @param outputMem - The output memory locations.
         */
        virtual void executeSkel(const cl_command_queue &executionQueue,
                                const unsigned int deviceIndex,
                                const unsigned int uniqueId,
                                const unsigned int partitionIndex,
                                const unsigned int overlapPartition,
                                std::vector<std::shared_ptr<marrow::Vector>> &inputData,
                                std::vector<std::shared_ptr<marrow::Vector>> &outputData,
                                std::vector<cl_mem> &inputMem,
                                std::vector<cl_mem> &outputMem) = 0;

        /**
         * Uploads the input data of a partition.
         *
         * @param executionQueue - the command queue used to issue the execution commands.
         * @param deviceIndex - The index of the device.
         * @param uniqueId - The indentifier of the computation
         * @param partitionIndex - The partitions index.
         * @param overlapPartition - The overlap partition.
         * @param inputData - The input marrow::Vectors.
         * @param inputMem - The input memory locations.
         */
        virtual void uploadInputData(const cl_command_queue &executionQueue,
                                const unsigned int deviceIndex,
                                const unsigned int uniqueId,
                                const unsigned int partitionIndex,
                                const unsigned int overlapPartition,
                                std::vector<std::shared_ptr<marrow::Vector>> &inputData,
                                std::vector<cl_mem> &inputMem) = 0;

        /**
         * Downloads the output data of a partition.
         *
         * @param executionQueue - The used command-queue.
         * @param deviceIndex - The index of the device for this computation.
         * @param uniqueId - The unique identifier of this computation.
         * @param partitionIndex - The partition index to be computed.
         * @param overlapPartition - The overlap partition index to be computed.
         * @param outputData - The dataset for transfer.
         * @param outputMem - The memory objects that hold the output data.
         */
        virtual void downloadOutputData(const cl_command_queue &executionQueue,
                                const unsigned int deviceIndex,
                                const unsigned int uniqueId,
                                const unsigned int partitionIndex,
                                const unsigned int overlapPartition,
                                std::vector<std::shared_ptr<marrow::Vector>> &outputData,
                                std::vector<cl_mem> &outputMem) = 0;

        /**
         * Get the total amount of global memory this computation tree requires. Does
         * not take into account partitioning.
         *
         * @return the amount of global-memory bytes required by this computation.
         **/
        virtual unsigned long requiredGlobalMem() = 0;

        /**
         * Gets all the KernelWrappers of this computation tree.
         *
         * @return vector with pointers to marrow::IExecutables (void* to avoid cyclic dependencies)
         */
        virtual std::vector<void*> * getExecutables() = 0;


        /**
         * Gets the input partitions of this Skeleton.
         *
         * @return pointer to a vector containing the partition definitions.
         **/
        virtual std::vector<std::shared_ptr<PartitionedInfo>> * getPartitionedInput() = 0;

        /**
         * Gets the input GPU memory associated with partititionIndex and overlapPartitionIndex.
         *
         * @param partitionIndex - the index of the partition.
         * @param overlapPartitionIndex - the index of the overlap partition.
         * @return pointer to a vector containing the GPU memory.
         **/
        virtual std::vector<cl_mem> * getInputMem(unsigned int partitionIndex,
                                                unsigned int overlapPartitionIndex) = 0;

        /**
         * Gets the output partitions of this Skeleton.
         *
         * @return pointer to a vector containing the partition definitions.
         **/
        virtual std::vector<std::shared_ptr<PartitionedInfo>> * getPartitionedOutput() = 0;

        /**
         * Gets the output GPU memory associated with partititionIndex and overlapPartitionIndex.
         *
         * @param partitionIndex - the index of the partition.
         * @param overlapPartitionIndex - the index of the overlap partition.
         * @return pointer to a vector containing the GPU memory.
         **/
        virtual std::vector<cl_mem> * getOutputMem(unsigned int partitionIndex,
                                                unsigned int overlapPartitionIndex) = 0;

        /**
         * This function defines a computation step to be executed at the end of
         * the computation (e.g. Reduce in MapReduce). Executed only when all partitions
         * have finished.
         *
         * @param uniqueId - the unique id of the computation request.
         * @param outputData - the output of the skeleton computation.
         **/
        virtual void finishExecution(const unsigned int uniqueId,
                                    std::vector<std::shared_ptr<marrow::Vector>> &outputData) = 0;
    };
}

#endif /* __MARROW_SKELETON_HPP__ */
