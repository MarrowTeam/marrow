/*
 * DataInfo.hpp
 *
 *  April 2012:
 *      Ricardo Marques
 *      Creation of the interface defining data arguments
 *
 *  September 2013:
 *      Fernando Alexandre
 *      Adaptation to suport partitions and their implicit merging
 *      using custom and pre-defined functions.
 *
 *  December 2013:
 *      Herve Paulino
 *      marrow namespace
 *
 *  Created on: 2 de Abr de 2012
 *      Author: Ricardo Marques
 *			Modified by Fernando Alexandre, Herve Paulino
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef __MARROW_DATAINFO_HPP__
#define __MARROW_DATAINFO_HPP__

#include "MergeOp.hpp"
#include "exceptions/UnsupportedImageException.hpp"
#include "runtime/IWorkData.hpp"


/**
 * Template class that represents a simple data-type, that is not compatible with OpenCL image objects. This class provides base functionality,
 * for other data-type objects. As such, DataInfo is equivalent to the most basic type of data-type objects, but cannot itself be instantiated.
 * Other data-type objects extend DataInfo to provide concrete functionality, to a specific type of kernel arguments.
 */

namespace marrow {
    template <class T>
    class DataInfo: public IWorkData {
    public:
        virtual ~DataInfo();

        virtual unsigned long getMemSize();

        virtual dataMode getAccessMode();

        virtual dataType getDataType();

        virtual format getFormat();

        virtual partitionMode getPartitionMode();

        virtual unsigned int getIndivisibleSize();

        virtual bool isDoublePrecision();

        virtual bool isEqual(IWorkData &other);

        virtual unsigned int getWidth();

        virtual unsigned int getHeight();

        virtual unsigned int getDepth();

        virtual void* value();

        virtual void merge(void * values1, void * values2);

    protected:
        // Number of elements that the kernel argument may hold
        unsigned int numberElems;
        // Access permissions for the kernel argument
        dataMode accessMode;
        // Data-type of the object
        dataType type;
        // Partitioning mode of object
        partitionMode partitioningMode;
        // Memory size, in bytes, of the data-type object
        unsigned long memSize;
        // The indivisible number of worksize elements (no need to take into account the memory size of each element).
        unsigned int indivisibleSize;

        // The merge operation.
        MergeOp<T> * mergeOp;

        /**
         * Protected constructor. Used by base classes when being instantiated.
         * Declares the bare minimum parametrization required to create a DataInfo instance.
         *
         * @param numberElems - Number of elements of type T that compose the kernel argument.
         * @param accessMode - The access permitions for the kernel argument. Enumeration dataMode is an IWorkdata member
         * @param type - The type of the kernel argument. Enumeration dataType is an IWorkdata member.
         */
        DataInfo(unsigned int numberElems, dataMode accessMode, dataType type);

        /**
         * Protected constructor. Used by base classes when being instantiated.
         * Declares the bare minimum parametrization required to create a DataInfo instance.
         *
         * @param numberElems - Number of elements of type T that compose the kernel argument.
         * @param accessMode - The access permitions for the kernel argument. Enumeration dataMode is an IWorkdata member
         * @param type - The type of the kernel argument. Enumeration dataType is an IWorkdata member.
         * @param partitioningMode - The partitioning mode of this object.
         */
        DataInfo(unsigned int numberElems, dataMode accessMode, dataType type, partitionMode partitioningMode);

        /**
         * Protected constructor. Used by base classes when being instantiated.
         * Declares the bare minimum parametrization required to create a DataInfo instance.
         *
         * @param numberElems - Number of elements of type T that compose the kernel argument.
         * @param accessMode - The access permitions for the kernel argument. Enumeration dataMode is an IWorkdata member
         * @param type - The type of the kernel argument. Enumeration dataType is an IWorkdata member.
         * @param partitioningMode - The partitioning mode of this object.
         * @param indivisibleSize - The minimum size this object can be decomposed in.
         */
        DataInfo(unsigned int numberElems, dataMode accessMode, dataType type, partitionMode partitioningMode, unsigned int indivisibleSize);
    };

    template <class T> DataInfo<T>::DataInfo(unsigned int numberElems, dataMode accessMode, dataType type, partitionMode partitioningMode, unsigned int indivisibleSize):
    numberElems(numberElems),
    accessMode(accessMode),
    type(type),
    partitioningMode(partitioningMode),
    memSize(sizeof(T)*numberElems),
    indivisibleSize(indivisibleSize),
    mergeOp(new MergeOp<T>(partitioningMode))
    {}

    template <class T> DataInfo<T>::DataInfo(unsigned int numberElems, dataMode accessMode, dataType type, partitionMode partitioningMode):
    numberElems(numberElems),
    accessMode(accessMode),
    type(type),
    partitioningMode(partitioningMode),
    memSize(sizeof(T)*numberElems),
    indivisibleSize(1),
    mergeOp(new MergeOp<T>(partitioningMode))
    {}

    template <class T> DataInfo<T>::DataInfo(unsigned int numberElems, dataMode accessMode, dataType type):
    numberElems(numberElems),
    accessMode(accessMode),
    type(type),
    partitioningMode(IWorkData::PARTITIONABLE),
    memSize(sizeof(T)*numberElems),
    indivisibleSize(1),
    mergeOp(new MergeOp<T>(partitioningMode))
    {}

    template <class T> DataInfo<T>::~DataInfo(){}

    template <class T> unsigned long DataInfo<T>::getMemSize(){
        return memSize;
    }

    template <class T> typename DataInfo<T>::dataMode DataInfo<T>::getAccessMode(){
        return accessMode;
    }

    template <class T> typename DataInfo<T>::dataType DataInfo<T>::getDataType(){
        return type;
    }

    template <class T> typename DataInfo<T>::format DataInfo<T>::getFormat(){
        throw UnsupportedImageException();
    }

    template <class T> typename DataInfo<T>::partitionMode DataInfo<T>::getPartitionMode(){
        return partitioningMode;
    }

    template <class T> unsigned int DataInfo<T>::getIndivisibleSize() {
        return indivisibleSize;
    }

    template <class T> bool DataInfo<T>::isDoublePrecision() {
        return (sizeof(T) == 8);
        // 64 bit type => double precision; If it is bigger, it is a composite type (e.g. cl_float4)
        // Applicable to singleton also.
    }

    template <class T> bool DataInfo<T>::isEqual(IWorkData &other){
        return (type == other.getDataType() && memSize == other.getMemSize() && accessMode == other.getAccessMode());
    }

    template <class T> unsigned int DataInfo<T>::getWidth(){
        return numberElems;
    }

    template <class T> unsigned int DataInfo<T>::getHeight(){
        return 1;
    }

    template <class T> unsigned int DataInfo<T>::getDepth(){
        return 1;
    }

    template <class T> void* DataInfo<T>::value(){
        return NULL;
    }

    template <class T> void DataInfo<T>::merge(void* values1, void* values2) {
        mergeOp->merge(static_cast<T*>(values1), static_cast<T*>(values2), numberElems);
    }

}

#endif /* __MARROW_DATAINFO_HPP__ */
