/*
 * Loop.hpp
 *
 *  April 2012:
 *       Ricardo Marques
 *       Creation of the Loop skeleton
 *
 *  September 2013:
 *      Fernando Alexandre
 *      Adaptation for multi-GPU execution, including new execution types
 *
 *  December 2013:
 *      Herve Paulino
 *      marrow namespace
 *
 *  Created on: 19 de Abr de 2012
 *      Author: Ricardo Marques
 *			Modified by Fernando Alexandre, Herve Paulino
 *
 *  Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef __MARROW_LOOP_HPP__
#define __MARROW_LOOP_HPP__

#include <string>

#include "IExecutable.hpp"
#include "Skeleton.hpp"
#include "ILoopState.hpp"

// Default execution strategy, partial results are used to step the Loop's state
#define READBACKPARTIAL true

// Output of a Loop execution is not equivalent to its input
#define LOOPDATAMISMATCH "Given input info must be equivalent to output info"

namespace marrow {

    /**
     * Loop skeleton. It applies an iterative computation to a single data-set, while the Loop's condition remains valid.
     * This skeleton is nestable.
     */
    class Loop: public marrow::Skeleton, public marrow::IExecutable {
    public:
        /**
         * Class destructor.
         */
        virtual ~Loop();

        #ifdef PROFILING
        virtual std::string getProfilingInfo();
        virtual std::string getProfilingInfo(unsigned int indent);
        #endif

    protected:
        // Child execution entity
        std::unique_ptr<marrow::IExecutable> exec;
        // If Loop reads the results of each iteration back to host memory
        bool partialRead;
        // If any of the stages performs device memory read operations
        bool childReads;
        // If the step requires synchronization between all the partitions
        bool stepSync;

        // Variables used for synched computation
        // Mutex used for the resources below
        boost::mutex currentStatesMutex;
        // Whether the step function with a synched iteration has been computed. Indexed by
        // uniqueId
        std::map<unsigned int, bool*> currentStatesSync;
        // Mutexes used for access of each state. Indexed by uniqueId
        std::map<unsigned int, boost::mutex*> currentStatesMutexes;
        // Map containing the state and shared barrier for synchronization
        std::map<unsigned int, std::pair<ILoopState*, boost::barrier*>> currentStates;

        /**
         * The Loop is parametrized with an executable entity, @exec. The latter is an
         * instance of the marrow::IExecutable interface. If used, this constructor defines an execution behaviour for the Loop
         * instances. Namely, it defines that the intances read the results of each iteration back to main memory, for
         * for possible evaluation. The maximum number of devices and default number of overlap partitions are used (if it is the root).
         *
         * @param exec - The executable entity on which the Loop will prompt the executions.
         */
        Loop(std::unique_ptr<marrow::IExecutable> &exec);

        /**
         * The Loop is parametrized with an executable entity, @exec. The latter is an
         * instance of the marrow::IExecutable interface. If used, this constructor defines an execution behaviour for the Loop
         * instances. Namely, it defines that the intances read the results of each iteration back to main memory, for
         * for possible evaluation. Used numDevices number of devices and default number of overlap partitions.
         * When this devices/overlapPartitions are defined, the skeleton is assumed as the root node.
         *
         * @param exec - The executable entity on which the Loop will prompt the executions.
         * @param numDevices - Number of devices to be used.
         */
        Loop(std::unique_ptr<marrow::IExecutable> &exec,
            unsigned int numDevices);

        /**
         * The Loop is parametrized with an executable entity, @exec. The latter is an
         * instance of the marrow::IExecutable interface. If used, this constructor defines an execution behaviour for the Loop
         * instances. Namely, it defines that the intances read the results of each iteration back to main memory, for
         * for possible evaluation. Used numDevices number of devices and numOverlapPartitions number of overlap partitions.
         * When this devices/overlapPartitions are defined, the skeleton is assumed as the root node.
         *
         * @param exec - The executable entity on which the Loop will prompt the executions.
         * @param numDevices - Number of devices to be used.
         * @param numOverlapPartitions - number of overlap partitions within each partition.
         */
        Loop(std::unique_ptr<marrow::IExecutable> &exec,
            unsigned int numDevices,
            unsigned int numOverlapPartitions);

        /**
         * The Loop is parametrized with an executable entity, @exec. The latter is an
         * instance of the marrow::IExecutable interface. This constructor delegates the definition of an execution behaviour, for the Loop
         * instances, to the user. The parameter @readbackPartial defines if the Loop instances read the results of each iteration back to main memory, for
         * for possible evaluation.
         *
         * @param exec - The executable entity on which the Loop will prompt the executions.
         * @param readbackPartial - Boolean parameter that determines if the Loop reads the results of each iteration to main
         * memory (true if yes, false otherwise).
         */
        Loop(std::unique_ptr<marrow::IExecutable> &exec,
            bool readbackPartial);

        /**
         * The Loop is parametrized with an executable entity, @exec. The latter is an
         * instance of the marrow::IExecutable interface. This constructor delegates the definition of an execution behaviour, for the Loop
         * instances, to the user. The parameter @readbackPartial defines if the Loop instances read the results of each iteration back to main memory, for
         * for possible evaluation. Used numDevices number of devices and default number of overlap partitions.
         * When this devices/overlapPartitions are defined, the skeleton is assumed as the root node.
         *
         * @param exec - The executable entity on which the Loop will prompt the executions.
         * @param readbackPartial - Boolean parameter that determines if the Loop reads the results of each iteration to main
         * memory (true if yes, false otherwise).
         * @param numDevices - Number of devices to be used.
         */
        Loop(std::unique_ptr<marrow::IExecutable> &exec,
            bool readbackPartial,
            unsigned int numDevices);

        /**
         * The Loop is parametrized with an executable entity, @exec. The latter is an
         * instance of the marrow::IExecutable interface. This constructor delegates the definition of an execution behaviour, for the Loop
         * instances, to the user. The parameter @readbackPartial defines if the Loop instances read the results of each iteration back to main memory, for
         * for possible evaluation. Used numDevices number of devices and numOverlapPartitions number of overlap partitions.
         * When this devices/overlapPartitions are defined, the skeleton is assumed as the root node.
         *
         * @param exec - The executable entity on which the Loop will prompt the executions.
         * @param readbackPartial - Boolean parameter that determines if the Loop reads the results of each iteration to main
         * memory (true if yes, false otherwise).
         * @param numDevices - Number of devices to be used.
         * @param numOverlapPartitions - number of overlap partitions within each partition.
         */
        Loop(std::unique_ptr<marrow::IExecutable> &exec,
            bool readbackPartial,
            unsigned int numDevices,
            unsigned int numOverlapPartitions);

        /**
         * The Loop is parametrized with an executable entity, @exec. The latter is an
         * instance of the marrow::IExecutable interface. This constructor delegates the definition of an execution behaviour, for the Loop
         * instances, to the user. The parameter @readbackPartial defines if the Loop instances read the results of each iteration back to main memory, for
         * for possible evaluation. The maximum number of devices and default number of overlap partitions are used (if it is the root).
         * This constructor also enables the definition whether this loop requires a synched step function or not.
         *
         * @param exec - The executable entity on which the Loop will prompt the executions.
         * @param readbackPartial - Boolean parameter that determines if the Loop reads the results of each iteration to main
         * memory (true if yes, false otherwise).
         * @param stepSync - Boolean defining if this loop requires a synchronized step.
         */
        Loop(std::unique_ptr<marrow::IExecutable> &exec,
            bool readbackPartial,
            bool stepSync);

        /**
         * The Loop is parametrized with an executable entity, @exec. The latter is an
         * instance of the marrow::IExecutable interface. This constructor delegates the definition of an execution behaviour, for the Loop
         * instances, to the user. The parameter @readbackPartial defines if the Loop instances read the results of each iteration back to main memory, for
         * for possible evaluation. Uses numDevices number of devices and default number of overlap partitions (if it is the root).
         * This constructor also enables the definition whether this loop requires a synched step function or not.
         *
         * @param exec - The executable entity on which the Loop will prompt the executions.
         * @param readbackPartial - Boolean parameter that determines if the Loop reads the results of each iteration to main
         * memory (true if yes, false otherwise).
         * @param stepSync - Boolean defining if this loop requires a synchronized step.
         * @param numDevices - Number of devices to be used.
         */
        Loop(std::unique_ptr<marrow::IExecutable> &exec,
            bool readbackPartial,
            bool stepSync,
            unsigned int numDevices);

        /**
         * The Loop is parametrized with an executable entity, @exec. The latter is an
         * instance of the marrow::IExecutable interface. This constructor delegates the definition of an execution behaviour, for the Loop
         * instances, to the user. The parameter @readbackPartial defines if the Loop instances read the results of each iteration back to main memory, for
         * for possible evaluation. Uses numDevices number of devices and numOverlapPartitions number of overlap partitions (if it is the root).
         * This constructor also enables the definition whether this loop requires a synched step function or not.
         *
         * @param exec - The executable entity on which the Loop will prompt the executions.
         * @param readbackPartial - Boolean parameter that determines if the Loop reads the results of each iteration to main
         * memory (true if yes, false otherwise).
         * @param stepSync - Boolean defining if this loop requires a synchronized step.
         * @param numDevices - Number of devices to be used.
         * @param numOverlapPartitions - number of overlap partitions within each partition.
         */
        Loop(std::unique_ptr<marrow::IExecutable> &exec,
            bool readbackPartial,
            bool stepSync,
            unsigned int numDevices,
            unsigned int numOverlapPartitions);

        /**
         * Auxiliar method that refracts part of the inicialization process, that is common to all constructors.
         *
         * @param isReconfigure - whether this call is a consequence of a reconfiguration.
         */
        virtual void initCycle(bool isReconfigure);

        /**
         * Method that reads from a given set of device memory objects to a another set of host memory positions. It only starts reading after the execution
         * associated to the @execEvent OpenCL event is completed.
         *
         * @param readQueue - Command-queue used to order the reading of the results.
         * @param partitionIndex - index of the partition.
         * @param overlapPartition - index of the overlap parition within partitionIndex.
         * @param execEvent - The event associated to the execution. Only after it is deemed as complete, may the reading begin.
         * @param outMem - Output device memory, from which the data is read.
         * @param outData - Output host memory, to which the device data is read.
         */
        virtual void readResults(cl_command_queue readQueue,
                                unsigned int partitionIndex,
                                const unsigned int overlapPartition,
                                cl_event execEvent,
                                const std::vector<cl_mem> &outMem,
                                std::vector<std::shared_ptr<marrow::Vector>> &outData);

        /**
         * This method is a representation of a Loop execution strategy. Namely the one that does not read the results of each iteration back
         * to main memory, for further evaluation. It uses the same execution enviroment as the marrow::IExecutable::execute method, ergo the same parameters (
         * with the exception of the first).
         *
         * @param execQueue - The used command-queue.
         * @param deviceIndex - index of the device to be executed on.
         * @param uniqueId - unique id of the task.
         * @param partitionIndex - index of the partition.
         * @param overlapPartition - index of the overlap parition within partitionIndex.
         * @param inputData - The memory objects that hold the input data.
         * @param singletonInputValues - Singleton values to be passed onto the first stage.
         * @param outputData - The memory objects where the results of the second stage should be stored.
         * @param waitEvent - Event used to synchronize the execution. The latter may only start after the first is completed.
         * @param resultMem - If the executable performs read operations, they should be performed to these memory references
         * @return An event associated to this object's execution.
         */
        virtual cl_event nonReadbackExec(cl_command_queue execQueue,
                                        unsigned int deviceIndex,
                                        unsigned int uniqueId,
                                        unsigned int partitionIndex,
                                        const unsigned int overlapPartition,
                                        std::vector<cl_mem> &inputData,
                                        std::vector<void*> &singletonInputValues,
                                        std::vector<cl_mem> &outputData, cl_event waitEvent,
                                        std::vector<std::shared_ptr<marrow::Vector>> &resultMem);

        /**
         * This method is a representation of a Loop execution strategy. Namely the one that reads the results of each iteration back
         * to main memory, for further evaluation. This function uses a synchronized step function which is only computed by one thread.
         *
         * @param execQueue - The used command-queue.
         * @param deviceIndex - index of the device to be executed on.
         * @param uniqueId - unique id of the task.
         * @param partitionIndex - index of the partition.
         * @param overlapPartition - index of the overlap parition within partitionIndex.
         * @param inputData - The memory objects that hold the input data.
         * @param singletonInputValues - Singleton values to be passed onto the first stage.
         * @param outputData - The memory objects where the results of the second stage should be stored.
         * @param waitEvent - Event used to synchronize the execution. The latter may only start after the first is completed.
         * @param resultMem - If the executable performs read operations, they should be performed to these memory references
         * @return An event associated to this object's execution.
         */
         virtual cl_event readbackSyncdExec(cl_command_queue execQueue,
                                unsigned int deviceIndex,
                                unsigned int uniqueId,
                                unsigned int partitionIndex,
                                const unsigned int overlapPartition,
                                std::vector<cl_mem> &inputData,
                                std::vector<void*> &singletonInputValues,
                                std::vector<cl_mem> &outputData,
                                std::vector<std::shared_ptr<marrow::Vector>> &resultMem);

        /**
         * This method is a representation of a Loop execution strategy. Namely the one that reads the results of each iteration back
         * to main memory, for further evaluation.
         *
         * @param execQueue - The used command-queue.
         * @param deviceIndex - index of the device to be executed on.
         * @param uniqueId - unique id of the task.
         * @param partitionIndex - index of the partition.
         * @param overlapPartition - index of the overlap parition within partitionIndex.
         * @param inputData - The memory objects that hold the input data.
         * @param singletonInputValues - Singleton values to be passed onto the first stage.
         * @param outputData - The memory objects where the results of the second stage should be stored.
         * @param waitEvent - Event used to synchronize the execution. The latter may only start after the first is completed.
         * @param resultMem - If the executable performs read operations, they should be performed to these memory references
         * @return An event associated to this object's execution.
         */
        virtual cl_event readbackExec(cl_command_queue execQueue,
                                unsigned int deviceIndex,
                                unsigned int uniqueId,
                                unsigned int partitionIndex,
                                const unsigned int overlapPartition,
                                std::vector<cl_mem> &inputData,
                                std::vector<void*> &singletonInputValues,
                                std::vector<cl_mem> &outputData,
                                std::vector<std::shared_ptr<marrow::Vector>> &resultMem);

        /** marrow::Skeleton Interface functions **/

        virtual void executeSkel(const cl_command_queue &executionQueue,
                                const unsigned int deviceIndex,
                                const unsigned int uniqueId,
                                const unsigned int partitionIndex,
                                const unsigned int overlapPartition,
                                std::vector<std::shared_ptr<marrow::Vector>> &inputData,
                                std::vector<std::shared_ptr<marrow::Vector>> &outputData,
                                std::vector<cl_mem> &inputMem,
                                std::vector<cl_mem> &outputMem);

        virtual void uploadInputData(const cl_command_queue &executionQueue,
                                const unsigned int deviceIndex,
                                const unsigned int uniqueId,
                                const unsigned int partitionIndex,
                                const unsigned int overlapPartition,
                                std::vector<std::shared_ptr<marrow::Vector>> &inputData,
                                std::vector<cl_mem> &inputMem);

        virtual void downloadOutputData(const cl_command_queue &executionQueue,
                                const unsigned int deviceIndex,
                                const unsigned int uniqueId,
                                const unsigned int partitionIndex,
                                const unsigned int overlapPartition,
                                std::vector<std::shared_ptr<marrow::Vector>> &outputData,
                                std::vector<cl_mem> &outputMem);

        virtual std::vector<void*> * getExecutables();

        virtual std::vector<std::shared_ptr<PartitionedInfo>> * getPartitionedInput();

        virtual std::vector<cl_mem> * getInputMem(unsigned int partitionIndex, unsigned int overlapPartitionIndex);

        virtual std::vector<std::shared_ptr<PartitionedInfo>> * getPartitionedOutput();

        virtual std::vector<cl_mem> * getOutputMem(unsigned int partitionIndex, unsigned int overlapPartitionIndex);

        virtual void finishExecution(const unsigned int uniqueId, std::vector<std::shared_ptr<marrow::Vector>> &outputData);

        /** marrow::IExecutable Interface functions **/

        virtual bool isInitialized();

        virtual void initExecutable();

        virtual void clearConfiguration();

        virtual void reconfigureExecutable();

        virtual cl_event execute(cl_command_queue executionQueue,
                                unsigned int deviceIndex,
                                unsigned int uniqueId,
                                unsigned int partitionIndex,
                                unsigned int overlapPartition,
                                std::vector<cl_mem> &inputData,
                                std::vector<void*> &singletonInputValues,
                                std::vector<cl_mem> &outputData,
                                cl_event waitEvent,
                                std::vector<std::shared_ptr<marrow::Vector>> &resultMem);

        virtual const std::vector<std::shared_ptr<IWorkData>> * getInputDataInfo();

        virtual unsigned int getNumInputEntries();

        virtual const std::vector<std::shared_ptr<IWorkData>> * getOutputDataInfo();

        virtual unsigned int getNumOutputEntries();

        virtual unsigned long requiredStaticMem();

        virtual unsigned long requiredDynamicMem();

        virtual unsigned long requiredGlobalMem();

        virtual unsigned long requiredLocalMem();

        virtual bool readsData();

        virtual std::vector<void*> * getNestedExecutables();

        virtual bool hasSetPartitions();

        virtual bool isDoublePrecision();

        virtual void requiresInputMemory(bool shouldAlloc);

        virtual void requiresOutputMemory(bool shouldAlloc);

        /** Abstract functions **/

        virtual ILoopState* createState() = 0;
    };
}
#endif /* __MARROW_LOOP_HPP__ */
