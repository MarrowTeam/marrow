/*
 * ILoopState.hpp
 *
 *  June 2012:
 *      Ricardo Marques
 *      Creation of a Loop's state interface
 *
 *  December 2013:
 *      Herve Paulino
 *      marrow namespace
 *
 *  Created on: 22 de Jun de 2012
 *      Author: Ricardo Marques
 *	       Modified by Herve Paulino
 *
 * Copyright 2014 MarrowTeam
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef __MARROW_ILOOPSTATE_HPP__
#define __MARROW_ILOOPSTATE_HPP__

#include "Vector.hpp"

namespace marrow {

    /**
     * Class that represents a Loop state environment. The latter contains, on the one hand, the necessary information
     * about the current state of the loop execution, and on the other hand, the computational behaviour of certain common Loop
     * execution processes. For instance, how to evaluate the condition, or how to step the loop's state after each iteration.
     * These environments are used by Loop instances to provide isolation between distinct executions, so that they may be
     * executed in parallel.
     */
    class ILoopState {
    public:
        virtual ~ILoopState() {};
        /**
         * Method that evaluates the condition of this Loop state, and returns true or false appropriately.
         *
         * @return True if the condition is valid, or false otherwise.
         */
        virtual bool condition() = 0;

        /**
         * Method that advances the state of the Loop. This process may use internal or external loop information to
         * proceed as such. The internal loop information is represented by the @previousOut parameter. It is a set of memory
         * references that point to the results of the last Loop iteration.
         *
         * @param previousOut - Memory references to the last Loop iteration's output.
         */
        virtual void step(std::vector<std::shared_ptr<marrow::Vector>> &previousOut) = 0;

        /**
         * Method that resets this Loop state enviroment to its original state. From this point onward, the Loop state can be
         * used to compute another execution.
         */
        virtual void reset() = 0;
    };
}
#endif /* __MARROW_ILOOPSTATE_HPP__ */
